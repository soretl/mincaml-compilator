let rec succ x = 
  let rec add y = x + y in 
  add 1 
in
print_int (succ 42)
