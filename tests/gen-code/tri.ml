let ar = Array.create 10 0 in
ar.(0) <- 5;
ar.(1) <- 2;
ar.(2) <- 9;
ar.(3) <- 1;
ar.(4) <- 4;
ar.(5) <- 0;
ar.(6) <- 6;
ar.(7) <- 8;
ar.(8) <- 7;
ar.(9) <- 3;
let rec print_array a s i =
  if i>=s
    then
      ()
    else(
      print_int (a.(i));
      print_array a s (i+1))
in
(*print_array ar 10 0;*)
let rec max a s m=
  if s<0
  then
    m
  else(
	 if a.(s)>a.(m)
	 then(
	      max a (s-1) s
	      )
	 else(
	      max a (s-1) m
	      )
	 )
in
let rec tri a s =
if s>0
then(
     let m = max a (s-1) (s-1) in
     let tmp = a.(s-1) in
     a.(s-1) <- a.(m);
     a.(m) <- tmp;
     tri a (s-1)
    )
else
()
in
tri ar 10;
print_array ar 10 0
