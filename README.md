# MinCaml-Compilator
### Un compilateur mincaml -> arm

## Fonctionalités
Les fonctionnalités actuelles sont les suivantes:

### typecheking:
- [X] Types de base (Int, Bool, Unit, Float ...)
- [X] If-Then-Else
- [X] Tuples et déclaration
- [X] Appels de fonctions
- [X] Let et Let-Rec
- [X] Génération Equations
- [X] Résolution Equations
- [X] Tableaux

### frontend
- [X] Expressions arithmétiques simples
- [X] Appels fonctions externes
- [X] Déclaration de fonctions (LetRec)
- [X] Créer apply_direct
- [X] If-Then-Else
- [X] Tableaux
- [X] Closure simples
- [ ] Closure avancée
- [ ] Float

### backend
- [X] Allocation de registres spill everything
- [X] Allocation de registres linear Scan
- [ ] Allocation de registes graph coloration
- [ ] Allocation de registres tree scan
- [X] Génération expression arithmétique simple
- [X] Génération appel de fonctions externes
- [X] Génération déclaration fonction
- [X] Génération if-then-else
- [X] Génération grands entiers
- [X] Génération closure simples
- [ ] Génération closure avancée
- [X] Génération tuples, arrays
- [ ] Génération floats

## Optimisation

Le compilateur peut être lancé dans ça version non optimisée ou optimisée (voir tableau d'options). Voici un tableau représentant les gains en lignes d'ARM en fonction de la taille des fichiers générés.

| Nombre de lignes des fichiers générés | Nombre de lignes gagnées en moyenne sur tous les tests |
|:------------------------------------- |:------------------------------------------------------ |
| < 100                                 | 29%                                                    |
| > 100                                 | 118%                                                   |


## Compilation

```
make
```
ou
```
ant -buildfile buildCL.xml build
```

## Execution
```
./mincamlc [OPTIONS] <fichier_entree> 
```

Les options suivantes sont disponibles:

| Option    | Version longue  | Arguments                           | Description                                         |
|:---------:|:--------------- |:-----------------------------------:|:--------------------------------------------------- |
| -v        | --version       | non                                 | Affiche la version du compilateur                   |
| -h        | --help          | non                                 | Affiche l'aide                                      |
| -o        | --output        | fichier_sortie                      | Spécification du fichier de sortie                  |
| -p        | --parse-only    | non                                 | Parse uniquement l'entree                           |
| -backend  |                 | non                                 | Execute uniquement le backend  (asml -> arm)        |
| -frontend |                 | non                                 | Execute uniquement le frontend (mincaml -> asml     |
| -t        | --typechecking  | non                                 | Execute uniquement le typechecking sur du mincaml   |
| -O        | --optimisation  | 0: spill everything, 1: Linear Scan | Type d'allocation de registres                      |
| -d        | --debug         | non                                 | Affiche une trace de chaque étape de la compilation |



### Tests
Les tests sont dans le dossier `tests`.
Pour lancer les tests executer :
```
ant -buildfile buildCL.xml {test_all | test_frontend | test_backend | test_backend_opti | test_parser | test_gen-code | test_gen-code_opti | test_typechecking}

```
ou

```
make test_all
```

Pour lancer tous les tests (optimisation -O0).

| test               | couverture                  |
|:------------------ |:--------------------------- |
| test_all           | Tous les tests suivants -O0 |
| test_all_opti      | Tous les tests suivants -O1 |
| test_frontend      | minCaml -> asml             |
| test_backend       | asml -> arm -O0             |
| test_backend_opti  | asml -> arm -O1             |
| test_parser        | parsing                     |
| test_gen-code      | minCaml -> arm -O0          |
| test_gen-code_opti | minCaml -> arm -O1          |
| test_typechecking  | typecheking                 |


