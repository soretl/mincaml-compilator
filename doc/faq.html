<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <title>faq</title>
  <style type="text/css">
      code{white-space: pre-wrap;}
      span.smallcaps{font-variant: small-caps;}
      div.line-block{white-space: pre-line;}
      div.column{display: inline-block; vertical-align: top; width: 50%;}
  </style>
  <link rel="stylesheet" href="style.css">
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="faq">FAQ</h1>
<h1 id="front-end">Front-end</h1>
<h2 id="do-i-need-to-create-a-new-datatype-for-k-normal-form-and-asml">Do I need to create a new datatype for K-normal form and ASML?</h2>
<p>You’ll need a datatype for ASML.</p>
<p>K-normal forms are a strict subset of the source language of MinCaml. Hence you can reuse the MinCaml AST to encode K-normal programs. However, it’s interesting to have a new datatype that will prevent you from defining ill-formed K-normal form accidentally. On the downside, defining a new AST takes additional work (especially in Java) but it’s probably a good idea nonetheless.</p>
<h2 id="i-need-more-explanation-on-closure-conversion">I need more explanation on closure conversion</h2>
<h3 id="direct-function-calls">Direct function calls</h3>
<p>First, restrict your compiler to MinCaml code that only uses <em>direct</em> function calls. A function call is direct if we know what function is called from the app node.</p>
<p>For instance, in</p>
<pre><code>let rec f x = print_int x in
f 0</code></pre>
<p>The call <code>f 0</code> refers to the function <code>f</code> defined above. This wouldn’t be the case with a call of the form <code>((g 0) 1)</code>. We also need to assume that function don’t use free variables (i.e. variable defined outside their scope). With these restrictions, closure conversion is straightforward. There are no closures!</p>
<p>In that case, you traverse the AST and generate a label from each function name. (this label will later be used in the ARM assembly code to identify the function address).</p>
<p>For each function call <code>f x y ...</code>. If <code>f</code> corresponds to a function already seen, generate an <code>apply_direct</code> instruction to the label corresponding to <code>f</code>. Otherwise, <code>f</code> should be an external function. In that case, create the label by appending <code>_min_caml_</code> to the name.</p>
<p>In ASM generation, <code>apply_direct</code> will be translated into ASML <code>call</code>. Then the backend will simply translate this call to a branching instruction. References to <code>_min_caml_</code> symbols will be resolved by the linker. <!-- 
Example:
```
let rec u x =                                                                    
  let rec v x = u x in v 0                                                     
in u 0  
````
is transformed to ASML program:
```
let _v x =                                                                   
    call _u x                                                                 

let _u y =                                                                   
    let w = 0 in                                                              
    call _v w                                                               

let _ =                                                                          
    let t = 0 in                                                              
    call _u t                                                               
```

Only when direct function calls work (*including ASML and ARM generation*),
start extending this to closures. --></p>
<h3 id="closures">Closures</h3>
<p>The previous scheme is ineffective in several cases. We already explained why in the <a href="./frontend.html">frontend</a> document. We give more detail on how to implement the closure conversion phase (see also 4.10 in the article). We want generalize the closure-conversion described above for direct calls to all types of functions.</p>
<p>Consider this example:</p>
<pre><code>let rec succ x = 
  let rec add y = x + y in 
  add 1 
in
print_int (succ 42)</code></pre>
<p>The previous algorithm isn’t working anymore. We can’t simply “extract” <code>add</code> from <code>succ</code> because it uses <code>x</code> that only exists in the context of <code>succ</code>.</p>
<p>To solve the issue, when processing the <code>let rec add</code> node, we determine the free variables appearing in <code>add</code> and separate them from the parameters.</p>
<pre><code>label: _add 
  free variables: x
  parameters: y
  code:
    x + y</code></pre>
<p>For <code>succ</code>, we should get something like</p>
<pre><code>label: _succ 
  free variables: None
  parameters: x
  code:
     ...</code></pre>
<p>The code part <code>add 1</code> should be converted to some sort of call to <code>_add</code> but it can’t be a direct call, as we also need to provide the value of the free variable <code>x</code>.</p>
<p>The trick is to translate <code>add 1</code> into two operation:</p>
<ul>
<li>a closure creation (make_closure). <code>add</code> is now a closure made of the label <code>_add</code> and the value of free variable <code>x</code>.</li>
<li>an apply closure operation for <code>add 1</code>.</li>
</ul>
<pre><code>label: _succ 
  free variables: None
  parameters: x
  code:
    let add = make_closure(_add, x) in
    let w = 1 in
    apply_closure(add, w)</code></pre>
<p>Here is what the corresponding ASML may look like.</p>
<pre><code>let _add.8 y.9 =
   let x.5 = mem(%self + 4) in
   add x.5 y.9

let _succ.4 x.5 =
   let add.8 = new 8 in
   let l.12 = _add.8 in
   let tu13 = mem(add.8 + 0) &lt;- l.12 in
   let tu11 = mem(add.8 + 4) &lt;- x.5 in
   let ti3.10 = 1 in
   call_closure add.8 ti3.10

let _ = 
   let ti1.7 = 42 in
   let ti2.6 = call _succ.4 ti1.7 in
   call _min_caml_print_int ti2.6</code></pre>
<p><code>make_closure</code> in <code>succ</code> has been translated to a memory allocation followed by two updates in that structure. Moreover, free variable <code>x</code> in <code>add</code> is looked up in the closure. <code>%self</code> is an implicit parameter that contains the address of the closure. In ARM, it will have to be passed explicitely by the generated code for <code>call_closure</code>.</p>
<h3 id="remark">Remark</h3>
<p><code>make_closure</code>, <code>apply_direct</code> and <code>apply_closure</code> correspond to nodes that are introduced in the closure conversion step. We use a syntactic notation in the examples, but you don’t have to produce this type of output. Eventually, <code>apply_direct</code> and <code>apply_closure</code> will be converted to <code>call</code> and <code>call_closure</code> instructions in the last phase of the front-end.</p>
<h2 id="what-is-the-13-bit-immediate-optimization-phase-mentioned-in-the-article">What is the 13-bit immediate optimization phase mentioned in the article?</h2>
<p>After k-normalization, all operations are of the form: op(x1,..,xn): They operate on variables, and not on litterals. However, in SPARC assembly (the target in the article), most integer instructions can take as operands immediate values if they need less than 13 bits. This phase is an optimization that uses those instructions (for instance, <code>MOV r0, #1; ADD r1, r0</code> is rewritten to <code>ADD r1, #1</code>). In ARM, there are different constraints on immediate values, but you should be able to perform similar operations. It is not required though.</p>
<h1 id="back-end">Back-end</h1>
<h2 id="do-i-need-to-write-a-parser-for-asml">Do I need to write a parser for ASML?</h2>
<p>No. It is useful if you want to test the backend before the frontend is working, but it’s not required.</p>
<h2 id="code-generation-for-function-calls">Code generation for function calls</h2>
<p>We explain here a bit more the calling conventions in ARM code. In ASML, functions can be called using the <code>call</code> instruction. For instance,</p>
<pre><code> let _g x1 = 
   let x2 = 1 in
   let x3 = 2 in 
   let x = call _f x1 x2 x3  
   ...
   ...

 let _f u v w = 
    ...
    ...</code></pre>
<p>We say that <code>_g</code> is the caller and <code>_f</code> is the callee.</p>
<p>The generated ARM code for <code>call</code> should copy the parameters in <code>r0</code> to <code>r3</code>, and on the stack if there are more than four parameters. In the example, <code>x1</code>, <code>x2</code> and <code>x3</code> will be placed in <code>r0</code>, <code>r1</code>, <code>r2</code>.</p>
<p>The code generated for <code>_f</code> should contain a <em>prologue</em> (first instructions) and an <em>epilogue</em> (last instructions).</p>
<h4 id="prologue">Prologue</h4>
<pre><code>stmfd  sp!, {fp, lr}   # save fp and lr on the stack
add fp, sp, #4         # position fp on the address of old fp
sub sp, #n             # allocate memory to store local variables</code></pre>
<h4 id="epilogue">Epilogue</h4>
<p>The epilogue restore <code>sp</code>, <code>fp</code> and <code>lr</code> and returns to caller</p>
<pre><code>sub sp, fp, #4         
ldmfd  sp!, {fp, lr}  
bx lr                  </code></pre>
<p>After the prologue, the instructions contained in the function body can access the parameters and the local variables using <code>fp</code>. The stack should have the following shape (remember that pushing on the stack means decreasing <code>sp</code>).</p>
<pre><code>#  (higher addresses)
#    param_n                          
#    ...
#    param_5  &lt;- 4(fp) 
#    old_fp  &lt;- fp
#    lr     
#    temp1   &lt;- -8(fp)
#    temp2   &lt;- -12(fp)
#    ...
#    tempn   &lt;- SP
#    (saved local registers)
#  (lower addresses)   </code></pre>
<p>The function body can access parameters using <code>r0</code> to <code>r3</code> and (if needed) addresses <code>4(fp)</code>, <code>8(fp)</code> and so on. Local variables can be accessed using <code>-8(fp)</code>, <code>-12(fp)</code> and so on.</p>
<p>The first instructions in the function body after the prologue should copy the parameters contained in <code>r0</code> to <code>r3</code> to the temporary variables in the stack or in the local registers <code>r4</code> to <code>r12</code>, depending on your register allocation strategy. After that registers <code>r0</code> to <code>r3</code> can be used as <em>scratch registers</em>. The callee doesn’t have to save them hence they can be modifed without being restored. However, if you use local registers <code>r4</code> to <code>r12</code> they should be be saved on the stack right after the prologue (<em>callee-saved registers</em>), and restored just before the epilogue. Be careful though with <code>r11</code> which is register <code>fp</code>.</p>
<h2 id="what-register-allocation-algorithm-should-i-use">What register allocation algorithm should I use?</h2>
<p>Use the basic allocation algorithm that spills everything. It is simple and sufficient to generate correct ARM code. Other algorithms are possible extensions. If you do implement other algorithms, add a command line option so that the use can choose between the algorithms.</p>
<h1 id="typing">Typing</h1>
<h2 id="what-is-option-ref-in-the-ocaml-type.t-datatype">What is <code>option ref</code> in the OCaml <code>Type.t</code> datatype?</h2>
<p>In the provided OCaml implementation, the <code>Type.t</code> datatype has a case <code>Var of t option ref</code>.</p>
<pre><code>type t =
| Unit
| Bool
| Int
| Float     
| Fun of t list * t    
| Tuple of t list
| Array of t   
| Var of t option ref </code></pre>
<p>This is done to implement type checking more efficiently. However, to simplify your typing algorithm, you can instead use <code>Var of string</code> and generate <em>fresh</em> type variables in the parser whenever you have to provide a type. Modify <code>Type.gentyp()</code> so that it returns a new variable name each time it is called (this is already the case in the java implementation).</p>
<h2 id="partial-application-in-mincaml">Partial application in mincaml</h2>
<!-- 
The case `Fun of t list * t` represents types of the form
`t1 -> (t2 -> (t3 -> ... -> tn))`. For instance, `Fun ([Int; Bool], Unit)`
represents the type `Int -> Bool -> Unit` (don't confuse it with 
`Int * Bool -> Unit`). -->
<p>In OCaml, one can use partial application such as:</p>
<pre><code>let rec f x y z = x + y + z in
let rec u x = f x 42 in   (* u has type int -&gt; (int -&gt; int) *)
()</code></pre>
<p>However, this is forbidden in MinCaml and should be written:</p>
<pre><code>let rec f x y z = x + y + z in
let rec u x z = f x 42 z in
()</code></pre>
<h3 id="what-is-the-difference-between-monomorphism-and-polymorphism">What is the difference between monomorphism and polymorphism</h3>
<p>Consider</p>
<pre><code>Let f x = () in (f 1) + (f true)</code></pre>
<p>In OCaml, function <code>f</code> has <em>polymorphic</em> type <code>'a -&gt; unit</code>. Mincaml only has <em>monomorphic</em> types and this program is not well-typed as the typechecker will generate two incompatible equations (type of <code>x</code> should be <code>int</code> and <code>bool</code>).</p>
<p>See <a href="https://en.wikipedia.org/wiki/Hindley–Milner_type_system#Algorithm_W">Hindly Milner</a> for a polymorphic typechecking algorithm. You can implement it as an extension.</p>
<h3 id="what-equations-should-we-generate-for-let-rec-and-app">What equations should we generate for <code>let-rec</code> and <code>app</code>?</h3>
<p>These cases were left as exercises in the <code>GenEquations</code> algorithm (typing document).</p>
<pre><code>GenEquations(env, expr, type) =
  case on expr:
  ...
  App ... -&gt;
  Letrec ...  -&gt;  </code></pre>
<p>Remember that <code>GenEquations</code> returns a list of equations that hold if and only if term <em>expr</em> has type <em>type</em> in environment <em>environment</em>.</p>
<p>We explain here the missing cases for functions and application with <em>one</em> parameter. You’ll have to generalize this to several parameters.</p>
<p>Suppose <code>App</code> is of the form <code>(expr1 expr2)</code>. <code>expr1</code> must have type <code>A -&gt; type</code> for some type variable <code>A</code> and <code>expr2</code> must have type <code>A</code>. This gives:</p>
<pre><code>App (expr1, expr2) -&gt; 
    define A as a new variable
    eq1 = genEquations(env, expr1, A -&gt; type) 
    eq2 = genEquations(env, expr2, A)
    return eq1 :: eq2 // concatenation of eq1 and eq2  </code></pre>
<p>Suppose <code>Letrec</code> is of the form <code>Letrec (f, t2, x, t1, expr1, expr2)</code>. <code>t2</code> is the type of the value returned by <code>f</code>, <code>t1</code> is the type of <code>x</code>. (In mincaml the parser fills these two types with type variables, but they could be types specified by the user.)</p>
<p><code>expr2</code> must have type <code>type</code> in the environment <code>env</code> extended with <code>f : t1 -&gt; t2</code>. <code>expr1</code> must have type <code>t2</code> in the environment <code>env</code> extended with <code>x : t1</code>.</p>
<pre><code>Letrec (f, t2, x, t1, expr1, expr2) -&gt;
   eq1 = genEquations(env + (f : t1 -&gt; t2), expr2, type)
   eq2 = genEquations(env + (x : t1), expr1, t2)
   return eq1 :: eq2 // concatenation of eq1 and eq2  </code></pre>
<p>The operation <code>env + (x : t)</code> masks any previous definition of <code>x</code> in <code>env</code>. Environments can be implemented as linked lists of associations. New associations are inserted on the head of the list. Lookup (in <code>Var</code> rule) returns the first association.</p>
<h3 id="do-we-really-need-this-complex-typing-algorithm">Do we really need this complex typing algorithm?</h3>
<p>In simple case, one can compute function types by simple tree walking and type propagation from the leaves to the root. For instance, one can easily infer that function <code>fun x -&gt; x + 1</code> has type <code>int -&gt; int</code>. This strategy doesn’t work on the following program.</p>
<pre><code>let f g = g 1 2 in
let h a b = a + b in
print_int (f h)</code></pre>
<h1 id="general-questions">General questions</h1>
<h2 id="do-we-have-to-implement-string-or-float-types">Do we have to implement string or float types?</h2>
<p>No. String or floats may still be mentioned in the documentation as they were part of previous years instances of this project but you don’t have to implement them. They can be implemented as extensions if everything else is working.</p>
<h2 id="what-library-functions-should-be-implemented.">What library functions should be implemented.</h2>
<p>Only <code>print_int</code> is mandatory. Other functions are optional and can be seen as extensions.</p>
<h2 id="can-i-extend-the-asml-language">Can I extend the ASML language?</h2>
<p>The ASML language is sufficient to encode simple features of MinCaml programs such as arithmetic expressing, functions, closures, tuples. However, you’ll probably need additional instructions for other features.</p>
<p>To implement arrays efficiently in ARM, you can use load and store operations with register offset and shifting. For instance:</p>
<pre><code>STR R0, [R1, R2, LSL #2] ; Stores R0 to an address equal to sum of R1
                         ; and four times R2.</code></pre>
<p>This is useful for array access (e.g <code>a.(i)</code> in mincaml). There is no corresponding instruction in ASML, but you can define an instruction <code>mem(x + y, n) &lt;- z</code>.</p>
<p>Another limitation of ASML concerns “big” integers. In ARM, not all 32-bits integers can be used as immediate values. For instance <code>mov r0, #1456</code> is not permitted. Such integers can be put in the data section but you’ll have to extend ASML if you want to perform this operation in the front-end.</p>
<p>If you add new instructions, you can add a warning when generating the ASML (with <code>-asml</code> option) to say that your generated ASML is not supported by the simulator.</p>
<h2 id="i-want-to-work-on-the-float-extension.-what-representation-of-float-should-i-use">I want to work on the float extension. What representation of float should I use?</h2>
<p>Use a 32-bit representation for floats. This simplifies code generation as all MinCaml values fit in one word of memory (otherwise, one would need to use typing information to know the size of values in order to generate appropriate code).</p>
<p>Also, the type-checker doesn’t need to decorate the AST with types as it is not needed (this is a simplification with respect to the article). The drawback is that MinCaml will differ from OCaml on operations involving floats.</p>
<h2 id="how-to-set-up-an-eclipsenetbeans-project-can-i-compile-my-project-using-antmaven">How to set up an Eclipse/Netbeans/… project? Can I compile my project using Ant/Maven/…?</h2>
<p>Use any editor/IDE you are able to use. For compiling, the only constraint is that we can build your project using <code>make</code> at the root of your repository on UGA servers.</p>
<p>Maven is a popular choice to build Java programs. You are encouraged to use it if you can.</p>
<h2 id="do-i-need-to-use-junit-for-testing-in-java-or-ounit-in-ocaml">Do I need to use JUnit for testing in Java (or OUnit in OCaml)</h2>
<p>There are several types of tests, notably</p>
<ul>
<li>unit test: to test one class or one function,</li>
<li>system test: for testing the whole system.</li>
</ul>
<p>We focus on system tests in this project. System tests work by:</p>
<ul>
<li>giving an input (in this project, a mincaml program)</li>
<li>running the compiler (possibly with an option such as -asml, -t …)</li>
<li>checking the result (for instance, checking the <a href="https://shapeshed.com/unix-exit-codes/">exit code</a> or running the generated program)</li>
</ul>
<p>To automatize system tests you need to use scripting language. You can extend the bash script given in the archive or write your own python scripts.</p>
<p>JUnit/OUnit is useful for unit testing. It’s great if you use it but it’s less important.</p>
<h2 id="can-you-provide-examples-of-arm-code-generated-from-mincaml-programs">Can you provide examples of ARM code generated from mincaml programs?</h2>
<p>Here are <a href="./examples-ARM.tar.gz">two examples</a> of ARM programs generated From mincaml programs. You can compile them with <code>make</code> and run them with <code>make test</code>.</p>
<p>In these examples, we use the basic register allocation strategy. All parameters and local variables are stored in the stack at a fixed offset from the <em>frame pointer</em>. Whenever variables or parameters are needed for an operation, they are copied from the stack to registers. In your code generation algorithm, you simply need to maintain a table that maps a parameter or a local variable to an offset in the stack</p>
<p>The second example uses tuples. See how tuples are allocated in a static area of memory (the <em>heap</em>) allocated in <code>libmincaml.S</code>. <code>_heap_ptr</code>, also defined in <code>libmincaml.S</code> points to the first available word of the heap.</p>
<h3 id="important-note">Important note</h3>
<p>To compile these examples, we use a different <code>libmincaml.S</code> file and different linker options than what we used for the <code>ARM/helloworld.s</code>. It’s important to understand that An ARM executable must define symbols <code>_start</code> and <code>_exit</code> as entry and exit points of the program. For simplicity, we define them directly in <code>ARM/helloworld.s</code>, but <em>not</em> in <code>example1.s</code> or <code>example2.s</code>. For these two examples, we rely instead on a library provided by <code>gcc</code>. This library defines <code>_start</code> and <code>_exit</code> in a way that interacts nicely with a <code>main</code> function. In particular, it sets the command line parameters that may be used by <code>main</code>, and it uses the value returned by <code>main</code> as the exit value of the process.</p>
<p>For your compiler, you can do either way, as long as you generate ARM code with the right behavior.</p>
<h2 id="why-cant-we-allocate-arrays-on-the-stack">Why can’t we allocate arrays on the stack?</h2>
<p>Consider this program</p>
<pre><code>let rec f _ = 
    Array.create 10 () in
let rec g _ = 
  (f ()).(0) in
g ()</code></pre>
<p><code>f</code> returns an array (which is subsequently used in <code>g</code>). If the array is on the stack, we can’t return its address as it is no longer valid after <code>f</code> returns.</p>
<p>It is the same issue you face in <code>C</code> with</p>
<pre><code>int *f() {
   int t[10];
   return t;
}</code></pre>
<p>Indeed, <code>gcc</code> complains with</p>
<pre><code>warning: address of stack memory associated with local variable &#39;t&#39; returned
   return t;
          ^</code></pre>
<p>The strategy used by mincaml is to represent arrays (and closures, tuples) as addresses on heap-allocated structures.</p>
</body>
</html>
