#! /bin/sh

root="$(dirname "$0")"/..
cd $root || exit 1

OCAML=/usr/bin/ocaml
MINCAMLC=$root/java/mincamlc

LINES0=0
LINES1=0
PERCENTAGE=0
N=0
TOT=0

cd ARM/

for test_case in $root/tests/gen-code/*.ml
do

    $MINCAMLC "$test_case" -o res0.s -O 0
    $MINCAMLC "$test_case" -o res1.s -O 1

    if [ -f res0.s ] && [ -f res1.s ]
    then
        LINES0=$(wc -l < res0.s)
        LINES1=$(wc -l < res1.s)
        PERCENTAGE=$(($LINES1-$LINES0/$LINES1))

        if [ $LINES0 -gt 100 ]
        then
            echo $(basename $test_case)
            echo Spill       : "$LINES0"
            echo Linear Scan : "$LINES1"
            echo Gain        : "$PERCENTAGE"%

            N=$(($N+1))
            TOT=$(($TOT+$PERCENTAGE))
        fi
    else
        echo $(basename $test_case) -- ERROR
    fi
    rm res0.s res1.s 2> /dev/null
done

echo Gain moyen : "$(($TOT/$N))"%
