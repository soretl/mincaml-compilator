#! /bin/sh

root="$(dirname "$0")"/..
cd $root || exit 1

OCAML=/usr/bin/ocaml
ASML=tools/asml
MINCAMLC=java/mincamlc

for test_case in tests/gen-code/*.ml
do
    $OCAML "$test_case" 2> /dev/null 1> expected.out~
    $MINCAMLC "$test_case" -o res.asml -frontend -O 1
    $ASML res.asml > res.out~
    if diff expected.out~ res.out~ && [ -f res.asml ] && [ -f res.out~ ] && [ -f expected.out~ ]

    then
        echo -n  "	[\033[32mOK\033[0m]	- "
    else
        echo -n "	[\033[31mFAILED\033[0m]	- "
    fi
    echo "testing arm generation on: "$(basename $test_case)
    rm expected.out~ res.out~ res.asml 2> /dev/null
done

