#! /bin/sh

root="$(dirname "$0")"/..
cd $root || exit 1

ASML=$root/tools/asml
MINCAMLC=$root/java/mincamlc

cd ARM/

for test_case in $root/tests/backend/*.asml
do
    $ASML "$test_case" 2> /dev/null 1> expected.out~
    if [ "$#" -ne 1 ]; then
        $MINCAMLC "$test_case" -o res.s -backend -O 0
    else
        $MINCAMLC "$test_case" -o res.s -backend -O "$1"
    fi
    make res.arm 2> /dev/null 1> /dev/null
    qemu-arm ./res.arm > res.out~
    if diff expected.out~ res.out~ && [ -f res.s ] && [ -f res.out~ ] && [ -f expected.out~ ]
    then
        echo -n  "	[\033[32mOK\033[0m]	- "
    else
        echo -n "	[\033[31mFAILED\033[0m]	- "
    fi
    echo "testing arm generation on: "$(basename $test_case)
    rm expected.out~ res.out~ res.s res.arm 2> /dev/null
done

