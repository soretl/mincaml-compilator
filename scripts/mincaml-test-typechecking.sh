#! /bin/bash
cd "$(dirname "$0")"/.. || exit 1

# TODO change this to point to your mincamlc executable if it's different, or add
# it to your PATH. Use the appropriate option to run the parser as soon
# as it is implemented
MINCAMLC=java/mincamlc

# run all test cases in syntax/valid and make sure they are parsed without error
# run all test cases in syntax/invalid and make sure the parser returns an error

# TODO extends this script to run test in subdirectories
#

echo ""
echo ""
echo "[TESTS VALIDES]"
for test_case in tests/typechecking/valid/*.ml
do
    
    if $MINCAMLC "$test_case" "-t" 2> /dev/null 1> /dev/null
    then
        echo -e -n "	[\e[32mOK\e[0m]	- "
    else
        echo -e -n "	[\e[31mNAN\e[0m]	- "
    fi
    echo "testing typechecking on: $test_case"
done
echo ""
echo ""
echo ""
echo ""
echo "[TESTS INVALIDES]"
for test_case in tests/typechecking/invalid/*.ml
do
    
    if $MINCAMLC "$test_case" "-t" 2> /dev/null 1> /dev/null
    then
        echo -e -n "	[\e[31mNAN\e[0m]	- "
    else
        echo -e -n "	[\e[32mOK\e[0m]	- "
    fi
    echo "testing typechecking on: $test_case"
done

