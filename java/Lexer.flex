import java_cup.runtime.*;
      
%%
   
/* -----------------Options and Declarations Section----------------- */
   
%class Lexer

/*
  The current line number can be accessed with the variable yyline
  and the current column number with the variable yycolumn.
*/
%line
%column
    
/* 
   Will switch to a CUP compatibility mode to interface with a CUP
   generated parser.
*/
%cup
   
/*
  Declarations
   
  Code between %{ and %}, both of which must be at the beginning of a
  line, will be copied letter to letter into the lexer class source.
  Here you declare member variables and functions that are used inside
  scanner actions.  
*/
%{   
    /* To create a new java_cup.runtime.Symbol with information about
       the current token, the token will have no value in this
       case. */
    private Symbol symbol(int type) {
        return new Symbol(type, yyline, yycolumn);
    }
    
    /* Also creates a new java_cup.runtime.Symbol with information
       about the current token, but this object has a value. */
    private Symbol symbol(int type, Object value) {
        return new Symbol(type, yyline, yycolumn, value);
    }
%}
   

/*
  Macro Declarations
  
  These declarations are regular expressions that will be used latter
  in the Lexical Rules Section.  
*/
   
space = [ \t\n\r]
digit = [0-9]
lower = [a-z]
upper = [A-Z]
comment =  "(*" [^*] ~"*)" 
%%
/* ------------------------Lexical Rules Section---------------------- */
   
/*
   This section contains regular expressions and actions, i.e. Java
   code, that will be executed when the scanner matches the associated
   regular expression. */
   
   /* YYINITIAL is the state at which the lexer begins scanning.  So
   these regular expressions will only be matched if the scanner is in
   the start state YYINITIAL. */
   
<YYINITIAL> {

{space}+  {  }
{comment}   {  }
"("     { return symbol(sym.LPAREN); }
")"     { return symbol(sym.RPAREN); }
"true"  { return symbol(sym.BOOL, true); }
"false" { return symbol(sym.BOOL, false); }

"()"	{ return symbol(sym.NIL); }
"("     { return symbol(sym.LPAREN); }
")"     { return symbol(sym.RPAREN); }
"+"     { return symbol(sym.PLUS); }
"="     { return symbol(sym.EQUAL); }
"=."    { return symbol(sym.FEQUAL); }
"<="    { return symbol(sym.LE); }
"<=."   { return symbol(sym.FLE); }
">="    { return symbol(sym.GE); }
"if"    { return symbol(sym.IF); }
"then"  { return symbol(sym.THEN); }
"else"  { return symbol(sym.ELSE); }
"let"   { return symbol(sym.LET); }
"in"    { return symbol(sym.IN); }
"."    	{ return symbol(sym.DOT); }
"neg"   { return symbol(sym.NEG); }
"fneg"  { return symbol(sym.FNEG); }
"mem"   { return symbol(sym.MEM); }
"fmul"  { return symbol(sym.FMUL); }
"fdiv"  { return symbol(sym.FMUL); }
"fsub"  { return symbol(sym.FSUB); }
"fadd"  { return symbol(sym.FADD); }
"<-"    { return symbol(sym.ASSIGN); }
"add"   { return symbol(sym.ADD); }
"sub"   { return symbol(sym.SUB); }
"new"	{ return symbol(sym.NEW); }
"call"	{ return symbol(sym.CALL); }
"nop"   { return symbol(sym.NOP ); }
"call_closure" { return symbol(sym.APPCLO); }
"_"     { return symbol(sym.UNDERSC); }
eof     { return symbol(sym.EOF); }

{digit}+  { return symbol(sym.INT, new Integer(yytext())); }
{digit}+ ("." {digit}*)? (["e" "E"] ["+" "-"]? digit+)?  
        { return symbol(sym.FLOAT, new java.lang.Float(yytext())); } 

"u" ({digit})+  { return symbol(sym.IDENT, new Ident(Id.genUnused(yytext()))); }
{lower} ({digit}|{lower}|{upper}|"_")*   { return symbol(sym.IDENT, new Ident(Id.genStandard(yytext()))); }
"%self" {return symbol(sym.IDENT, new Ident(Id.genSelf())); }
"_" ({digit}|{lower}|{upper}|"_")*   { return symbol(sym.LABEL, new Label(Id.genLabel(yytext()))); }
}
[^]                    { throw new Error("Illegal character <"+yytext()+">"); }



