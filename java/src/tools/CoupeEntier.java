package tools;

public class CoupeEntier {
	
	//renvoie le plus grand n tel que  max<2^n<val
	 public static Pair<Integer,Integer> Coupe(int val,int min) {
		 int i = 0;
		 while(val>min) {
			 val = val >> 8;
			 i+=8;
		 }
		 return new Pair<Integer,Integer>(val,i);
	 }
}

//0 1 2 3 4  5  6  7   8
//1 2 4 8 16 32 64 128 256