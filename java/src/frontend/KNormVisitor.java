package frontend;

import java.util.*;

import mincaml.type.Type;
import mincaml.tree.Add;
import mincaml.tree.App;
import mincaml.tree.Apply_closure;
import mincaml.tree.Apply_direct;
import mincaml.tree.Array;
import mincaml.tree.Bool;
import mincaml.tree.Eq;
import mincaml.tree.Exp;
import mincaml.tree.FAdd;
import mincaml.tree.FDiv;
import mincaml.tree.FMul;
import mincaml.tree.FNeg;
import mincaml.tree.FSub;
import mincaml.tree.Float;
import mincaml.tree.FunDef;
import mincaml.tree.Get;
import mincaml.tree.Id;
import mincaml.tree.If;
import mincaml.tree.Int;
import mincaml.tree.LE;
import mincaml.tree.Let;
import mincaml.tree.LetRec;
import mincaml.tree.LetTuple;
import mincaml.tree.Make_closure;
import mincaml.tree.Neg;
import mincaml.tree.Not;
import mincaml.tree.Put;
import mincaml.tree.Sub;
import mincaml.tree.Tuple;
import mincaml.tree.Unit;
import mincaml.tree.Var;
import mincaml.visitor.ObjVisitor;
import mincaml.visitor.PrintVisitor;

/**
 * Cette classe décompose toutes les variables
 * imbriquées dans de nouvelles variables, pour
 * se rapprocher du langage assembleur
 */
public class KNormVisitor implements ObjVisitor<Exp> {

    public Unit visit(Unit e) {
        return e;
    }

    public Bool visit(Bool e) {
        return e;
    }

    public Int visit(Int e) {
        return e;
    }

    public Float visit(Float e) { 
        return e;
    }

    /**
     * K-normalise l'expression Not e en créant un Let
     * @param e
     * @return  e K-normalisée
     */
    public Let visit(Not e) {
        Exp e1 = e.e.accept(this);
        Id new_var = Id.gen();
        Type new_type = Type.gen();
        return new Let(new_var, new_type, e1, new Not(new Var(new_var))) ;
    }

    /**
     * K-normalise l'expression Neg e en créant un Let
     * @param e
     * @return  e K-normalisée
     */
    public Let visit(Neg e) {
        Exp e1 = e.e.accept(this);
        Id new_var = Id.gen();
        Type new_type = Type.gen();
        return new Let(new_var, new_type, e1, new Neg(new Var(new_var))) ;
    }

    /**
     * K-normalise l'expression Add e en créant un Let
     * @param e
     * @return  e K-normalisée
     */
    public Let visit(Add e) {
        Exp e1 = e.e1.accept(this);
        Exp e2 = e.e2.accept(this);
        Id new_var1 = Id.gen();
        Type new_type1 = Type.gen();
        Id new_var2 = Id.gen();
        Type new_type2 = Type.gen();
        Let res = new Let(new_var1, new_type1, e1,
                  new Let(new_var2, new_type2, e2,
                    new Add(new Var(new_var1), new Var(new_var2))));
        return res;
    }

    /**
     * K-normalise l'expression Sub e en créant un Let
     * @param e
     * @return  e K-normalisée
     */
    public Let visit(Sub e) {
	Exp e1 = e.e1.accept(this);
        Exp e2 = e.e2.accept(this);
        Id new_var1 = Id.gen();
        Type new_type1 = Type.gen();
        Id new_var2 = Id.gen();
        Type new_type2 = Type.gen();
        Let res = new Let(new_var1, new_type1, e1,
                  new Let(new_var2, new_type2, e2,
                    new Sub(new Var(new_var1), new Var(new_var2))));
        return res;
    }

    /**
     * K-normalise l'expression FNeg e en créant un Let
     * @param e
     * @return  e K-normalisée
     */
    public Exp visit(FNeg e){
        Exp e1 = e.e.accept(this);
        Id new_var = Id.gen();
        Type new_type = Type.gen();
        return new Let(new_var, new_type, e1, new FNeg(new Var(new_var))) ;
    }

    /**
     * K-normalise l'expression FAdd e en créant un Let
     * @param e
     * @return  e K-normalisée
     */
    public Let visit(FAdd e){
        Exp e1 = e.e1.accept(this);
        Exp e2 = e.e2.accept(this);
        Id new_var1 = Id.gen();
        Type new_type1 = Type.gen();
        Id new_var2 = Id.gen();
        Type new_type2 = Type.gen();
        Let res = new Let(new_var1, new_type1, e1,
                  new Let(new_var2, new_type2, e2,
                    new FAdd(new Var(new_var1), new Var(new_var2))));
        return res;
    }

    /**
     * K-normalise l'expression FSub e en créant un Let
     * @param e
     * @return  e K-normalisée
     */
    public Let visit(FSub e){
        Exp e1 = e.e1.accept(this);
        Exp e2 = e.e2.accept(this);
        Id new_var1 = Id.gen();
        Type new_type1 = Type.gen();
        Id new_var2 = Id.gen();
        Type new_type2 = Type.gen();
        Let res = new Let(new_var1, new_type1, e1,
                  new Let(new_var2, new_type2, e2,
                    new FSub(new Var(new_var1), new Var(new_var2))));
        return res;
    }

    /**
     * K-normalise l'expression FMul e en créant un Let
     * @param e
     * @return  e K-normalisée
     */
    public Let visit(FMul e) {
        Exp e1 = e.e1.accept(this);
        Exp e2 = e.e2.accept(this);
        Id new_var1 = Id.gen();
        Type new_type1 = Type.gen();
        Id new_var2 = Id.gen();
        Type new_type2 = Type.gen();
        Let res = new Let(new_var1, new_type1, e1,
                  new Let(new_var2, new_type2, e2,
                    new FMul(new Var(new_var1), new Var(new_var2))));
        return res;
    }

    /**
     * K-normalise l'expression FDiv e en créant un Let
     * @param e
     * @return  e K-normalisée
     */
    public Let visit(FDiv e){
        Exp e1 = e.e1.accept(this);
        Exp e2 = e.e2.accept(this);
        Id new_var1 = Id.gen();
        Type new_type1 = Type.gen();
        Id new_var2 = Id.gen();
        Type new_type2 = Type.gen();
        Let res = new Let(new_var1, new_type1, e1,
                  new Let(new_var2, new_type2, e2,
                    new FDiv(new Var(new_var1), new Var(new_var2))));
        return res;
    }

    /**
     * K-normalise l'expression Eq e en créant un Let
     * Permet de déclarer une variable pour chaque expression du Eq,
     * puis d'appliquer le Eq sur ces variables
     * @param e
     * @return  e K-normalisée
     */
    public Let visit(Eq e){
        Exp e1 = e.e1.accept(this);
        Exp e2 = e.e2.accept(this);
        Id new_var1 = Id.gen();
        Type new_type1 = Type.gen();
        Id new_var2 = Id.gen();
        Type new_type2 = Type.gen();
        Let res = new Let(new_var1, new_type1, e1,
                  new Let(new_var2, new_type2, e2,
                    new Eq(new Var(new_var1), new Var(new_var2))));
        return res;
    }

    /**
     * K-normalise l'expression LE e en créant un Let
     * Permet de déclarer une variable pour chaque expression du LE,
     * puis d'appliquer le LE sur ces variables
     * @param e
     * @return  e K-normalisée
     */
    public Let visit(LE e){
        Exp e1 = e.e1.accept(this);
        Exp e2 = e.e2.accept(this);
        Id new_var1 = Id.gen();
        Type new_type1 = Type.gen();
        Id new_var2 = Id.gen();
        Type new_type2 = Type.gen();
        Let res = new Let(new_var1, new_type1, e1,
                  new Let(new_var2, new_type2, e2,
                    new LE(new Var(new_var1), new Var(new_var2))));
        return res;
    }

    /**
     * K-normalise l'expression If e en créant un Let 
     * dont toutes les expressions sont normalisées,
     * ordonne l'ordre des variables pour ne pas avoir
     * de déclaration de variables dans la condition 
     * du If
     * @param e
     * @return  e K-normalisée
     */
    public Exp visit(If e){
        Exp e1 = e.e1.accept(this);
        Exp e2;
        Exp e3;
        //Si la condition est sous forme de Let ou de Not, on la traite ainsi
        if(e1 instanceof Let || e1 instanceof Not){
            /* Adapte la condition en changeant l'ordre des expressions
               si nécessaire et en enlevant le Not, utilise
               les fonctions traiterNot et enleverNot */
            switch(traiterNot(e.e1, false)){    
                case 1: //Condition = Not Equals, équivaut à Diff
                    e1 = enleverNot(e.e1).accept(this);
                    e2 = e.e3.accept(this);
                    e3 = e.e2.accept(this);
                    break;
                case 3: //Condition = Not LE, équivaut à Sup
                    e1 = enleverNot(e.e1).accept(this);
                    e2 = e.e3.accept(this);
                    e3 = e.e2.accept(this);
                    break;
                default: //Pas de Not
                    e1 = e.e1.accept(this);
                    e2 = e.e2.accept(this);
                    e3 = e.e3.accept(this);
                    break;
            }
            /* On crée un let pour contenir le résultat du Let de la condition
               Dans le nouveau If qu'on crée, à la place du Let de la condition on mettra 
               Juste la variable qui contient son résultat */
            Let letcond = (Let) e1;
            Id stocke_resultat = Id.gen();
            Let new_let = new Let(stocke_resultat, Type.gen(), letcond, new If(new Var(stocke_resultat), e2, e3));
            return new_let;
        }else if(e1 instanceof App){    //Si la condition est sous forme de App, on la traite ainsi
            /* On crée un let pour stocker le App dans une variable, et on utilise
               cette variable dans le If que l'on crée à la suite du let */
            Id stocke_resultat = Id.gen();
            Let new_let = new Let(stocke_resultat, Type.gen(), e.e1.accept(this), new If(new Var(stocke_resultat), e.e2.accept(this), e.e3.accept(this)));
            return new_let;
        }else if(e1 instanceof Bool){   //Si la condition est sous forme de App, on la traite ainsi
            /* On crée un let pour stocker le Bool dans une variable, et on utilise
               cette variable dans le If que l'on crée à la suite du let */
            Id stocke_resultat = Id.gen();
            Let new_let = new Let(stocke_resultat, Type.gen(), e.e1.accept(this), new If(new Var(stocke_resultat), e.e2.accept(this), e.e3.accept(this)));
            return new_let;
        }
        else{   //Sinon, on crée juste un If contenant les expressions du If K-Normaliées
            If res = new If(e.e1.accept(this), e.e2.accept(this), e.e3.accept(this));
            e.e1.accept(this).accept(new PrintVisitor());
            return res;
        }
    }
    
    /**
     * Parcours une condition sous forme de Not Let
     * pour retrouver si la condition a valeur de Eq, Diff, Le ou Sup,
     * et renvoie un entier correspondant
     * @param e
     * @param notactif
     * @return 0:Eq, 1:Diff, 2:Le, 3:Sup, -1:Err
     */
    public int traiterNot(Exp e, Boolean notactif){
        if(e instanceof Not){
            Not eNot = (Not) e; 
            return traiterNot(eNot.e, !notactif);
        }
        else if(e instanceof Eq){
            if(notactif){
                return 1; //DIFF
            }else{
                return 0; //EQ
            }
        }
        else if(e instanceof LE){
            if(notactif){
                return 3; //SUP
            }else{
                return 2; //LE
            }
        }
        return -1;
    }
    
    /**
     * Enlève le not à l'expression donnée, 
     * renvoie donc l'expression contenue dans 
     * le Not
     * @param e
     * @return Expression contenue dans le Not 
     */
    public Exp enleverNot(Exp e){
        if(e instanceof Not){
            Not eNot = (Not) e; 
            return enleverNot(eNot.e);
        }else{
            return e;
        }
    }

    /**
     * K-normalise l'expression Let e en recréant un Let
     * dont les expressions sont normalisées
     * @param e
     * @return  e K-normalisée
     */
    public Let visit(Let e) {
        Id idLet = e.id;
        if(e.e1 instanceof Put){    //Les variables utilisées pour les Put ne seront pas nécessaires en mémoire, elles sont donc marquées unused
            idLet = Id.genUnused();
        }
        Let res = new Let(idLet, e.t, e.e1.accept(this), e.e2.accept(this));
        return res;
    }

    public Var visit(Var e){
        return e;
    }

    /**
     * K-normalise l'expression LetRec e en créant un Let
     * dont la fonction définie et l'expressions après 
     * le in sont normalisés
     * @param e
     * @return  e K-normalisée
     */
    public Exp visit(LetRec e){
        FunDef fd = new FunDef(e.fd.id, e.fd.type, e.fd.args, e.fd.e.accept(this));
        LetRec res = new LetRec(fd, e.e.accept(this));
        return res;
    }

    /**
     * K-normalise l'expression App e en créant un Let
     * dans lequel on appelle la fonction récursive appRec
     * pour traiter récursivement la liste d'expressions
     * @param e
     * @return  e K-normalisée
     */
    public Exp visit(App e){
        if(e.es.size()==1){ //Cas où fonction a un seul paramètre
            Exp temp = e.es.get(0);
            if(temp instanceof Unit){   //Si le seul paramètre est un Unit, rien à faire, c'est une fonction sans paramètres
                return e;
            }
        }
        List<Id> listeId = new ArrayList();
        if(e.e instanceof Var){
            Var v = (Var)e.e;
            return appRec(v.id, listeId, e.es);
        }else{
            Id new_var1 = Id.gen();
            Type new_type1 = Type.gen();
            Let res = new Let(new_var1, new_type1, e.e.accept(this), appRec(new_var1, listeId, e.es));
            return res;
        }
    }
    
    /**
     * Si il reste des expressions à traiter,
     * crée un identifiant pour l'expression
     * et l'enlève de la liste, puis s'appelle récursivement. 
     * Sinon, retourne l'application de 
     * toutes les expressions traitées.
     * @param idPrincipal   id de la première expression de l'application
     * @param ids   ids liste des ids des autres expressions traitées
     * @param es    liste des expressions non traitées
     * @return un let k-normalisé récursivement si non finis, une application sinon
     */
    public Exp appRec(Id idPrincipal, List<Id> ids, List<Exp> es){
        if(es.size()>=1){
            int i=0;
            Exp e = null;
            List<Exp> listeAncienne = es;
            List<Exp> listeRes = new ArrayList();
            for(Exp el:listeAncienne){
                if(i==0){
                    e = el;
                    i++;
                }
                else{
                    listeRes.add(el); 
                }
            }
            Id new_var1 = Id.gen();
            Type new_type1 = Type.gen();
            ids.add(new_var1);
            return new Let(new_var1, new_type1, e.accept(this), appRec(idPrincipal, ids, listeRes));
        }
        else{
            List<Exp> listeVar = new ArrayList();
            for(Id id:ids){
                listeVar.add(new Var(id));
            }
            App res = new App(new Var(idPrincipal), listeVar);
            return res;
        }
    }

    /**
     * K-normalise l'expression Tuple e en recréant un Let
     * dont les expressions sont normalisées, en utilisant
     * la fonction TupleRec
     * @param e
     * @return  e K-normalisée
     */
    public Let visit(Tuple e){
        Id new_id = Id.gen();
        Type new_type = Type.gen();
        return new Let(new_id, new_type, (Let)tupleRec(new ArrayList(), e.es), new Var(new_id));
    }
    
    /**
     * Si il reste des expressions à traiter,
     * crée un identifiant pour l'expression
     * et l'enlève de la liste, puis s'appelle récursivement. 
     * Sinon, retourne l'application de 
     * toutes les expressions traitées.
     * @param ids liste des identifiants des expressiosn déjà traitées récursivement
     * @param es liste des expressoins restantes à traiter
     * @return Une successions de Let avant un Tuple, pour K-normalizer ce tuple
     */
    public Exp tupleRec(List<Id> ids, List<Exp> es){
        if(es.size()>=1){
            int i=0;
            Exp e = null;
            List<Exp> listeAncienne = es;
            List<Exp> listeRes = new ArrayList();
            for(Exp el:listeAncienne){
                if(i==0){
                    e = el;
                    i++;
                }
                else{
                    listeRes.add(el); 
                }
            }
            Id new_var1 = Id.gen();
            Type new_type1 = Type.gen();
            ids.add(new_var1);
            return new Let(new_var1, new_type1, e.accept(this), tupleRec(ids, listeRes));
        }
        else{
            List<Exp> listeVar = new ArrayList();
            for(Id id:ids){
                listeVar.add(new Var(id));
            }
            return new Tuple(listeVar);
        }
    }
    
    /**
     * K-normalise l'expression LetTuple e en recréant un Let
     * dont les expressions sont normalisées
     * @param e
     * @return  e K-normalisée
     */
    public LetTuple visit(LetTuple e){
        LetTuple res = new LetTuple(e.ids, e.ts, e.e1.accept(this), e.e2.accept(this));
        return res;
    }

    /**
     * K-normalise l'expression Array e en recréant un Let
     * dont les expressions sont normalisées
     * @param e
     * @return  e K-normalisée
     */
    public Let visit(Array e){
        Exp new_valDefaut = e.e2;
        //Si la valeur par défaut est un unit, on la remplace par convention par un 0
        if(new_valDefaut instanceof Unit){
            new_valDefaut = new Int(0);
        }
        
        //La taille et la valeur par défaut sont stockées dans des variables, qu'on applique ensuite à l'Array
        Var var_taille = new Var(Id.gen());
        Var var_valDefaut = new Var(Id.gen());
        
        Let valDefaut = new Let(var_valDefaut.id, Type.gen(), new_valDefaut.accept(this), new Array(var_taille, var_valDefaut));
        Let taille = new Let(var_taille.id, Type.gen(), e.e1.accept(this), valDefaut);
        return taille;
   }

    /**
     * K-normalise l'expression Array e en recréant un Let
     * dont les expressions sont normalisées
     * @param e
     * @return  e K-normalisée
     */
    public Let visit(Get e){
        //Le tableau  et l'emplacement(offset) sont stockés dans des variables, qu'on applique ensuite au Get
        Var var_tableau = new Var(Id.gen());
        Var var_place = new Var(Id.gen());
        Let place = new Let(var_place.id, Type.gen(), e.e2.accept(this), new Get(var_tableau, var_place));
        Let tableau = new Let(var_tableau.id, Type.gen(), e.e1.accept(this), place);
        return tableau;
    }

    /**
     * K-normalise l'expression App e en recréant un Let
     * dont les expressions sont normalisées
     * @param e
     * @return  e K-normalisée
     */
    public Let visit(Put e){
        Exp e1 = e.e1.accept(this);
        Exp e2 = e.e2.accept(this);
        Exp e3 = e.e3.accept(this);

        Id new_var1 = Id.gen();
        Type new_type1 = Type.gen();
        Id new_var2 = Id.gen();
        Type new_type2 = Type.gen();
        Id new_var3 = Id.gen();
        Type new_type3 = Type.gen();

        Let res = 
        new Let(new_var1, new_type1, e1,
          new Let(new_var2, new_type2, e2,
              new Let(new_var3, new_type3, e3,
                new Put(new Var(new_var1), new Var(new_var2), new Var(new_var3)))));
        return res;
    }

    @Override
    public Exp visit(Apply_direct e) {
        return e;
    }
    
    @Override
    public Exp visit(Apply_closure e) {
        return e;
    }
    
    @Override
    public Exp visit(Make_closure e) {
        return e;
    }
}


