package frontend;

import java.util.ArrayList;
import java.util.List;

import mincaml.type.Type;
import mincaml.tree.Add;
import mincaml.tree.App;
import mincaml.tree.Apply_closure;
import mincaml.tree.Apply_direct;
import mincaml.tree.Array;
import mincaml.tree.Bool;
import mincaml.tree.Eq;
import mincaml.tree.Exp;
import mincaml.tree.FAdd;
import mincaml.tree.FDiv;
import mincaml.tree.FMul;
import mincaml.tree.FNeg;
import mincaml.tree.FSub;
import mincaml.tree.Float;
import mincaml.tree.FunDef;
import mincaml.tree.Get;
import mincaml.tree.Id;
import mincaml.tree.If;
import mincaml.tree.Int;
import mincaml.tree.LE;
import mincaml.tree.Let;
import mincaml.tree.LetRec;
import mincaml.tree.LetTuple;
import mincaml.tree.Make_closure;
import mincaml.tree.Neg;
import mincaml.tree.Not;
import mincaml.tree.Put;
import mincaml.tree.Sub;
import mincaml.tree.Tuple;
import mincaml.tree.Unit;
import mincaml.tree.Var;
import mincaml.visitor.ObjVisitor;

/**
 * Classe applatissant les Lets imbriqués
 */
public class Let_RedVisitor implements ObjVisitor<Exp>{
    
    /**
     * Algorithme de reduction des Lets imbriqués
     * Utilise la fonction insert
     * @param e Expression contenant possiblement des Lets imbriques
     * @return L'expression e traitée
     */
    public Exp reduction(Exp e){
        if(e != null){
            if(e instanceof If){
                If eIf = (If)e;
                If res = new If(eIf.e1, reduction(eIf.e2), reduction(eIf.e3));
                return res;
            }
            else if(e instanceof Let){
                Let eLet = (Let)e;
                return insert(eLet.e1, eLet);
            }
            else if(e instanceof LetRec){
                LetRec eLetR = (LetRec)e;
                FunDef fd = new FunDef(eLetR.fd.id, eLetR.fd.type, eLetR.fd.args, reduction(eLetR.fd.e));
                LetRec res = new LetRec(fd, reduction(eLetR.e));
                return res;
            }
            else if(e instanceof LetTuple){
                LetTuple eLetT = (LetTuple)e;
                //LetTuple res = new LetTuple(eLetT.ids, eLetT.ts, eLetT.e1, reduction(eLetT.e2));
                return insert(eLetT.e1, eLetT);
            }
        }
        return e;
    }
    
    /**
     * Part récursive de l'algorithme 
     * de reduction des Lets imbriqués
     * @param e Expression à traiter
     * @param original Premier Let de l'arborescence traitée
     * @return L'expression e traitée
     */
    private Exp insert(Exp e, Exp original){
        if(e != null){
            if(e instanceof Let){
                Let eLet = (Let)e;
                return new Let(eLet.id, eLet.t, eLet.e1, insert(eLet.e2, original));
            }
            else if(e instanceof LetRec){
                LetRec eLetR = (LetRec)e;
                return new LetRec(eLetR.fd, insert(eLetR.e, original));
            }
            else if(e instanceof LetTuple){
                LetTuple eLetT = (LetTuple)e;
                return new LetTuple(eLetT.ids, eLetT.ts, eLetT.e1, insert(eLetT.e2, original));
            }
            else if(e instanceof Exp){
                if(original instanceof Let){
                    return new Let(((Let)original).id, ((Let)original).t, e, reduction(((Let)original).e2));
                }else{
                    return new LetTuple(((LetTuple)original).ids, ((LetTuple)original).ts, e, reduction(((LetTuple)original).e2));
                }
            }
        }
        return e;
    }

    @Override
    public Exp visit(Unit e) {
        return e;
    }

    @Override
    public Exp visit(Bool e) {
        return e;
    }

    @Override
    public Exp visit(Int e) {
        return e;
    }

    @Override
    public Exp visit(Float e) {
        return e;
    }

    /**
     * Applique la Let reduction sur l'expression Not e
     * en l'appliquant récursivement à l'expression que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(Not e) {
        Not res = new Not(e.e.accept(this));
        return res;
    }

    /**
     * Applique la Let reduction sur l'expression Neg e
     * en l'appliquant récursivement à l'expression que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(Neg e) {
        Neg res = new Neg(e.e.accept(this));
        return res;
    }

    /**
     * Applique la Let reduction sur l'expression Add e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(Add e) {
        Exp e1 = e.e1.accept(this);
        Exp e2 = e.e2.accept(this);
        Add res = new Add(e1, e2);
        return res;
    }

    /**
     * Applique la Let reduction sur l'expression Sub e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(Sub e) {
        Exp e1 = e.e1.accept(this);
        Exp e2 = e.e2.accept(this);
        Sub res = new Sub(e1, e2);
        return res;
    }

    /**
     * Applique la Let reduction sur l'expression FNeg e
     * en l'appliquant récursivement à l'expression que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(FNeg e) {
        Exp e1 = e.e.accept(this);
        FNeg res = new FNeg(e1);
        return res;
    }

    /**
     * Applique la Let reduction sur l'expression FAdd e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(FAdd e) {
        Exp e1 = e.e1.accept(this);
        Exp e2 = e.e2.accept(this);
        FAdd res = new FAdd(e1, e2);
        return res;
    }

    /**
     * Applique la Let reduction sur l'expression FSub e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(FSub e) {
        Exp e1 = e.e1.accept(this);
        Exp e2 = e.e2.accept(this);
        FSub res = new FSub(e1, e2);
        return res;
    }

    /**
     * Applique la Let reduction sur l'expression FMul e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(FMul e) {
        Exp e1 = e.e1.accept(this);
        Exp e2 = e.e2.accept(this);
        FMul res = new FMul(e1, e2);
        return res;
    }

    /**
     * Applique la Let reduction sur l'expression FDiv e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(FDiv e) {
        Exp e1 = e.e1.accept(this);
        Exp e2 = e.e2.accept(this);
        FDiv res = new FDiv(e1, e2);
        return res;
    }

    /**
     * Applique la Let reduction sur l'expression Eq e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(Eq e) {
        Exp e1 = e.e1.accept(this);
        Exp e2 = e.e2.accept(this);
        Eq res = new Eq(e1, e2);
        return res;
    }

    /**
     * Applique la Let reduction sur l'expression LE e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(LE e) {
        Exp e1 = e.e1.accept(this);
        Exp e2 = e.e2.accept(this);
        LE res = new LE(e1, e2);
        return res;
    }

    /**
     * Applique la Let reduction sur l'expression If e
     * en l'appliquant récursivement aux expressions que
     * e contient, et en appliquant la methode reduction
     * au resultat
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(If e) {
        If res = new If(e.e1.accept(this), e.e2.accept(this), e.e3.accept(this));
        return reduction(res);
    }

    /**
     * Applique la Let reduction sur l'expression Let e
     * en l'appliquant récursivement aux expressions que
     * e contient, et en appliquant la methode reduction
     * au resultat
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(Let e) {
        Let res = new Let(e.id, e.t, e.e1.accept(this), e.e2.accept(this));
        return reduction(res);
    }

    @Override
    public Exp visit(Var e) {
        return e;
    }

    /**
     * Applique la Let reduction sur l'expression LetRec e
     * en l'appliquant récursivement aux expressions que
     * e contient, et en appliquant la methode reduction
     * au resultat
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(LetRec e) {
        FunDef fd = new FunDef(e.fd.id, e.fd.type, e.fd.args, e.fd.e.accept(this));
        LetRec res = new LetRec(fd, e.e.accept(this));
        return reduction(res);
    }

    /**
     * Applique la Let reduction sur l'expression LE e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(App e) {
        List<Exp> listeAncienne = e.es;
        List<Exp> listeRes = new ArrayList();
        for(Exp el:listeAncienne){
            listeRes.add(el.accept(this)); 
        }
        
        App res = new App(e.e.accept(this), listeRes);
        return res;
    }

    /**
     * Applique la Let reduction sur l'expression LE e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(Tuple e) {
        List<Exp> listeAncienne = e.es;
        List<Exp> listeRes = new ArrayList();
        for(Exp el:listeAncienne){
            listeRes.add(el.accept(this)); 
        }
        
        Tuple res = new Tuple(listeRes);
        return res;
    }

    /**
     * Applique la Let reduction sur l'expression LE e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(LetTuple e) {
        List<Id> listeAncienneId = e.ids;
        List<Id> listeResId = new ArrayList();
        for(Id id:listeAncienneId){
            listeResId.add(id); 
        }
        
        LetTuple res = new LetTuple(listeResId, e.ts, e.e1.accept(this), e.e2.accept(this));
        return reduction(res);
    }

    /**
     * Applique la Let reduction sur l'expression LE e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(Array e) {
        Array res = new Array(e.e1.accept(this), e.e2.accept(this));
        return res;
    }

    /**
     * Applique la Let reduction sur l'expression LE e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(Get e) {
        Get res = new Get(e.e1.accept(this), e.e2.accept(this));
        return res;
    }

    /**
     * Applique la Let reduction sur l'expression LE e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(Put e) {
        Put res = new Put(e.e1.accept(this), e.e2.accept(this), e.e3.accept(this));
        return res;
    }
    
    @Override
    public Exp visit(Apply_direct e) {
        return e;
    }
    
    @Override
    public Exp visit(Apply_closure e) {
        return e;
    }
    
    @Override
    public Exp visit(Make_closure e) {
        return e;
    }
    
}
