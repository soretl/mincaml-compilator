package frontend;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import mincaml.type.Type;
import mincaml.tree.Add;
import mincaml.tree.App;
import mincaml.tree.Apply_closure;
import mincaml.tree.Apply_direct;
import mincaml.tree.Array;
import mincaml.tree.Bool;
import mincaml.tree.Eq;
import mincaml.tree.Exp;
import mincaml.tree.FAdd;
import mincaml.tree.FDiv;
import mincaml.tree.FMul;
import mincaml.tree.FNeg;
import mincaml.tree.FSub;
import mincaml.tree.Float;
import mincaml.tree.FunDef;
import mincaml.tree.Get;
import mincaml.tree.Id;
import mincaml.tree.If;
import mincaml.tree.Int;
import mincaml.tree.LE;
import mincaml.tree.Let;
import mincaml.tree.LetRec;
import mincaml.tree.LetTuple;
import mincaml.tree.Make_closure;
import mincaml.tree.Neg;
import mincaml.tree.Not;
import mincaml.tree.Put;
import mincaml.tree.Sub;
import mincaml.tree.Tuple;
import mincaml.tree.Unit;
import mincaml.tree.Var;
import mincaml.visitor.ObjVisitor;
import mincaml.visitor.VarVisitor;

/**
 * Classe séparant les fonctions du corps du programme, et les
 * stockant dans la structure ClosureConverted. 
 * Gère les problèmes de Closure 
 */
public class ClosureVisitor implements ObjVisitor<Exp>{
    ClosureConverted c;
    
    /**
     * Constructeur de ClosureVisitor
     * @param e l'AST d'origine
     */
    public ClosureVisitor(Exp e){
        c = new ClosureConverted(e);
        Exp res = c.main.accept(this).accept(new Let_RedVisitor());
        c.main = res;
        majFonctions();
    }
    
    /**
     * @return la structure resultat ClosureConverted
     */
    public ClosureConverted getClosureConverted(){
        return c;
    }
    
    /**
     * Algorithme de reduction des LetRecs imbriqués
     * Utilise la fonction insert
     * @param e Expression contenant possiblement des LetRecs imbriques
     * @return L'expression e traitée
     */
    public Exp reduction(Exp e){
        if(e != null){
            if(e instanceof LetRec){
                LetRec eLetR = (LetRec)e;
                FunDef fd = new FunDef(eLetR.fd.id, eLetR.fd.type, eLetR.fd.args, eLetR.fd.e);
                LetRec res = new LetRec(fd, eLetR.e);
                return insert(res.fd.e, res);
            }
        }
        return e;
    }
    
    /**
     * Part récursive de l'algorithme 
     * de reduction des LetRecs imbriqués
     * @param e Expression à traiter
     * @param original Premier LetRec de l'arborescence traitée
     * @return L'expression e traitée
     */
    private Exp insert(Exp e, LetRec original){
        if(e != null){
            if(e instanceof LetRec){
                LetRec eLetR = (LetRec)e;
                FunDef fd = new FunDef(eLetR.fd.id, eLetR.fd.type, eLetR.fd.args, eLetR.fd.e);
                FunDef fd2 = new FunDef(original.fd.id, original.fd.type, original.fd.args, eLetR.e);
                LetRec res2 = new LetRec(fd2, original.e);
                return new LetRec(fd, res2);
            }
            else if(e instanceof Exp){
                FunDef fd = new FunDef(original.fd.id, original.fd.type, original.fd.args, e);
                return new LetRec(fd, original.e);
            }
        }
        return e;
    }
    
    /**
     * Parcours toutes les fonctions avec le ClosureVisitor courant
     * une nouvelle fois, pour permettre de mettre à jour les apply_direct
     * et apply_closure maintenant que toutes les fonctions sont déclarées
     */
    private void majFonctions(){
        for(FunDef fd:c.fonctions){
            fd.e = fd.e.accept(this);
        }
        for(Closure c:c.fonctionsClosure){
            c.fd.e = c.fd.e.accept(this);
        }
    }

    @Override
    public Exp visit(Unit e) {
        return e;
    }

    @Override
    public Exp visit(Bool e) {
        return e;
    }

    @Override
    public Exp visit(Int e) {
        return e;
    }

    @Override
    public Exp visit(Float e) {
        return e;
    }

    /**
     * Applique la Closure Conversion sur l'expression Not e
     * en l'appliquant récursivement à l'expression que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(Not e) {
        Not res = new Not(e.e.accept(this));
        return res;
    }

    /**
     * Applique la Closure Conversion sur l'expression Neg e
     * en l'appliquant récursivement à l'expression que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(Neg e) {
        Neg res = new Neg(e.e.accept(this));
        return res;
    }

    /**
     * Applique la Closure Conversion sur l'expression Add e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(Add e) {
        Exp e1 = e.e1.accept(this);
        Exp e2 = e.e2.accept(this);
        Add res = new Add(e1, e2);
        return res;
    }

    /**
     * Applique la Closure Conversion sur l'expression Sub e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(Sub e) {
        Exp e1 = e.e1.accept(this);
        Exp e2 = e.e2.accept(this);
        Sub res = new Sub(e1, e2);
        return res;
    }

    /**
     * Applique la Closure Conversion sur l'expression FNeg e
     * en l'appliquant récursivement à l'expression que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(FNeg e) {
        Exp e1 = e.e.accept(this);
        FNeg res = new FNeg(e1);
        return res;
    }

    /**
     * Applique la Closure Conversion sur l'expression FAdd e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(FAdd e) {
        Exp e1 = e.e1.accept(this);
        Exp e2 = e.e2.accept(this);
        FAdd res = new FAdd(e1, e2);
        return res;
    }

    /**
     * Applique la Closure Conversion sur l'expression FSub e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(FSub e) {
        Exp e1 = e.e1.accept(this);
        Exp e2 = e.e2.accept(this);
        FSub res = new FSub(e1, e2);
        return res;
    }

    /**
     * Applique la Closure Conversion sur l'expression FMul e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(FMul e) {
        Exp e1 = e.e1.accept(this);
        Exp e2 = e.e2.accept(this);
        FMul res = new FMul(e1, e2);
        return res;
    }

    /**
     * Applique la Closure Conversion sur l'expression FDiv e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(FDiv e) {
        Exp e1 = e.e1.accept(this);
        Exp e2 = e.e2.accept(this);
        FDiv res = new FDiv(e1, e2);
        return res;
    }

    /**
     * Applique la Closure Conversion sur l'expression Eq e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(Eq e) {
        Exp e1 = e.e1.accept(this);
        Exp e2 = e.e2.accept(this);
        Eq res = new Eq(e1, e2);
        return res;
    }

    /**
     * Applique la Closure Conversion sur l'expression LE e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(LE e) {
        Exp e1 = e.e1.accept(this);
        Exp e2 = e.e2.accept(this);
        LE res = new LE(e1, e2);
        return res;
    }

    /**
     * Applique la Closure Conversion sur l'expression If e
     * en l'appliquant récursivement aux expressions que
     * e contient, et en appliquant la methode reduction
     * au resultat
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(If e) {
        If res = new If(e.e1.accept(this), e.e2.accept(this), e.e3.accept(this));
        return reduction(res);
    }

    /**
     * Applique la Closure Conversion sur l'expression Let e
     * en l'appliquant récursivement aux expressions que
     * e contient, et en appliquant la methode reduction
     * au resultat
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(Let e) {
        Let res = new Let(e.id, e.t, e.e1.accept(this), e.e2.accept(this));
        return reduction(res);
    }

    /**
     * Si la variable est un nom de closure, renvoie un make_closure
     * adapté, sinon renvoie simplement la variable
     * @param e
     * @return 
     */
    @Override
    public Exp visit(Var e) {
        //On vérifie si e est le label d'une closure
        Boolean found = false;
        Closure c_correspondante = null;
        for(Closure tclosure:c.fonctionsClosure){
            if(tclosure.fd.id.toString().equals(e.id.toString())){
                found = true;
                c_correspondante = tclosure;
            }
        }
        if(found){  //Si oui, on renvoie un Make_closure contenant cet id et la liste des variables libres de la fonction
            Make_closure res = new Make_closure(e.id, c_correspondante.fv);
            return res;
        }else{
            for(FunDef tempfd:c.fonctions){ //Sinon, c'est que cette closure était considérée à tort comme une fonction, on modifie ça
            if(tempfd.id.toString().equals(e.id.toString())){
                    transfererFonction(e.id);
                }
            }
            return e;
        }
    }

    /**
     * Applique la Closure Conversion sur l'expression LetRec e
     * en l'appliquant récursivement aux expressions que
     * e contient, et en appliquant la methode reduction
     * au resultat
     * Ajoute la fonction définie par LetRec à la liste 
     * des fonctions
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(LetRec e) {
        LetRec temp = (LetRec)reduction(e);
        List<Id> tempargs = temp.fd.args;
        
        FunDef fd = new FunDef(temp.fd.id, temp.fd.type, temp.fd.args, temp.fd.e.accept(this));
        
        //On vérifie si la fonction a des variables libres
        Iterator<String> it = fd.e.accept(new VarVisitor()).iterator(); //Liste des variables de la définition de la fonction
        
        List<Var> varLibres = new ArrayList();   //Liste des variables libres, qu'on va remplir
        while(it.hasNext()) {   //tant qu'il reste des variables
            String tempvar = it.next();
            Boolean found = false;
            
            //Si la variable est le nom de la fonction, ce n'est pas une variable libre
            if(tempvar.equals(fd.id.id)){
                found = true;
            }
            
            //Si la variable correspond à un nom de fonction, ce n'est pas une variable libre
            for(FunDef tempfd:c.fonctions){
                if(tempfd.id.id.equals(tempvar)){
                    found = true;
                }
            }
            //Si la variable correspond à un nom de fonction avec closure, ce n'est pas une variable libre
            for(Closure tempclosure:c.fonctionsClosure){
                if(tempclosure.fd.id.id.equals(tempvar)){
                    found = true;
                }
            }
            //Si la variable correspond a un argument, ce n'est pas une variable libre
            for(Id temparg:fd.args){    
                if(temparg.id.equals(tempvar)){
                    found = true;
                }
            }
            if(!found){ //Si la variable ne correspond à rien de connu, c'est une variable libre
                varLibres.add(new Var(new Id(tempvar)));
            }
        }
        if(varLibres.isEmpty()){    //Si pas de variables libres, appel direct
            c.fonctions.add(fd);
        }else{  //Si variables libres, création d'un closure
            Closure closure = new Closure(fd, varLibres);
            c.fonctionsClosure.add(closure);
        }
        return reduction(temp.e).accept(this);
    }

    /**
     * Applique la Closure Conversion sur l'expression App e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * Si l'App est l'application d'une variable, détermine
     * si la conversion se fait en apply_direct ou en apply_closure
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(App e) {
        List<Exp> listeAncienne = e.es;
        List<Exp> listeRes = new ArrayList();
        for(Exp el:listeAncienne){
            listeRes.add(el.accept(this)); 
        }
        
        if(e.e instanceof Var){ //Si on applique une variable
            Var v = (Var)e.e;
            
            //on cherche si c'est une fonction directe
            for(FunDef tempfd:c.fonctions){
                if(tempfd.id.toString().equals(v.id.toString())){
                    if(e.es.size() == tempfd.args.size()){  //Si le nombre d'arguments donnés correspond au nombre d'arguments attendus, c'est une fonction directe 
                        Apply_direct res = new Apply_direct(v.id, listeRes);
                        return res;
                    }else{  //sinon, cette fonction qui était considérée comme directe est une fonction closure, on modifie donc ça
                        transfererFonction(v.id);
                    }
                }
            }
            //Sinon, on cherche si c'est une fonction système
            if(v.id.toString().equals("print_int") || v.id.toString().equals("print")){
                Apply_direct res = new Apply_direct(v.id, listeRes);
                return res;
            }
            //Sinon, on cherche si c'est une fonction closure à laquelle on a pas encore associé ses variables libres
            for(Closure tempClosure:c.fonctionsClosure){
                if(tempClosure.fd.id.toString().equals(v.id.toString())){
                    Make_closure mc = new Make_closure(v.id, tempClosure.fv);
                    Id new_id = Id.gen();
                    App new_app = new App(new Var(new_id), listeRes);
                    Let res = new Let(new_id, Type.gen(), mc, new_app.accept(this));
                    return res;
                }
            }
            //Sinon, on affirme, peut être temporairement, que c'est une fonction closure
            //Ce sera vérifié une fois toutes les fonctions traitées, et listées
            Apply_closure res = new Apply_closure(v.id, listeRes);
            return res;
        }
        else{
            App res = new App(e.e.accept(this), listeRes);
            return res;
        }
    }
    
    /**
     * Transfère la fonction d'id id de la liste
     * des fonctions directes aux closures 
     * @param id 
     */
    public void transfererFonction(Id id){
        
        Boolean trouvee = false;
        FunDef fonction_trouvee = null;
        for(FunDef f:c.fonctions){
            if(id.toString().equals(f.id.toString())){
                trouvee = true;
                fonction_trouvee = f;
            }
        }
        if(trouvee){
            List<FunDef> listeAncienne = c.fonctions;
            List<FunDef> listeRes = new ArrayList();
            for(FunDef f:listeAncienne){
                if(!id.toString().equals(f.id.toString())){
                    listeRes.add(f);
                }
            }
            c.fonctions = listeRes;
            
            c.fonctionsClosure.add(new Closure(fonction_trouvee, new ArrayList()));
        }
    }

    /**
     * Applique la Closure Conversion sur l'expression Tuple e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(Tuple e) {
        List<Exp> listeAncienne = e.es;
        List<Exp> listeRes = new ArrayList();
        for(Exp el:listeAncienne){
            listeRes.add(el.accept(this)); 
        }
        
        Tuple res = new Tuple(listeRes);
        return res;
    }

    /**
     * Applique la Closure Conversion sur l'expression LetTuple e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(LetTuple e) {
        List<Id> listeAncienneId = e.ids;
        List<Id> listeResId = new ArrayList();
        for(Id id:listeAncienneId){
            listeResId.add(id); 
        }
        
        LetTuple res = new LetTuple(listeResId, e.ts, e.e1.accept(this), e.e2.accept(this));
        return res;
    }

    /**
     * Applique la Closure Conversion sur l'expression Array e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(Array e) {
        Array res = new Array(e.e1.accept(this), e.e2.accept(this));
        return res;
    }

    /**
     * Applique la Closure Conversion sur l'expression Get e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(Get e) {
        Get res = new Get(e.e1.accept(this), e.e2.accept(this));
        return res;
    }

    /**
     * Applique la Closure Conversion sur l'expression Put e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à réduire
     * @return e réduite
     */
    @Override
    public Exp visit(Put e) {
        Put res = new Put(e.e1.accept(this), e.e2.accept(this), e.e3.accept(this));
        return res;
    }
    
    /**
     * Applique la Closure Conversion sur l'expression Apply_direct e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * Transforme e en Apply_closure si on s'était trompés quand il
     * nous manquait des informations
     * @param e
     * @return 
     */
    @Override
    public Exp visit(Apply_direct e) {
        Id id = e.id;
        //on cherche si c'est une fonction directe
        for(FunDef tempfd:c.fonctions){
            if(tempfd.id.id.equals(id.toString())){
                if(e.es.size() == tempfd.args.size()){ //Si le nombre d'arguments donnés correspond au nombre d'arguments attendus, c'est une fonction directe 
                    Apply_direct res = new Apply_direct(id, e.es);
                    return res;
                }else{ //sinon, cette fonction qui était considérée comme directe est une fonction closure, on modifie donc ça
                    transfererFonction(id);
                }
            }
        }
        //Sinon, on cherche si c'est une fonction système
        if(id.toString().equals("print_int")){
            Apply_direct res = new Apply_direct(id, e.es);
            return res;
        }
       //Sinon, on cherche si c'est une fonction closure à laquelle on a pas encore associé ses variables libres
        for(Closure tempClosure:c.fonctionsClosure){
            if(tempClosure.fd.id.toString().equals(id.toString())){
                Make_closure mc = new Make_closure(id, tempClosure.fv);
                Id new_id = Id.gen();
                App new_app = new App(new Var(new_id), e.es);
                Let res = new Let(new_id, Type.gen(), mc, new_app.accept(this));
                return res;
            }
        }
        return e;
    }
    
    /**
     * Applique la Closure Conversion sur l'expression Apply_closure e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * Transforme e en Apply_direct si on s'était trompés quand il
     * nous manquait des informations
     * @param e
     * @return 
     */
    @Override
    public Exp visit(Apply_closure e) {
        Id id = e.id;
        //on cherche si c'est une fonction directe
        for(FunDef tempfd:c.fonctions){
            if(tempfd.id.id.equals(id.toString())){
                if(e.es.size() == tempfd.args.size()){  //Si le nombre d'arguments donnés correspond au nombre d'arguments attendus, c'est une fonction directe 
                    Apply_direct res = new Apply_direct(id, e.es);
                    return res;
                }else{  //sinon, cette fonction qui était considérée comme directe est une fonction closure, on modifie donc ça
                    transfererFonction(id);
                }
            }
        }
        //Sinon, on cherche si c'est une fonction système
        if(id.toString().equals("print_int")){
            Apply_direct res = new Apply_direct(id, e.es);
            return res;
        }
       //Sinon, on cherche si c'est une fonction closure à laquelle on a pas encore associé ses variables libres
        for(Closure tempClosure:c.fonctionsClosure){
            if(tempClosure.fd.id.toString().equals(id.toString())){
                Make_closure mc = new Make_closure(id, tempClosure.fv);
                Id new_id = Id.gen();
                App new_app = new App(new Var(new_id), e.es);
                Let res = new Let(new_id, Type.gen(), mc, new_app.accept(this));
                return res;
            }
        }
        return e;
    }
    
    /**
     * @param e
     * @return le Make_closure e
     */
    @Override
    public Exp visit(Make_closure e) {
        return e;
    }
}
