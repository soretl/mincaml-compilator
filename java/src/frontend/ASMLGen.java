package frontend;

import java.util.*;
import asml.tree.*;
import asml.tree.exp.*;
import asml.tree.asmt.*;
import asml.tree.fundef.Fun;
import asml.tree.fundef.Fundef;
import asml.tree.ident_or_imm.*;
import asml.visitor.AsmlGenVisitor;
import asml.visitor.AsmlPrintVisitor;
import mincaml.tree.App;
import mincaml.tree.Apply_closure;
import mincaml.tree.Apply_direct;
import mincaml.tree.Array;
import mincaml.tree.Bool;
import mincaml.tree.Eq;
import mincaml.tree.FAdd;
import mincaml.tree.FDiv;
import mincaml.tree.FMul;
import mincaml.tree.FNeg;
import mincaml.tree.FSub;
import mincaml.tree.Float;
import mincaml.tree.FunDef;
import mincaml.tree.Get;
import mincaml.tree.LE;
import mincaml.tree.Let;
import mincaml.tree.LetRec;
import mincaml.tree.LetTuple;
import mincaml.tree.Make_closure;
import mincaml.tree.Not;
import mincaml.tree.Put;
import mincaml.tree.Tuple;
import mincaml.tree.Unit;
import mincaml.tree.Var;
import mincaml.visitor.ObjVisitor;
import mincaml.visitor.PrintVisitor;

/**
 * Classe convertissant le code mincaml optimisé en code ASML
 */
public class ASMLGen implements ObjVisitor<Noeud> {
    
    asml.tree.fundef.Main main; //Début du corps du programme ASML
    asml.tree.fundef.Fundef ftop;   //Début de la définition des fonctions ASML
    asml.tree.Toplevel top; //Début de l'arbre ASML
    ClosureConverted c; //Structure contenant le code Mincaml optimisé
    
    /**
     * Constructeur de la classe 
     * @param closureConverted Le code à optimiser
     */
    public ASMLGen(ClosureConverted closureConverted){
        c = closureConverted;
        main = new asml.tree.fundef.Main((Asmt)c.getMain().accept(this));   //Conversion du corps du programme
        ftop = main;
        if(c.fonctions != null || c.fonctionsClosure != null){  //Si il y a des fonctions, on les traite en amont, pour mettre le corps du programme à leur suite
            List totalFonctions = new ArrayList();
            totalFonctions.addAll(c.fonctions);
            for(Closure tempClosure:c.fonctionsClosure){
                totalFonctions.add(tempClosure.fd);
            }
            ftop = (Fundef)appRec(totalFonctions, main);
        }
        top = new Toplevel(ftop);
    }
    
    /**
     * Convertit en ASML la liste de fonctions qui lui est passée en paramètre
     * @param fds
     * @param main
     * @return 
     */
    public Noeud appRec(List<FunDef> fds, asml.tree.fundef.Main main){
        if(fds.size()>=1){  //tant qu'il reste des fonctions à traiter, on les traite
            int i=0;
            FunDef fd = null;
            List<FunDef> listeAncienne = fds;
            List<FunDef> listeRes = new ArrayList();
            for(FunDef fdt:listeAncienne){  //on stocke la fundef à traiter, on recopie les autres
                if(i==0){
                    fd = fdt;
                    i++;
                }
                else{
                    listeRes.add(fdt); 
                }
            }
            Id id = new Id(fd.id.toString());
            Label l = new Label(id);    //Conversion en abel du nom de la fonction
            List<mincaml.tree.Id> params = fd.args;
            Formal_arg fa = new Formal_arg();
            for(mincaml.tree.Id idt:params){    //Conversion des arguments de la fonction en formal_args
                Id new_id = new Id(idt.toString());
                Ident new_ident = new Ident(new_id);
                fa.add_end(new_ident);
            }
            Noeud temp = fd.e.accept(this);
            if(!c.trouverVariablesLibres(fd.id).isEmpty()){
                temp = temp.accept(new VarLibresVisitor( c.trouverVariablesLibres(fd.id) ));
            }
            Asmt res;
            if(temp instanceof Asmt){
                res = (asml.tree.asmt.Asmt)temp;
            }else{
                res = new FinAsmt((Exp)temp);
            }
            return new Fun(l, fa, res, (asml.tree.fundef.Fundef)appRec(listeRes, main));
        }
        else{
            return main;
        }
    }
    
    /**
     * Renvoie pour un string s donné le nom de la 
     * fonction mincaml correspondante si elle existe
     * @param s
     * @return 
     */
    public String correspondance_fonctions(String s){
        switch(s){
            case "print_int":
                return "_min_caml_print_int";
            case "print":
                return "_min_caml_print_int";
        }
        return s;
    }
    
    public asml.tree.fundef.Main getMain(){
        return main;
    }
    
    public Toplevel getTop(){
        return top;
    }

    @Override
    public Noeud visit(Unit e) {
        return new Nop();
    }

    //CONVENTION : True = 1, False = 0
    @Override
    public Noeud visit(Bool e) {
        if(e.b == true){
            return new Int(1);
        }else{
            return new Int(0);
        }
    }

    @Override
    public Noeud visit(mincaml.tree.Int e) {
        return new asml.tree.exp.Int(e.i);
    }

    @Override
    public Noeud visit(Float e) {
        return null;
    }

    @Override
    public Noeud visit(Not e) {
        return new If(Condition.EQ, (Ident)e.e.accept(this), new Int(1), new FinAsmt(new Int(0)), new FinAsmt(new Int(1)));
    }

    @Override
    public Noeud visit(mincaml.tree.Neg e) {
        Var v = (Var)e.e;
        Id id = new Id(correspondance_fonctions(v.id.toString()));
        Ident ident = new Ident(id);
        
        Neg res = new Neg(ident);
        return res;
    }

    @Override
    public Noeud visit(mincaml.tree.Add e) {
        Var v1 = (Var)e.e1;
        Id id1 = new Id(correspondance_fonctions(v1.id.toString()));
        Ident ident1 = new Ident(id1);
        
        Var v2 = (Var)e.e2;
        Id id2 = new Id(correspondance_fonctions(v2.id.toString()));
        Ident ident2 = new Ident(id2);
        
        Add res = new Add(ident1, ident2);
        return res;
    }

    @Override
    public Noeud visit(mincaml.tree.Sub e) {
        Var v1 = (Var)e.e1;
        Id id1 = new Id(correspondance_fonctions(v1.id.toString()));
        Ident ident1 = new Ident(id1);
        
        Var v2 = (Var)e.e2;
        Id id2 = new Id(correspondance_fonctions(v2.id.toString()));
        Ident ident2 = new Ident(id2);
        
        Sub res = new Sub(ident1, ident2);
        return res;
    }

    @Override
    public Noeud visit(FNeg e) {
        return null;
    }

    @Override
    public Noeud visit(FAdd e) {
        return null;
    }

    @Override
    public Noeud visit(FSub e) {
        return null;
    }

    @Override
    public Noeud visit(FMul e) {
        return null;
    }

    @Override
    public Noeud visit(FDiv e) {
        return null;
    }

    @Override
    public If visit(Eq e) {
        return new If(Condition.EQ, (Ident)e.e1.accept(this), (Ident)e.e2.accept(this), new FinAsmt(new Int(1)), new FinAsmt(new Int(0)));
    }

    @Override
    public If visit(LE e) {
        return new If(Condition.LE, (Ident)e.e1.accept(this), (Ident)e.e2.accept(this), new FinAsmt(new Int(1)), new FinAsmt(new Int(0)));
    }

    /**
     * Convertit le If e Mincaml en ASML
     * @param e
     * @return 
     */
    @Override
    public Noeud visit(mincaml.tree.If e) {
        Noeud condition = e.e1.accept(this);
        Asmt nextThen;
        Asmt nextElse;
        
        Noeud e2 = e.e2.accept(this);
        if(e2 instanceof LetId){    
            nextThen = (LetId)e2;
        }else{
            nextThen = new FinAsmt((Exp)e2);
        }
        Noeud e3 = e.e3.accept(this);
        if(e3 instanceof LetId){
            nextElse = (LetId)e3;
        }else{
            nextElse = new FinAsmt((Exp)e3);
        }
        
        if(condition instanceof Ident){ //Si la condition est un ident, alors elle équivaut à un booléen. Par convention True = 1 et False = 0. La condition deviens donc ident == 1
            If res = new If(Condition.EQ, (Ident)condition, new Int(1), nextThen, nextElse);
            return res;
        }else{  //Si la condition n'est pas un ident, alors c'est un if
            If res = (If)condition;
            res.nextThen = nextThen;
            res.nextElse = nextElse;
            return res;
       }
    }

    /**
     * Convertit le Let e Mincaml en ASML
     * @param e
     * @return 
     */
    @Override
    public Noeud visit(Let e) {
        Ident idSuite = new Ident(new asml.tree.Id(e.id.getId()));
        Asmt fin = null;
        LetId res = null;
        if(!(e.e2 instanceof Let) && !(e.e2 instanceof LetTuple)){ //Dans le cas ou la deuxieme partie du let ne soit pas un let, et donc pas une asmt
            fin = new FinAsmt((asml.tree.exp.Exp)e.e2.accept(this));
        }else{
            fin = (asml.tree.asmt.Asmt)e.e2.accept(this);
        }
        if(e.e1 instanceof Tuple){  //Si e1 est un Tuple il est traité dans la fonction traiterTuple qui renvoie un LetId
            res = traiterTuple( (Tuple)e.e1, idSuite, fin);
        }else if(e.e1 instanceof Make_closure){ //Si e1 est un Make_closure il est traité dans la fonction traiter_make_closure qui renvoie un LetId
            res = (LetId)traiter_make_closure( (Make_closure)e.e1, idSuite, fin);
        }else{  //Sinon, e1 est traité récursivement
            res = new LetId(idSuite, (asml.tree.exp.Exp)e.e1.accept(this), fin);
        }
        return res;
    }

    @Override
    public Noeud visit(Var e) {
        Id i = new Id(correspondance_fonctions(e.id.toString())); 
        Ident res = new Ident(i);
        return res;
    }

    @Override
    public Noeud visit(LetRec e) {
        return null;
    }

    //N'est pas censé exister dans l'arbre de closure
    @Override
    public Noeud visit(App e) {
        return null;
    }

    //Jamais rencontré par choix, voir traiterTuple
    @Override
    public Noeud visit(Tuple e) {
        return null;
    }
    
    /**
     * Convertir le tuple en Array, utilise créer tupleRec
     * pour remplir le tableau
     * @param e
     * @param ident
     * @param suite
     * @return 
     */
    public LetId traiterTuple(Tuple e, Ident ident, Asmt suite){
        Id array = new Id("_min_caml_create_array");
        Label label_array = new Label(array);
        
        Int defaut = new Int(0);        
        Ident ident_defaut = new Ident(Id.genStandard());
        
        Int taille = new Int(e.es.size());        
        Ident ident_taille = new Ident(Id.genStandard());
        
        Formal_arg args = new Formal_arg();
        args.add_end(ident_taille);
        args.add_end(ident_defaut);
        
        Call creation_tuple = new Call(label_array, args);
        
        LetId let_tableau = new LetId(ident, creation_tuple,  creerTupleRec(e.es, ident, new Int(0), suite));
        LetId let_defaut = new LetId(ident_defaut, defaut, let_tableau);
        LetId let_taille = new LetId(ident_taille, taille, let_defaut);
        return let_taille;
    }
    
    /**
     * Remplit le tableau tableau avec les expressions es récursivement
     * @param es
     * @param tableau
     * @param emplacement
     * @param suite
     * @return 
     */
    public Asmt creerTupleRec(List<mincaml.tree.Exp> es, Ident tableau, Int emplacement, Asmt suite){
        if(es.size()>=1){
            int i=0;
            mincaml.tree.Exp exp_courante = null;
            List<mincaml.tree.Exp> listeAncienne = es;
            List<mincaml.tree.Exp> listeRes = new ArrayList();
            for(mincaml.tree.Exp temp_exp:listeAncienne){
                if(i==0){
                    exp_courante = temp_exp;
                    i++;
                }
                else{
                    listeRes.add(temp_exp); 
                }
            }
            Ident unused_ident = new Ident(Id.genUnused());
            MemStore stockeValeur = new MemStore(tableau, emplacement, (Ident_or_Arm)exp_courante.accept(this));
            if(es.size()>1){
                return new LetId(unused_ident, stockeValeur, creerTupleRec(listeRes, tableau, new Int(emplacement.i+1), suite));
            }else{
                return new LetId(unused_ident, stockeValeur, suite);
            }
        }
        else{
            return null;
        }
    }

    /**
     * Utilise tupleRec pour convertir
     * le LetTuple mincaml en une 
     * succession de lectures dans un 
     * tableau en ASML
     * @param e
     * @return 
     */
    @Override
    public Noeud visit(LetTuple e) {
        return tupleRec(e.ids, (Ident)e.e1.accept(this),new Int(0), (Asmt)e.e2.accept(this));
    }
    
    /**
     * Convertir récursivement les données
     * d'un LetTuple en une succession de 
     * lectures dans un tableau ASML
     * @param ids
     * @param tableauSuite
     * @param emplacement
     * @param suite
     * @return 
     */
    public Asmt tupleRec(List<mincaml.tree.Id> ids, Ident tableauSuite, Int emplacement, Asmt suite){
        if(ids.size()>=1){
            int i=0;
            mincaml.tree.Id id_courant = null;
            List<mincaml.tree.Id> listeAncienne = ids;
            List<mincaml.tree.Id> listeRes = new ArrayList();
            for(mincaml.tree.Id temp_id:listeAncienne){
                if(i==0){
                    id_courant = temp_id;
                    i++;
                }
                else{
                    listeRes.add(temp_id); 
                }
            }
            Id conv_id = new Id(id_courant.toString());
            Ident conv_ident = new Ident(conv_id);
            MemLoad recupValeur = new MemLoad(tableauSuite, emplacement);
            if(ids.size()>1){
                return new LetId(conv_ident, recupValeur, tupleRec(listeRes, tableauSuite, new Int(emplacement.i+1), suite));
            }else{
                return new LetId(conv_ident, recupValeur, suite);
            }
        }
        else{
            return null;
        }
    }

    @Override
    public Noeud visit(Array e) {
        Id array = new Id("_min_caml_create_array");
        Label label_array = new Label(array);
        
        Formal_arg args = new Formal_arg();
        args.add_end((Ident)e.e1.accept(this));
        args.add_end((Ident)e.e2.accept(this));
        return new Call(label_array, args);
    }

    @Override
    public Noeud visit(Get e) {
        return new MemLoad((Ident)e.e1.accept(this), (Ident)e.e2.accept(this));
    }

    @Override
    public Noeud visit(Put e) {
        return new MemStore((Ident)e.e1.accept(this), (Ident)e.e2.accept(this), (Ident)e.e3.accept(this));
    }

    /**
     * Convertit l'apply_direct mincaml en call ASML
     * en convertissant individuellement chaque attribut 
     * de e
     * @param e
     * @return 
     */
    @Override
    public Noeud visit(Apply_direct e) {
        Id id = new Id(correspondance_fonctions(e.id.toString()));
        Label l = new Label(id);
        
        List<mincaml.tree.Exp> listeAncienne = e.es;
        Formal_arg listeRes = new Formal_arg();
        if(!(e.es.get(0) instanceof Unit)){ //Si ce n'est pas une fonction sans arguments, on la traite normallement
            for(mincaml.tree.Exp el:listeAncienne){
                Var v1 = (Var)el;
                Id id1 = new Id(v1.id.getId());
                Ident ident1 = new Ident(id1);
                listeRes.add_end(ident1);
            }
        }else{  //Parcours des fonctions pour enlever l'argument connu de celle ci, car c'est une fonction sans arguments
            for(FunDef fd:c.fonctions){ 
                if(fd.id.toString().equals(e.id.toString())){
                    fd.args.clear();
                }
            }
        }
        
        Call res = new Call(l, listeRes);
        return res;
    }
    
    /**
     * Convertit l'apply_closure mincaml en callClosure ASML
     * en convertissant individuellement chaque attribut 
     * de e
     * @param e
     * @return 
     */
    @Override
    public Noeud visit(Apply_closure e) {
        Id id = new Id(correspondance_fonctions(e.id.toString()));
        Ident ident = new Ident(id);
        
        List<mincaml.tree.Exp> listeAncienne = e.es;
        Formal_arg listeRes = new Formal_arg();
        for(mincaml.tree.Exp el:listeAncienne){
            Var v1 = (Var)el;
            Id id1 = new Id(v1.id.getId());
            Ident ident1 = new Ident(id1);
            listeRes.add_end(ident1);
        }
        
        CallClo res = new CallClo(ident, listeRes);
        return res;
    }
    
    /**
     * On n'utilise ce visiteur que dans 
     * le cas ou le make_closure était 
     * contenu dans une FinAsmt, 
     * on crée alors un Let avec
     * une nouvelle variable
     * @param e
     * @return 
     */
    @Override
    public Noeud visit(Make_closure e) {
        Ident new_ident = new Ident(Id.genStandard());
        return traiter_make_closure(e, new_ident, new FinAsmt(new_ident));
    }
    
    /**
     * Convertit le make_closure en une succession
     * de stockages en mémoire des différentes
     * variables libresen utilisant deploiement_make
     * @param e
     * @param ident
     * @param suite
     * @return 
     */
    public Asmt traiter_make_closure(Make_closure e, Ident ident, Asmt suite){
        //On crée un ident pour l'adresse de la fonction traitée par le MakeClosure
        Ident address = new Ident(new Id("addr"+e.id));
        MemStore stockage_adresse = new MemStore(ident, new Int(0), address);
        LetId let_stockage_adresse = new LetId(new Ident(Id.genUnused()), stockage_adresse, deploiement_make(c.trouverVariablesLibres(e.id), ident, new Int(0), suite));
        LetId definition_adresse = new LetId(address, new Ident(new Id(e.id.toString())), let_stockage_adresse);
        
        //On crée un let pour l'allocation en mémoire pour la fonction
        New alloc = new New( new Int(c.trouverVariablesLibres(e.id).size() + 1) );
        LetId let_alloc = new LetId(ident, alloc, definition_adresse);
        
        return let_alloc;
    }
    
    /**
     * Fonction qui stocke récursivement toutes les variables libres en mémoire dans la fonction
     */
    public Asmt deploiement_make(List<Var> fv, Ident address, Int offset, Asmt suite){
        //Si plus de variables libres, fin de la récursivité
        if (fv.isEmpty()){
            return (Asmt)suite;
        }
        //Sinon, on stocke la variable
        else{
            Var var_courante = null;
            List listeResultat = new ArrayList();
            Boolean stop = false;
            for(Var tempfv:fv){
                if(!stop){
                    var_courante = tempfv;
                    stop = true;
                }
                else{
                    listeResultat.add(tempfv);
                }
            }
            Int new_offset = new Int(offset.i+1);
            Id new_id = new Id(var_courante.id.toString());
            Ident source = new Ident(new_id);
            MemStore stockage = new MemStore(address, new_offset, source);
            LetId res = new LetId(new Ident(Id.genUnused()), stockage, deploiement_make(listeResultat, address, new_offset, suite));
            return res;
        }
    }
    
    
}

