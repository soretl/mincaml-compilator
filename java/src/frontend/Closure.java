package frontend;

import java.util.*;
import mincaml.tree.FunDef;
import mincaml.tree.Id;
import mincaml.tree.Var;

class Closure {
    FunDef fd;  //Définition de la fonction
    List<Var> fv;    //Liste des variables libres de la fonction
    
    public Closure(FunDef fd, List<Var> fv){
        this.fd = fd;
        this.fv = fv;
    }
}
