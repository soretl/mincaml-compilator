package frontend;

import asml.parser.AsmlReader;
import asml.tree.fundef.Fundef;
import asml.visitor.AsmlGenVisitor;
import asml.visitor.AsmlPrintVisitor;
import asml.visitor.AsmlWriteVisitor;
import backend.AffichageAsmlTree;
import backend.BasicRegisterAllocation;
import backend.SimpleGenerator;
import mincaml.parser.MincamlReader;
import mincaml.parser.Parser;
import mincaml.tree.Exp;
import mincaml.visitor.HeightVisitor;
import mincaml.visitor.ObjVisitor;
import mincaml.visitor.PrintVisitor;
import mincaml.visitor.VarVisitor;

import java.io.*;
import java.util.*;

public class Main {
    static public void main(String argv[]) {    
        try {
            Exp expression = MincamlReader.read(argv[0]);      
            assert (expression != null);

            System.out.println("------ AST ------");
            expression.accept(new PrintVisitor());
            System.out.println();
            System.out.println();
            System.out.println();
            
            System.out.println("K-Normalization");
            Exp knorm = expression.accept(new KNormVisitor());
            knorm.accept(new PrintVisitor());
            
            System.out.println();
            System.out.println();
            System.out.println();
            
            System.out.println("Alpha_Reduction");
            Exp alpha_red = knorm.accept(new Alpha_RedVisitor());
            alpha_red.accept(new PrintVisitor());
            
            System.out.println();
            System.out.println();
            System.out.println();
            
            System.out.println("Nested Let_Reduction");
            Exp let_red = alpha_red.accept(new Let_RedVisitor());
            let_red.accept(new PrintVisitor());

            System.out.println();
            System.out.println();
            System.out.println();
            
            System.out.println("Closure Conversion");
            ClosureVisitor cv = new ClosureVisitor(let_red);
            ClosureConverted closureConv = cv.getClosureConverted();
            closureConv.afficherFonctions();
            closureConv.afficherClosures();
            closureConv.getMain().accept(new PrintVisitor());

            System.out.println();
            System.out.println();
            System.out.println();
            
            System.out.println("ASML_Gen");
            ASMLGen aGen = new ASMLGen(closureConv);
            aGen.getTop().accept(new AsmlPrintVisitor());

            System.out.println();
            System.out.println();
            System.out.println();
            
            System.out.println("Code ASML");
            aGen.getTop().accept(new AsmlGenVisitor());

            System.out.println();
            System.out.println();
            System.out.println();
            
            new AsmlWriteVisitor(aGen.getTop(), "ababa.txt");

            /*System.out.println("------ Height of the AST ----");
            ObjVisitor<Integer> v = new HeightVisitor();
            int height = expression.accept(v);
            System.out.println("using HeightVisitor: " + height);*/

            System.out.println(expression.accept(new VarVisitor()));


    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}

