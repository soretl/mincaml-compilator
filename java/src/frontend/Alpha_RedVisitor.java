package frontend;

import java.util.*;

import mincaml.tree.Add;
import mincaml.tree.App;
import mincaml.tree.Apply_closure;
import mincaml.tree.Apply_direct;
import mincaml.tree.Array;
import mincaml.tree.Bool;
import mincaml.tree.Eq;
import mincaml.tree.Exp;
import mincaml.tree.FAdd;
import mincaml.tree.FDiv;
import mincaml.tree.FMul;
import mincaml.tree.FNeg;
import mincaml.tree.FSub;
import mincaml.tree.Float;
import mincaml.tree.FunDef;
import mincaml.tree.Get;
import mincaml.tree.Id;
import mincaml.tree.If;
import mincaml.tree.Int;
import mincaml.tree.LE;
import mincaml.tree.Let;
import mincaml.tree.LetRec;
import mincaml.tree.LetTuple;
import mincaml.tree.Make_closure;
import mincaml.tree.Neg;
import mincaml.tree.Not;
import mincaml.tree.Put;
import mincaml.tree.Sub;
import mincaml.tree.Tuple;
import mincaml.tree.Unit;
import mincaml.tree.Var;
import mincaml.visitor.ObjVisitor;

/**
 * Cette classe s'assure que toutes les variables différentes ont
 * des noms différents, elle va donc associer dans une Map les anciens
 * Ids aux nouveaux Ids 
 * Assure aussi la redéfinition des noms de fonctions
 */
public class Alpha_RedVisitor implements ObjVisitor<Exp>{
    Map<String,Id> conv;    //mapping des anciens Ids vers les nouveaux Ids
    
    /**
     * Constructeur Alpha_RedVisitor, permet d'instancier la map des Ids
     */
    public Alpha_RedVisitor(){
        conv = new HashMap<>();
    }
    
    /**
     * crée un id correspondant à celui donné et le renvoie
     * @param id l'ancien Id
     * @return le nouvel Id
     */
    public Id conversion(Id id){
        Id new_id = null;
        if(id.toString().substring(0,1).equals("u")){   //Si la variable est unused, on génère une variable unused
            new_id = Id.genUnused();
        }else{
            new_id = Id.gen();
        }
        conv.put(id.getId(), new_id);
        return new_id;
    }
    
    /**
     * crée un id correspondant à celui donné en rajoutant un underscore et le renvoie
     * Si cet id existe déjà, on crée un nouvel id pour la redéfinition, et on remplace l'ancien id
     * @param id l'ancien Id
     * @return le nouvel Id
     */
    public Id conversionFonction(Id id){
        Id new_id = new Id("_"+id.toString());
        while(conv.containsKey(id.getId())){
            conv.remove(id.getId());
            new_id.id = new_id.id + "_" + (int)(Math.random() * 10);
        }
        conv.put(id.getId(), new_id);
        return new_id;
    }
    
    /**
     * Renvoie le nouvel Id correspondant à un ancien, et null si aucun ne correspond
     * @param id ancien id
     * @return id correspondant si il existe, null sinon
     */
    public Id getId(Id id){
        if(conv.containsKey(id.getId())){
            return conv.get(id.getId());
        }else{
            return id;
        }
    }
    
    /**
     * Affiche le contenu de la Map des anciens
     * ids aux nouveaux ids
     */
    public void afficherMap(){
        for (Map.Entry mapentry : conv.entrySet()) {
           System.out.println("clé: "+mapentry.getKey() 
                              + " | valeur: " + mapentry.getValue());
        }
    }
    
    @Override
    public Unit visit(Unit e) {
        return e;
    }

    @Override
    public Bool visit(Bool e) {
        return e;
    }

    @Override
    public Int visit(Int e) {
        return e;
    }

    @Override
    public Float visit(Float e) { 
        return e;
    }

    /**
     * Applique l'alpha reduction sur l'expression Not e
     * en l'appliquant récursivement à l'expression que
     * e contient
     * @param e Expression à alpha-réduire
     * @return e alpha-réduite
     */
    @Override
    public Exp visit(Not e) {
        Exp e1 = e.e.accept(this);
        return new Not(e1);
    }

    /**
     * Applique l'alpha reduction sur l'expression Neg e
     * en l'appliquant récursivement à l'expression que
     * e contient
     * @param e Expression à alpha-réduire
     * @return e alpha-réduite
     */
    @Override
    public Neg visit(Neg e) {
        Exp e1 = e.e.accept(this);
        return new Neg(e1);
    }

    /**
     * Applique l'alpha reduction sur l'expression Add e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à alpha-réduire
     * @return e alpha-réduite
     */
    @Override
    public Add visit(Add e) {
        Exp e1 = e.e1.accept(this);
        Exp e2 = e.e2.accept(this);
        return new Add(e1,e2);
    }

    /**
     * Applique l'alpha reduction sur l'expression Sub e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à alpha-réduire
     * @return e alpha-réduite
     */
    @Override
    public Sub visit(Sub e) {
        Exp e1 = e.e1.accept(this);
        Exp e2 = e.e2.accept(this);
        return new Sub(e1,e2);
    }

    @Override
    public Exp visit(FNeg e) {
        return null;
    }

    @Override
    public Exp visit(FAdd e) {
        return null;
    }

    @Override
    public Exp visit(FSub e) {
        return null;
    }

    @Override
    public Exp visit(FMul e) {
        return null;
    }

    @Override
    public Exp visit(FDiv e) {
        return null;
    }

    /**
     * Applique l'alpha reduction sur l'expression Eq e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à alpha-réduire
     * @return e alpha-réduite
     */
    @Override
    public Eq visit(Eq e) {
        Exp e1 = e.e1.accept(this);
        Exp e2 = e.e2.accept(this);
        return new Eq(e1,e2);
    }

    /**
     * Applique l'alpha reduction sur l'expression LE e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à alpha-réduire
     * @return e alpha-réduite
     */
    @Override
    public LE visit(LE e) {
        Exp e1 = e.e1.accept(this);
        Exp e2 = e.e2.accept(this);
        return new LE(e1,e2);
    }

    /**
     * Applique l'alpha reduction sur l'expression If e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à alpha-réduire
     * @return e alpha-réduite
     */
    @Override
    public If visit(If e) {
        Exp e1 = e.e1.accept(this);
        Exp e2 = e.e2.accept(this);
        Exp e3 = e.e3.accept(this);
        return new If(e1,e2,e3);
    }

    /**
     * Applique l'alpha reduction sur l'expression Let e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e Expression à alpha-réduire
     * @return e alpha-réduite
     */
    @Override
    public Let visit(Let e) {
        Exp e1 = e.e1.accept(this); //On traite la 1ere expression récursivement
        /*On associe l'id du Let avec un nouvel Id, 
        ainsi la modif n'a pas été prise en compte 
        dans le traitement de la 1ere expression*/
        Id id = conversion(e.id);   
        Exp e2 = e.e2.accept(this); //On traite la 2de expression récursivement
        Let res = new Let(id, e.t, e1, e2);
        return res;
    }

    /**
     * Retourne e avec son nouvel Id, associé
     * à l'ancien
     * @param e La variable à modifier Var e
     * @return la variable e avec son nouvel id
     */
    @Override
    public Var visit(Var e) {
        return new Var(getId(e.id));
    }

    /**
     * Applique l'alpha reduction sur l'expression LetRec e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * On crée un nouvel Alpha_RedVisitor pour explorer les 
     * sous-branches du LetRec, 
     * @param e
     * @return 
     */
    @Override
    public LetRec visit(LetRec e) {
        Alpha_RedVisitor redefinition = new Alpha_RedVisitor();
        
        for (Map.Entry mapentry : conv.entrySet()) {
           redefinition.conv.put((String)mapentry.getKey(), (Id)mapentry.getValue());
        }
        
        Id id = redefinition.conversionFonction(e.fd.id);
        //Id id = conversionFonction(e.fd.id);    //association d'un nouveau nom de fonction à l'ancien
        Exp e2 = e.e.accept(redefinition);  //traitement récursif de la 2de expression du LetRec
        
        //Conversion de chaque argument de la fonction, on leur crée un nouvel Id associé
        List<Id> listeAncienne = e.fd.args;
        List<Id> listeResultat = new ArrayList();
        for(Id idt:listeAncienne){ 
            Id temp = conversion(idt);
            listeResultat.add(temp);
            redefinition.conv.put(idt.getId(), temp);
        }
        
        FunDef fd = new FunDef(id, e.fd.type ,listeResultat, e.fd.e.accept(redefinition));;
        LetRec res = new LetRec(fd, e2);
        return res;
    }

    /**
     * Applique l'alpha reduction sur l'expression App e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e
     * @return 
     */
    @Override
    public App visit(App e) {
        List<Exp> listeAncienne = e.es;
        List<Exp> listeResultat = new ArrayList();
        for(Exp es:listeAncienne){
            listeResultat.add(es.accept(this));
        }
        App res = new App(e.e.accept(this), listeResultat);
        return res;
    }

    /**
     * Applique l'alpha reduction sur l'expression Tuple e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e
     * @return 
     */
    @Override
    public Tuple visit(Tuple e){
        List<Exp> listeAncienne = e.es;
        List<Exp> listeRes = new ArrayList();
        for(Exp el:listeAncienne){
            listeRes.add(el.accept(this));
        }
        return new Tuple(listeRes);
    }

    /**
     * Applique l'alpha reduction sur l'expression LetTuple e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e
     * @return 
     */
    @Override
    public LetTuple visit(LetTuple e){
        List<Id> listeAncienne = e.ids;
        List<Id> listeRes = new ArrayList();
        for(Id temp_id:listeAncienne){
            listeRes.add(conversion(temp_id));
        }
        
        LetTuple res = new LetTuple(listeRes, e.ts, e.e1.accept(this), e.e2.accept(this));
        return res;
    }

    /**
     * Applique l'alpha reduction sur l'expression Array e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e
     * @return 
     */
    @Override
    public Array visit(Array e){
        Array res = new Array(e.e1.accept(this), e.e2.accept(this));
        return res;
   }

    /**
     * Applique l'alpha reduction sur l'expression Get e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e
     * @return 
     */
    @Override
    public Get visit(Get e){
        Get res = new Get(e.e1.accept(this), e.e2.accept(this));
        return res;
    }

    /**
     * Applique l'alpha reduction sur l'expression Put e
     * en l'appliquant récursivement aux expressions que
     * e contient
     * @param e
     * @return 
     */
    @Override
    public Exp visit(Put e) {
        Put res = new Put(e.e1.accept(this), e.e2.accept(this), e.e3.accept(this));
        return res;
    }

    @Override
    public Exp visit(Apply_direct e) {
        return e;
    }

    @Override
    public Exp visit(Apply_closure e) {
        return e;
    }
    
    @Override
    public Exp visit(Make_closure e) {
        return e;
    }
}
