package frontend;

import java.util.*;

import mincaml.type.Type;
import mincaml.tree.Add;
import mincaml.tree.App;
import mincaml.tree.Apply_closure;
import mincaml.tree.Apply_direct;
import mincaml.tree.Array;
import mincaml.tree.Bool;
import mincaml.tree.Eq;
import mincaml.tree.Exp;
import mincaml.tree.FAdd;
import mincaml.tree.FDiv;
import mincaml.tree.FMul;
import mincaml.tree.FNeg;
import mincaml.tree.FSub;
import mincaml.tree.Float;
import mincaml.tree.FunDef;
import mincaml.tree.Get;
import mincaml.tree.Id;
import mincaml.tree.If;
import mincaml.tree.Int;
import mincaml.tree.LE;
import mincaml.tree.Let;
import mincaml.tree.LetRec;
import mincaml.tree.LetTuple;
import mincaml.tree.Make_closure;
import mincaml.tree.Neg;
import mincaml.tree.Not;
import mincaml.tree.Put;
import mincaml.tree.Sub;
import mincaml.tree.Tuple;
import mincaml.tree.Unit;
import mincaml.tree.Var;
import mincaml.visitor.ObjVisitor;

public class DuplVisitor implements ObjVisitor<Exp> {

    public Unit visit(Unit e) {
        Unit res = new Unit();
        return res;
    }

    public Bool visit(Bool e) {
        Bool res = new Bool(e.b);
        return res;
    }

    public Int visit(Int e) {
        Int res = new Int(e.i);
        return res;
    }

    public Float visit(Float e) { 
        Float res = new Float(e.f);
        return res;
    }

    public Not visit(Not e) {
        Not res = new Not(e.e.accept(this));
        return res;
    }

    public Neg visit(Neg e) {
        Neg res = new Neg(e.e.accept(this));
        return res;
    }

    public Add visit(Add e) {
        Add res = new Add(e.e1.accept(this), e.e2.accept(this));
        return res;
    }

    public Sub visit(Sub e) {
        Sub res = new Sub(e.e1.accept(this), e.e2.accept(this));
        return res;
    }

    public FNeg visit(FNeg e){
        FNeg res = new FNeg(e.e.accept(this));
        return res;
    }

    public FAdd visit(FAdd e){
        FAdd res = new FAdd(e.e1.accept(this), e.e2.accept(this));
        return res;
    }

    public FSub visit(FSub e){
        FSub res = new FSub(e.e1.accept(this), e.e2.accept(this));
        return res;
    }

    public FMul visit(FMul e) {
        FMul res = new FMul(e.e1.accept(this), e.e2.accept(this));
        return res;
    }

    public FDiv visit(FDiv e){
        FDiv res = new FDiv(e.e1.accept(this), e.e2.accept(this));
        return res;
    }

    public Eq visit(Eq e){
        Eq res = new Eq(e.e1.accept(this), e.e2.accept(this));
        return res;
    }

    public LE visit(LE e){
        LE res = new LE(e.e1.accept(this), e.e2.accept(this));
        return res;
    }

    public If visit(If e){
        If res = new If(e.e1.accept(this), e.e2.accept(this), e.e3.accept(this));
        return res;
    }

    public Let visit(Let e) {
        Let res = new Let(e.id, e.t, e.e1.accept(this), e.e2.accept(this));
        return res;
    }

    public Var visit(Var e){
        Var res = new Var(e.id);
        return res;
    }

    public LetRec visit(LetRec e){
        LetRec res = new LetRec(e.fd, e.e.accept(this));
        return res;
    }

    public App visit(App e){
        List<Exp> listeAncienne = e.es;
        List<Exp> listeRes = new ArrayList();
        for(Exp el:listeAncienne){
            listeRes.add(el.accept(this)); 
        }
        
        App res = new App(e.e.accept(this), listeRes);
        return res;
    }

    public Tuple visit(Tuple e){
        List<Exp> listeAncienne = e.es;
        List<Exp> listeRes = new ArrayList();
        for(Exp el:listeAncienne){
            listeRes.add(el.accept(this)); 
        }
        
        Tuple res = new Tuple(listeRes);
        return res;
    }

    public LetTuple visit(LetTuple e){
        List<Id> listeAncienneId = e.ids;
        List<Id> listeResId = new ArrayList();
        for(Id id:listeAncienneId){
            listeResId.add(id); 
        }
        
        List<Type> listeAncienneT = e.ts;
        List<Type> listeResT = new ArrayList();
        for(Type t:listeAncienneT){
            listeResT.add(t); 
        }
        LetTuple res = new LetTuple(listeResId, listeResT, e.e1.accept(this), e.e2.accept(this));
        return res;
    }

    public Array visit(Array e){
        Array res = new Array(e.e1.accept(this), e.e2.accept(this));
        return res;
    }

    public Get visit(Get e){
        Get res = new Get(e.e1.accept(this), e.e2.accept(this));
        return res;
    }

    public Put visit(Put e){
        Put res = new Put(e.e1.accept(this), e.e2.accept(this), e.e3.accept(this));
        return res;
    }
    
    public FunDef visit(FunDef f){
        List<Id> listeAncienneId = f.args;
        List<Id> listeResId = new ArrayList();
        for(Id id:listeAncienneId){
            listeResId.add(id); 
        }
        
        FunDef res = new FunDef(f.id, f.type, listeResId, f.e.accept(this));
        return res;
    }
    
    @Override
    public Exp visit(Apply_direct e) {
        return e;
    }
    
    @Override
    public Exp visit(Apply_closure e) {
        return e;
    }
    
    @Override
    public Exp visit(Make_closure e) {
        return e;
    }
}


