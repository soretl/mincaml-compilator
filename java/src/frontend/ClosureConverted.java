package frontend;

import java.util.*;
import mincaml.tree.Exp;

import mincaml.tree.FunDef;
import mincaml.tree.Id;
import mincaml.tree.Var;
import mincaml.visitor.PrintVisitor;

public class ClosureConverted {
    Exp main;   //Racine de l'AST
    List<FunDef> fonctions; //Liste des fonctions sans variables libres de l'AST
    List<Closure> fonctionsClosure; //Liste des fonctions avec des variables libres de l'AST
    
    /**
     * Constructeur de ClosureConverted
     * @param e La racine de l'AST
     */
    public ClosureConverted(Exp e){
        main = e;
        fonctions = new ArrayList();
        fonctionsClosure = new ArrayList();
    }
    
    /**
     * @return la racine de l'AST 
     */
    public Exp getMain(){
        return main;
    }
    
    /**
     * Affiche les attributs des fonctions sans variables libres
     */
    public void afficherFonctions(){
        for(FunDef fd:fonctions){
            System.out.println("Id: "+fd.id);
            System.out.print("Arguments:");
            for(Id arg:fd.args){
                System.out.print(" "+arg);
            }
            System.out.println();
            System.out.print("Corps:");
            fd.e.accept(new PrintVisitor());
            System.out.println();
            System.out.println();
        }
    }
    
    /**
     * Affiche les attributs des fonctions sans variables libres
     */
    public void afficherClosures(){
        for(Closure c:fonctionsClosure){
            FunDef fd = c.fd;
            System.out.println("Id: "+fd.id);
            System.out.print("Arguments:");
            for(Id arg:fd.args){
                System.out.print(" "+arg);
            }
            System.out.println();
            System.out.print("Variables Libres:");
            for(Var fv:c.fv){
                System.out.print(" "+fv.id.toString());
            }
            System.out.println();
            System.out.print("Corps:");
            fd.e.accept(new PrintVisitor());
            System.out.println();
            System.out.println();
        }
    }
    
    /**
     * Cherche dans les fonctions avec closure celle
     * correspondant à l'identifiant en entrée, et renvoie la liste 
     * des variables libres de cette fonctions
     * @param id
     * @return La liste des variables libres de la fonction d'identifiant id
     */
    public List<Var> trouverVariablesLibres(Id id){
        for(Closure c:fonctionsClosure){
            if(c.fd.id.toString().equals(id.toString())){
                return c.fv;
            }
        }
        return new ArrayList();
    }
}
