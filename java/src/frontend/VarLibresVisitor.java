package frontend;

import asml.tree.Formal_arg;
import asml.tree.Id;
import asml.tree.Noeud;
import asml.tree.Toplevel;
import asml.tree.asmt.Asmt;
import asml.tree.asmt.FinAsmt;
import asml.tree.asmt.LetId;
import asml.tree.exp.*;
import asml.tree.fundef.Fun;
import asml.tree.fundef.Fundef;
import asml.tree.fundef.Main;
import asml.tree.ident_or_imm.Ident;
import asml.tree.ident_or_imm.Ident_or_Arm;
import asml.tree.ident_or_imm.Ident_or_imm;
import asml.tree.ident_or_imm.Register;
import asml.tree.ident_or_imm.StackOffset;
import asml.visitor.AsmlObjVisitor;
import asml.visitor.AsmlVisitor;
import java.util.ArrayList;
import java.util.List;

/**
 * Visiteur permettant de convertir les variables 
 * d'une liste donnée en MemLoad, dans le cadre des 
 * variables libres
 */
public class VarLibresVisitor implements AsmlObjVisitor<Noeud>{
    List<String> fv;    //Liste des variables libres recherchées
    
    public VarLibresVisitor(List<mincaml.tree.Var> fv){
        List<String> listeRes = new ArrayList();
        for(mincaml.tree.Var temp_var:fv){
            listeRes.add(temp_var.id.toString());
        }
        this.fv = listeRes;
    }

    @Override
    public Int visit(Int e) {
        return e;
    }

    /**
     * Si la variable libre est celle recherchée,
     * la convertir en MemLoad sur self
     * @param e
     * @return 
     */
    @Override
    public Noeud visit(Ident e) {
        int i = 0;
        int emplacement = 0;
        for(String temp_fv:fv){
            i++;
            if(temp_fv.equals(e.id.toString())){
                emplacement = i;
            }
        }
        if(emplacement > 0){
            Ident adr = new Ident(Id.genSelf());
            return new MemLoad(adr, new Int(emplacement));
        }
        return e;
    }

    @Override
    public Label visit(Label e) {
        return e;
    }

    @Override
    public Neg visit(Neg e) {
        return new Neg((Ident)e.accept(this));
    }

    @Override
    public Add visit(Add e) {
        return new Add((Ident)e.left.accept(this), (Ident)e.right.accept(this));
    }

    @Override
    public Sub visit(Sub e) {
        return new Sub((Ident)e.left.accept(this), (Ident)e.right.accept(this));
    }

    @Override
    public MemLoad visit(MemLoad e) {
        return new MemLoad((Ident)e.address.accept(this), (Ident)e.offset.accept(this));
    }

    @Override
    public MemStore visit(MemStore e) {
        return new MemStore((Ident_or_Arm)e.address.accept(this), (Ident_or_imm)e.offset.accept(this), (Ident_or_Arm)e.source.accept(this));
    }

    @Override
    public If visit(If e) {
        return new If(e.condition, (Ident_or_Arm)e.condLeft.accept(this), (Ident_or_imm)e.condRight.accept(this), (Asmt)e.nextThen.accept(this), (Asmt)e.nextElse.accept(this));
    }

    @Override
    public Call visit(Call e) {
        return new Call((Label)e.l.accept(this), (Formal_arg)e.args.accept(this));
    }

    @Override
    public Noeud visit(CallClo e) {
        return new CallClo((Ident)e.ident.accept(this), (Formal_arg)e.args.accept(this));
    }

    @Override
    public New visit(New e) {
        return new New((Ident_or_imm)e.ident.accept(this));
    }

    @Override
    public Nop visit(Nop e) {
        return e;
    }

    @Override
    public Formal_arg visit(Formal_arg e) {
        Formal_arg listeRes = new Formal_arg();
        for(Ident_or_Arm temp_id:e.args){
            listeRes.add_end((Ident_or_Arm)temp_id.accept(this));
        }
        return listeRes;
    }

    @Override
    public Noeud visit(LetId e) {
        return new LetId((Ident_or_Arm)e.id.accept(this), (Exp)e.exp.accept(this), (Asmt)e.next.accept(this));
    }

    @Override
    public Noeud visit(FinAsmt e) {
        return new FinAsmt((Exp)e.exp.accept(this));
    }

    @Override
    public Noeud visit(Main e) {
        return new Main((Asmt)e.asmt.accept(this));
    }

    @Override
    public Noeud visit(Fun e) {
        return new Fun((Label)e.label.accept(this), (Formal_arg)e.args.accept(this), (Asmt)e.functionCore.accept(this), (Fundef)e.next.accept(this));
    }

    @Override
    public Noeud visit(Toplevel e) {
        return new Toplevel((Fundef)e.f.accept(this));
    }

    @Override
    public Noeud visit(StackOffset e) {
        return e;
    }

    @Override
    public Noeud visit(Register e) {
        return e;
    }

}