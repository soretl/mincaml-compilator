package backend;

import asml.tree.Formal_arg;
import asml.tree.Toplevel;
import asml.tree.asmt.FinAsmt;
import asml.tree.asmt.LetId;
import asml.tree.exp.*;
import asml.tree.fundef.Fun;
import asml.tree.fundef.Main;
import asml.tree.ident_or_imm.Ident;
import asml.tree.ident_or_imm.Ident_or_Arm;
import asml.tree.ident_or_imm.Register;
import asml.tree.ident_or_imm.StackOffset;
import asml.visitor.AsmlObjVisitor;

/**
 * 
 * Visiteur qui permet de générer du code ARM
 *
 */
public class SimpleGenerator implements AsmlObjVisitor<String>{

    ExpRegGenerator erg;
    public int ifThenElseCounter = 0;
    
    static final int NB_REG_FCT = 4; //nb de registre contenant les arguments avant de push les arguments sur la pile lors d'un call
	
	public SimpleGenerator() {
		erg = new ExpRegGenerator(this);
	}
	
	
	@Override
	public String visit(Int e) {
		return "#"+e.i;
	}

	@Override
	public String visit(Ident e) {
		if(e.id.getId().charAt(0)=='_') {
			return e.id.getId().substring(1);
		}
		return e.id.toString();
	}

	@Override
	public String visit(Label e) {
		return e.id.toString().substring(1, e.id.toString().length());
	}

	@Override
	public String visit(Call e) {
		String res="";
		res+=e.args.accept(this);
		res += "\tsub sp, sp, #4\n"; //place pour que les offsets soit ok (normalement cette place est utilisé pour la closure)
		res+="\tbl "+e.l.accept(this)+"\n";
		res += "\tadd sp, sp, #4\n";//place closure
		if(e.args.args.size()>NB_REG_FCT)
			res+="\tadd sp, sp, #"+(e.args.args.size()-NB_REG_FCT)*4+"\n";
		return res;
	}

	@Override
	public String visit(Formal_arg e) {
		String res = "";
		for(int i=e.args.size()-1;i>=NB_REG_FCT;i--){
			res+=e.args.get(i).accept(erg,"r2");
			res+="\tsub sp, sp, #4\n";
			res+="\tstr r2, [sp]\n";
		}
		for(int i=0;i<NB_REG_FCT && i<e.args.size();i++) {
			res+=e.args.get(i).accept(erg,"r"+i);
		}
		return res;
	}

	@Override
	public String visit(LetId e) {
		String res="";
		String resfin="";
		if(e.id instanceof StackOffset){
			res+=e.exp.accept(erg,"r12");
			if(((StackOffset)e.id).aPush){
				res+="\tsub sp, sp, #4\n";//TODO EN 1 OPERATION POUR TOUTES LES VARIABLES LOCALES
				resfin="\tadd sp, sp, #4\n";//TODO EN 1 OPERATION POUR TOUTES LES VARIABLES LOCALES
			}
			res+="\tstr r12, "+e.id.toString()+"\n";
		}
		else if(e.id instanceof Register){
			res+=e.exp.accept(erg,e.id.toString());
		}
		else{
			res+= e.exp.accept(this);
			//assert false:"Error: bad tree";
		}
		return res+e.next.accept(this)+resfin;
	}

	@Override
	public String visit(FinAsmt e) {
		return e.exp.accept(this);
	}

	@Override
	public String visit(Main e) {
		String res="";
		//int nvl = e.asmt.accept(new LocalVariablesCounterVisitor());
		//res += "@nvl: "+nvl+"\n";
		res+="_start:\n\tsub fp, sp, #8\n\tstmfd sp!, {r4-r10,fp,lr}\n";
		//res += "\tsub sp, sp, #"+(nvl*4)+"\n";
		res+=e.asmt.accept(this);
		//res += "\tadd sp, sp, #"+(nvl*4)+"\n";
		res+="\tldmfd sp!, {r4-r10,fp,lr}\n\tbl min_caml_exit\n";
		return res;
	}

	@Override
	public String visit(MemLoad e) {
		//ne devrait pas arriver
		return "[UNDEFINED: MemLoad]\n";
	}

	@Override
	public String visit(MemStore e) {
		String res = "";
		res += e.address.accept(erg, "r0");
		if(e.offset instanceof Int) {
			res += ((Int)e.offset).accept(erg,"r1");
		}
		else if(e.offset instanceof Ident_or_Arm){
			res += ((Ident_or_Arm)e.offset).accept(erg,"r1");
		}
		else {
			assert false : "bad tree";
		}
		res += e.source.accept(erg,"r2");
		res += "\tbl min_caml_array_set\n";
		return res;
	}

	@Override
	public String visit(Neg e) {
		//ne devrait pas arriver
		return "[UNDEFINED: Neg]\n";
	}

	@Override
	public String visit(Add e) {
		//ne devrait pas arriver
		return "[UNDEFINED: Add]\n";
	}

	@Override
	public String visit(Sub e) {
		//ne devrait pas arriver
		return "[UNDEFINED: Sub]\n";
	}

	@Override
	public String visit(If e) {
		String res="";
		int Lthen = ifThenElseCounter;
		int Lfin = ifThenElseCounter+1;
		//int nvl_then = e.nextThen.accept(new LocalVariablesCounterVisitor());
		//int nvl_else = e.nextElse.accept(new LocalVariablesCounterVisitor());;
		ifThenElseCounter+=2;
		if(e.condLeft instanceof Register)
			res = "\tcmp "+e.condLeft+", "+e.condRight.accept(this)+"\n";
		else if(e.condLeft instanceof StackOffset) {
			if(e.condRight instanceof StackOffset) {
				res += ((StackOffset)e.condRight).accept(erg,"r3");
				res += e.condLeft.accept(erg,"r2")+"\tcmp r2, r3\n";
			} 
			else {
				res += e.condLeft.accept(erg,"r2")+"\tcmp r2, "+e.condRight.accept(this)+"\n";
			}

		}
		else {
			assert false:"Error: bad tree";
		}
		//res += "@then:"+nvl_then+"   else:"+nvl_else+"\n";
		switch(e.condition) {
		case EQ:
			res += "\tbeq .L"+Lthen+"\n";
			break;
		case FEQ:
			res += "\t[UNDEFINED: FEQ]\n";
			break;
		case FLE:
			res += "\t[UNDEFINED: FLE]\n";
			break;
		case GE:
			res += "\tbge .L"+Lthen+"\n";
			break;
		case LE:
			res += "\tble .L"+Lthen+"\n";
			break;
		}
		//else
		//res += "\tsub sp, sp, #"+(nvl_else*4)+"\n";
		res += e.nextElse.accept(this)+"\tb .L"+Lfin+"\n";
		//res += "\tadd sp, sp, #"+(nvl_else*4)+"\n";
		//then
		res += ".L"+Lthen+":\n";
		//res += "\tsub sp, sp, #"+(nvl_then*4)+"\n";
		res += e.nextThen.accept(this);
		//res += "\tadd sp, sp, #"+(nvl_then*4)+"\n";
		res += ".L"+Lfin+":\n";
		return res;
	}

	@Override
	public String visit(CallClo e) {
		String res = "";
		res+="\tstmfd sp!, {r4}\t@_____callclo_____\n";
		res += e.ident.accept(erg,"r3");
		res += "\tmov r0, r3\n\tmov r1, #0\n\tbl min_caml_array_get\n";
		res += "\tmov r4, r0\n";//r4<-fonction a appeler

		for(int i=e.args.args.size()-1;i>=SimpleGenerator.NB_REG_FCT;i--){
			res+=e.args.args.get(i).accept(erg,"r2");
			res+="\tsub sp, sp, #4\n";
			res+="\tstr r2, [sp]\n ";
		}
		//res += "\tsub sp, sp, #4\n\tstr r3, [sp]\n";//push de la closure sur la pile

		for(int i=0;i<SimpleGenerator.NB_REG_FCT && i<e.args.args.size();i++) {
			res+=e.args.args.get(i).accept(erg,"r"+i);
		}
		res += "\tmov lr, pc\n";
		res += "\tmov pc, r4\n";
		if(e.args.args.size()>SimpleGenerator.NB_REG_FCT)
			res+="\tadd sp, sp, #"+(e.args.args.size()-SimpleGenerator.NB_REG_FCT)*4+"\n";
		res+="\tldmfd sp!, {r4}\n";
		return res;
	}

	@Override
	public String visit(New e) {
		return "[UNDEFINED: New]\n";
	}

	@Override
	public String visit(Nop e) {
		return "";
	}


	@Override
	public String visit(Fun e) {
		String res = e.label.accept(this)+":\n";
		res+="\tstmfd sp!, {r4-r10,fp,lr}\n\tadd fp, sp, #28\n";
		//int nvl = e.functionCore.accept(new LocalVariablesCounterVisitor());
		//res+="@nvl: "+nvl+"\n";
		//res += "\tsub sp, sp, #"+(nvl*4)+"\n";
		res+=e.functionCore.accept(erg,"r0");
		//res += "\tadd sp, sp, #"+(nvl*4)+"\n";
		res+="\tldmfd sp!, {r4-r10,fp,lr}\n\tbx lr\n";
		return res+e.next.accept(this);
	}


	@Override
	public String visit(Toplevel e) {
		String res="";
		res+="\t.data\n\t.text\n\t.global _start\n";
		return res+e.f.accept(this);
	}


	@Override
	public String visit(StackOffset e) {
		return e.toString();
	}


	@Override
	public String visit(Register e) {
		return e.toString();
	}

}
