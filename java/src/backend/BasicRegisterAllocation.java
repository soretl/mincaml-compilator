package backend;

import java.util.HashMap;

import asml.tree.Formal_arg;
import asml.tree.Toplevel;
import asml.tree.Noeud;
import asml.tree.asmt.Asmt;
import asml.tree.asmt.FinAsmt;
import asml.tree.asmt.LetId;
import asml.tree.exp.*;
import asml.tree.fundef.Fun;
import asml.tree.fundef.Fundef;
import asml.tree.fundef.Main;
import asml.tree.ident_or_imm.*;
import asml.visitor.AsmlObjVisitor;
import asml.visitor.AsmlVisitor;

/**
 * Allocateur "spill everything"
 * Visiteur qui remplace toutes les variables en adresses sur la pile de la forme [fp,offset]
 * Maintient le sommet de la pile dans currentOffset et les variables déjà allouées dans la hashmap varaibleMap
 */
public class BasicRegisterAllocation implements AsmlObjVisitor<Noeud> {

    private static int baseOffset = -32;
    private static int baseArguments = 12;

    HashMap<Ident_or_Arm,Ident_or_Arm> variableMap;
    int currentOffset;

    public BasicRegisterAllocation(){
        variableMap = new HashMap<Ident_or_Arm,Ident_or_Arm>();
        currentOffset = baseOffset; // a cause du store de r4-r12 et lr (10 registres)
    }

    public HashMap<Ident_or_Arm,Ident_or_Arm> getVariableMap() {
        return variableMap;
    }

    @Override
    public Noeud visit(Int e) {
        //Pas utilisé
        return e;
    }

    @Override
    public Noeud visit(Ident e) {
        if (!e.id.isSelf() && (e.id.toString().charAt(0) != '_')) {
            return (Noeud) variableMap.get(e);
        } else {
            return e;
        }
    }

    @Override
    public Noeud visit(Label e) {
        //Pas utilisé
        return e;
    }

    @Override
    public Noeud visit(Neg e) {
        e.ident = (Ident_or_Arm) e.ident.accept(this);
        return e;
    }

    @Override
    public Noeud visit(Add e) {
        e.left = (Ident_or_Arm) e.left.accept(this);
        e.right = (Ident_or_imm) e.right.accept(this);
        return e;
    }

    @Override
    public Noeud visit(Sub e) {
        e.left = (Ident_or_Arm) e.left.accept(this);
        e.right = (Ident_or_imm) e.right.accept(this);
        return e;
    }

    @Override
    public Noeud visit(MemLoad e) {
        e.address = (Ident_or_Arm) e.address.accept(this);
        e.offset = (Ident_or_imm) e.offset.accept(this);
        return e;
    }

    @Override
    public Noeud visit(MemStore e) {
        e.offset = (Ident_or_imm) e.offset.accept(this);
        e.source = (Ident_or_Arm) e.source.accept(this);
        e.address = (Ident_or_Arm) e.address.accept(this);
        return e;
    }

    @Override
    public Noeud visit(If e) {
        e.condLeft = (Ident_or_Arm) e.condLeft.accept(this);
        e.condRight = (Ident_or_imm) e.condRight.accept(this);
        int tmp = this.currentOffset;
        e.nextThen = (Asmt) e.nextThen.accept(this);
        this.currentOffset = tmp;
        e.nextElse = (Asmt) e.nextElse.accept(this);
        return e;
    }

    @Override
    public Noeud visit(Call e) {
        for (int i=0;i<e.args.args.size();i++) {
            e.args.args.set(i,this.variableMap.get(e.args.args.get(i)));
        }
        return e;
    }

    @Override
    public Noeud visit(CallClo e) {
        e.ident = (Ident_or_Arm) e.ident.accept(this);
        for (int i=0;i<e.args.args.size();i++) {
            e.args.args.set(i, this.variableMap.get(e.args.args.get(i)));
        }
        return e;
    }

    @Override
    public Noeud visit(New e) {
        e.ident = (Ident_or_imm) e.ident.accept(this);
        return e;
    }

    @Override
    public Noeud visit(Nop e) {
        return e;
    }

    @Override
    public Noeud visit(Formal_arg e) {
        int offset = baseArguments;
        LetId result = null;
        LetId current;
        LetId previous = null;

        for (int i=0;i<e.args.size();i++) {
            if(i<4){ //Les 4 premiers argument sont dans r0-r3
                this.variableMap.put(e.args.get(i),new StackOffset(currentOffset,false));
                e.args.set(i,new Register(i));
                current = new LetId(new Register(i),new StackOffset(currentOffset,true),null);
                if(i==0){
                    result = current;
                }
                if(previous!=null){
                    previous.setNext(current);
                }
                current = previous;
                currentOffset+=4;

            } else {
                this.variableMap.put(e.args.get(i),new StackOffset(offset,false));
                e.args.set(i,new StackOffset(offset,false));
                offset+=4;
            }
        }

        return result;
    }

    @Override
    public Noeud visit(LetId e) {
        if(!((Ident)e.id).id.isUnused()) { // Variable inutile ?
            if (!this.variableMap.containsKey(e.id)) {
                this.variableMap.put(e.id, new StackOffset(currentOffset, false));
                e.id = new StackOffset(currentOffset, true);
                currentOffset -= 4;
            } else {
                e.id = (Ident_or_Arm) e.id.accept(this);
            }
        }
    	e.exp = (Exp) e.exp.accept(this);
        e.next = (Asmt) e.next.accept(this);
        return e;
    }

    @Override
    public Noeud visit(FinAsmt e) {
        e.exp = (Exp) e.exp.accept(this);
        return e;
    }

    @Override
    public Noeud visit(Main e) {
        e.asmt = (Asmt) e.asmt.accept(this);
        return e;
    }

    @Override
    public Noeud visit(Fun e) {
        int offset = baseArguments;
        LetId result = null;
        LetId current = null;
        LetId previous = null;
        currentOffset=baseOffset;

        for (int i=0;i<e.args.args.size();i++) {
            if (i < 4) {
                this.variableMap.put(e.args.args.get(i), new StackOffset(currentOffset, false));
                e.args.args.set(i, new Register(i));
                current = new LetId(new StackOffset(currentOffset, true),new Register(i), null);
                if (i == 0) {
                    result = current;
                }
                if (previous != null) {
                    previous.setNext(current);
                }
                previous = current;
                currentOffset -= 4;

            } else {
                this.variableMap.put(e.args.args.get(i), new StackOffset(offset, false));
                e.args.args.set(i, new StackOffset(offset, false));
                offset += 4;
            }
        }

        //-------------------------------------------
        e.functionCore = (Asmt) e.functionCore.accept(this);
        if(current != null){
            current.setNext(e.functionCore);
            e.functionCore = result;
        }
        this.currentOffset=baseOffset;
        e.next = (Fundef) e.next.accept(this);
        return e;
    }

    @Override
    public Noeud visit(Toplevel e) {
        e.f = (Fundef) e.f.accept(this);
	    return e;
    }

	@Override
	public Noeud visit(StackOffset e) {
		// Pas utilisé
		return e;
	}

	@Override
	public Noeud visit(Register e) {
        // Pas utilisé
		return e;
	}
}
