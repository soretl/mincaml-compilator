package backend;

import asml.tree.Formal_arg;
import asml.tree.Noeud;
import asml.tree.Toplevel;
import asml.tree.asmt.FinAsmt;
import asml.tree.asmt.LetId;
import asml.tree.exp.*;
import asml.tree.fundef.Fun;
import asml.tree.fundef.Main;
import asml.tree.ident_or_imm.Ident;
import asml.tree.ident_or_imm.Ident_or_Arm;
import asml.tree.ident_or_imm.Register;
import asml.tree.ident_or_imm.StackOffset;
import asml.visitor.AsmlObjVisitor;
import tools.Pair;

import java.util.HashMap;

/**
 * Calcul dans livesIntervals la durée de vie de chaque variables.
 * Le début de vie d'une variable est la profondeur dans l'arbre de sa définition
 * La fin de vie d'une variable est la profondeur de sa dérnière utilisation
 */
public class LiveIntervalsVisitor implements AsmlObjVisitor<Noeud> {

    HashMap<Ident_or_Arm, Pair<Integer,Integer>> livesIntervals;
    int currentDepth;

    public LiveIntervalsVisitor() {
        livesIntervals = new HashMap<Ident_or_Arm, Pair<Integer,Integer>>();
        this.currentDepth = 0;
    }

    public HashMap<Ident_or_Arm, Pair<Integer, Integer>> getLivesIntervals() {
        return livesIntervals;
    }

    @Override
    public Noeud visit(Int e) {
        return null;
    }

    @Override
    public Noeud visit(Ident e) {
        if(!e.id.isSelf() && e.id.toString().charAt(0) != '_') {
            Pair<Integer, Integer> interval = this.livesIntervals.get(e);
            this.livesIntervals.put(e, new Pair<Integer, Integer>(interval.getKey(), Math.max(this.currentDepth, interval.getValue())));
        }
        return null;
    }

    @Override
    public Noeud visit(Label e) {
        return null;
    }

    @Override
    public Noeud visit(Neg e) {
        e.ident.accept(this);
        return null;
    }

    @Override
    public Noeud visit(Add e) {

        e.left.accept(this);
        e.right.accept(this);
        return null;
    }
    @Override
    public Noeud visit(Sub e) {
        e.left.accept(this);
        e.right.accept(this);
        return null;
    }

    @Override
    public Noeud visit(MemLoad e) {

        e.address.accept(this);

        e.offset.accept(this);
        return null;
    }

    @Override
    public Noeud visit(MemStore e) {
        e.address.accept(this);
        e.source.accept(this);
        e.offset.accept(this);
        return null;
    }

    @Override
    public Noeud visit(If e){
        //TODO : TESTS
        //CondLeft
        e.condLeft.accept(this);
        //CondRight
        e.condRight.accept(this);

        e.nextThen.accept(this);
        e.nextElse.accept(this);

        return null;
    }

    @Override
    public Noeud visit(Call e) {
        for (Ident_or_Arm ident: e.args.args) {
            ident.accept(this);
        }
        return null;
    }

    @Override
    public Noeud visit(CallClo e) {

        e.ident.accept(this);

        for (Ident_or_Arm ident: e.args.args) {
            ident.accept(this);
        }
        return null;
    }

    @Override
    public Noeud visit(New e) {
        return e;
    }

    @Override
    public Noeud visit(Nop e) {
        return e;
    }

    @Override
    public Noeud visit(Formal_arg e) {
        return null;
    }

    @Override
    public Noeud visit(LetId e) {
        if(livesIntervals.containsKey(e.id)){
            e.id.accept(this);
        } else {
            this.livesIntervals.put(e.id,new Pair<Integer, Integer>(this.currentDepth,-1));
        }
        e.exp.accept(this);
        this.currentDepth++;
        e.next.accept(this);
        return null;
    }

    @Override
    public Noeud visit(FinAsmt e) {
        e.exp.accept(this);
        return null;
    }

    @Override
    public Noeud visit(Main e) {
        e.asmt.accept(this);
        return null;
    }

    @Override
    public Noeud visit(Fun e) {

        int startingFctOffset = currentDepth;
        for (Ident_or_Arm ident: e.args.args) {
            this.livesIntervals.put(ident,new Pair<Integer, Integer>(startingFctOffset,currentDepth));
        }
        e.functionCore.accept(this);
        for (Ident_or_Arm ident: e.args.args) {
            this.livesIntervals.put(ident,new Pair<Integer, Integer>(startingFctOffset,currentDepth));
        }
        this.currentDepth++;
        e.next.accept(this);
        return null;
    }

    @Override
    public Noeud visit(Toplevel e) {
        e.f.accept(this);
        return null;
    }

    @Override
    public Noeud visit(StackOffset e) {
        return null;
    }

    @Override
    public Noeud visit(Register e) {
        return null;
    }
}