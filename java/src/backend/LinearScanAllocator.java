package backend;

import asml.tree.Formal_arg;
import asml.tree.Noeud;
import asml.tree.Toplevel;
import asml.tree.asmt.Asmt;
import asml.tree.asmt.FinAsmt;
import asml.tree.asmt.LetId;
import asml.tree.exp.*;
import asml.tree.fundef.Fun;
import asml.tree.fundef.Fundef;
import asml.tree.fundef.Main;
import asml.tree.ident_or_imm.*;
import asml.visitor.AsmlObjVisitor;
import tools.Pair;

import java.util.*;

/**
 * Allocateur "linear scan"
 * 
 */
public class LinearScanAllocator implements AsmlObjVisitor<Noeud> {

    private static int baseOffset = -32;
    private static int baseArguments = 12;

    //Intervals de vie des variables
    HashMap<Ident_or_Arm, Pair<Integer,Integer>> livesIntervals;

    //Variables acctuellement alouées dans des registres
    ArrayList<Pair<Ident_or_Arm,Pair<Integer,Integer>>> active;

    static final int r = 7; // nombre de registres

    /*  Registres r4-10
        true : libre
        false : occupé
    */
    ArrayList<Boolean> registerStates = new ArrayList<Boolean>(){{
        add(true);
        add(true);
        add(true);
        add(true);
        add(true);
        add(true);
        add(true);
    }};

    // Map de variable vers registre/offset courrant
    HashMap<Ident_or_Arm,Ident_or_Arm> variableMap;

    private int currentMaxOffset;

    /**
     * Alloue un registre non utilisé et le déclare comme utilisé dans registerSrates
     * @return le numéro du registre alloué
     */
    public int allocateRegistor(){
        int i=0;
        if(active.size() == r){
            System.err.println("[Erreur interne] Allocation impossible, plus de registres disponibles");
            return -1;
        }
        while(!registerStates.get(i)){ i++; }
        registerStates.set(i,false);
        return i+4;
    }

    /**
     * Alloue un nouvel offset en fonction de currentMaxOffset, modifie ce dernier
     * @return l'offset alloué
     */
    public int allocateOffset(){
        int res = this.currentMaxOffset;
        this.currentMaxOffset -= 4;
        return res;
    }

    public LinearScanAllocator(HashMap<Ident_or_Arm, Pair<Integer,Integer>> livesIntervals){
        this.livesIntervals = livesIntervals;
        this.active = new ArrayList<Pair<Ident_or_Arm,Pair<Integer,Integer>>>();
        this.variableMap = new HashMap<Ident_or_Arm,Ident_or_Arm>();
        this.currentMaxOffset = baseOffset;

        for(Ident_or_Arm key:livesIntervals.keySet()){
            if(livesIntervals.get(key).getValue() == -1){
                livesIntervals.put(key,new Pair<Integer, Integer>(livesIntervals.get(key).getKey(),livesIntervals.get(key).getKey()));
            }
        }
    }

    /**
     * Alloue un registre ou un offset à la variable i dans variableMap
     * @param i La variable à allouer
     * @return un LetId si on doit spill une variable déjà dans un registre null sinon
     */
    public LetId linearScanRegisterAllocation(Pair<Ident_or_Arm,Pair<Integer,Integer>> i){

        expireOldIntervals(i);
        if(active.size() == r){ //Plus de place dans les registres
            return spillAtIntervals(i);
        } else {
            variableMap.put(i.getKey(),new Register(allocateRegistor()));
            active.add(i);
            active.sort(Comparator.comparingInt(a -> a.getValue().getValue()));
            return null;
        }
    }

    /**
     * Met i sur la pile si i est la variable qui à la fin de vie la plus éloignée sinon
     * met sur la pile la variable qui à la fin de vie la plus éloignée et met i dans son registre
     * @param i La variable à allouer
     * @return un LetId si on doit spill une variable déjà dans un registre null sinon
     */
    private LetId spillAtIntervals(Pair<Ident_or_Arm, Pair<Integer, Integer>> i) {
        Pair<Ident_or_Arm, Pair<Integer, Integer>> spill = active.get(active.size()-1);
        if(spill.getValue().getValue() > i.getValue().getValue()){
            variableMap.put(i.getKey(),variableMap.get(spill.getKey())); //register[i] <- register[spill]
            Ident_or_Arm registerOld = variableMap.get(spill.getKey());
            variableMap.put(spill.getKey(),new StackOffset(allocateOffset(),true)); // location[spill] <- new stack location
            active.remove(spill); // remove spill from active
            active.add(i); //add i to active
            active.sort(Comparator.comparingInt(a -> a.getValue().getValue()));
            return new LetId(variableMap.get(spill.getKey()), (Exp) registerOld,null);
        } else {
            variableMap.put(i.getKey(),new StackOffset(allocateOffset(),false)); //location[i] <- new stack location
            return null;
        }
    }

    /**
     * Supprime de active les variables qui ne sont plus vivante au moment du traitement de i
     * @param i
     */
    private void expireOldIntervals(Pair<Ident_or_Arm, Pair<Integer, Integer>> i) {
        ArrayList<Pair<Ident_or_Arm,Pair<Integer,Integer>>> tmp = new ArrayList<Pair<Ident_or_Arm,Pair<Integer,Integer>>>(active);

        for(Iterator<Pair<Ident_or_Arm, Pair<Integer, Integer>>> iterator = tmp.iterator(); iterator.hasNext();){
            Pair<Ident_or_Arm, Pair<Integer, Integer>> j = iterator.next();
            if(j.getValue().getValue() >= i.getValue().getKey()){
                return;
            }
            active.remove(j);
            if(variableMap.get(j.getKey()) instanceof Register){
                registerStates.set(((Register) variableMap.get(j.getKey())).getNumero()-4,true); //add register[i] to pool of free registers
            } else {
                System.err.println("[Erreur interne] Expiration d'intervals expirés sur autre chose qu'un registre.");
            }
            variableMap.remove(j.getKey());
            active.sort(Comparator.comparingInt(a -> a.getValue().getValue()));
        }

    }


    @Override
    public Noeud visit(Int e) {
        return e;
    }

    @Override
    public Noeud visit(Ident e) {
        if(!e.id.isSelf() && (e.id.toString().charAt(0) != '_')) {
            return (Noeud) variableMap.get(e).clone();
        } else {
            return e;
        }
    }

    @Override
    public Noeud visit(Label e) {
        return e;
    }

    @Override
    public Noeud visit(Neg e) {
        e.ident = (Ident_or_Arm) e.ident.accept(this);
        return e;
    }

    @Override
    public Noeud visit(Add e) {
        e.left = (Ident_or_Arm) e.left.accept(this);
        e.right = (Ident_or_imm) e.right.accept(this);
        return e;
    }

    @Override
    public Noeud visit(Sub e) {
        e.left = (Ident_or_Arm) e.left.accept(this);
        e.right = (Ident_or_imm) e.right.accept(this);
        return e;
    }

    @Override
    public Noeud visit(MemLoad e) {

        e.address = (Ident_or_Arm) e.address.accept(this);
        e.offset = (Ident_or_imm) e.offset.accept(this);

        return e;
    }

    @Override
    public Noeud visit(MemStore e) {
        e.address = (Ident_or_Arm) e.address.accept(this);
        e.source = (Ident_or_Arm) e.source.accept(this);
        e.offset = (Ident_or_imm) e.offset.accept(this);

        return e;
    }

    @Override
    public Noeud visit(If e) {
        e.condLeft = (Ident_or_Arm) e.condLeft.accept(this);
        e.condRight = (Ident_or_imm) e.condRight.accept(this);

        int tmpOffset = this.currentMaxOffset;
        ArrayList<Boolean> tmpRegstate = this.registerStates;
        ArrayList<Pair<Ident_or_Arm,Pair<Integer,Integer>>> tmpActive =this.active;
        e.nextThen = (Asmt) e.nextThen.accept(this);
        this.currentMaxOffset = tmpOffset;
        this.registerStates = tmpRegstate;
        this.active = tmpActive;
        e.nextElse = (Asmt) e.nextElse.accept(this);

        return e;
    }

    @Override
    public Noeud visit(Call e) {

        for (int i=0;i<e.args.args.size();i++) {
            Ident_or_Arm tmp = (Ident_or_Arm) e.args.args.get(i).accept(this);
            if(tmp instanceof StackOffset){
                ((StackOffset) tmp).aPush = false;
            }
            e.args.args.set(i,tmp);
        }
        return e;
    }

    @Override
    public Noeud visit(CallClo e) {

        e.ident = (Ident_or_Arm) e.ident.accept(this);
        for (int i=0;i<e.args.args.size();i++) {
            Ident_or_Arm tmp = (Ident_or_Arm) e.args.args.get(i).accept(this);
            if(tmp instanceof StackOffset){
                ((StackOffset) tmp).aPush = false;
            }
            e.args.args.set(i,tmp);
        }
        return e;
    }

    @Override
    public Noeud visit(New e) {
        e.ident = (Ident_or_imm) e.ident.accept(this);
        return e;
    }

    @Override
    public Noeud visit(Nop e) {
        return e;
    }


    //TODO : jamais appellé
    @Override
    public Noeud visit(Formal_arg e) {
        int offset = baseArguments;
        for (int i=0;i<e.args.size();i++) {
            if(i<4){
                this.variableMap.put(e.args.get(i),new Register(i));

                e.args.set(i,new Register(i));
            } else {
                this.variableMap.put(e.args.get(i),new StackOffset(offset,false));
                e.args.set(i,new StackOffset(offset,false));
                offset+=4;
            }
        }
        return e;
    }

    @Override
    public Noeud visit(LetId e) {
        LetId res = null;
        if(!((Ident)e.id).id.isUnused()) { // Variable inutile ?
            if (variableMap.containsKey(e.id)) { //réassignation ?
                e.id = (Ident_or_Arm) e.id.accept(this);
                if (e.id instanceof StackOffset) {
                    ((StackOffset) e.id).aPush = false;
                }

            } else {
                res = linearScanRegisterAllocation(new Pair<>(e.id, livesIntervals.get(e.id)));
                if (res != null) {
                    res.setNext(e);
                } else {
                    res = e;
                }
                e.id = (Ident_or_Arm) e.id.accept(this);
                if (e.id instanceof StackOffset) {
                    ((StackOffset) e.id).aPush = true;
                }
            }
        }

        e.exp = (Exp) e.exp.accept(this);

        e.next = (Asmt) e.next.accept(this);

        if(res != null){
            return res;
        } else {
            return e;
        }
    }

    @Override
    public Noeud visit(FinAsmt e) {
        e.exp = (Exp) e.exp.accept(this);
        return e;
    }

    @Override
    public Noeud visit(Main e) {
        e.asmt.accept(this);
        return e;
    }

    @Override
    public Noeud visit(Fun e) {

        int offset = baseArguments;
        LetId result = null;
        LetId current = null;
        LetId previous = null;
        this.currentMaxOffset = baseOffset;

        //Parcours les arguments, on sauvegarde r0-r3 celon la callee convention
        for (int i=0;i<e.args.args.size();i++) {
            if (i < 4) {
                linearScanRegisterAllocation(new Pair<>(e.args.args.get(i),livesIntervals.get(e.args.args.get(i))));
                current = new LetId(variableMap.get(e.args.args.get(i)),new Register(i), null);
                e.args.args.set(i, new Register(i));
                if (i == 0) {
                    result = current;
                }
                if (previous != null) {
                    previous.setNext(current);
                }
                previous = current;

            } else {
                this.variableMap.put(e.args.args.get(i), new StackOffset(offset, false));
                e.args.args.set(i, new StackOffset(offset, false));
                offset += 4;
            }
        }

        //----------------------------------------------
        //e.args = (Formal_arg) e.args.accept(this);
        e.functionCore = (Asmt) e.functionCore.accept(this);
        this.currentMaxOffset = baseOffset;
        if(current != null){
            current.setNext(e.functionCore);
            e.functionCore = result;
        }
        e.next = (Fundef) e.next.accept(this);
        return e;
    }

    @Override
    public Noeud visit(Toplevel e) {
        e.f.accept(this);
        return e;
    }

    @Override
    public Noeud visit(StackOffset e) {
        return e;
    }

    @Override
    public Noeud visit(Register e) {
        return e;
    }
}
