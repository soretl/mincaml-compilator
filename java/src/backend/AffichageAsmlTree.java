package backend;

import asml.parser.AsmlReader;
import asml.tree.Toplevel;
import asml.visitor.AsmlPrintVisitor;

public class AffichageAsmlTree {

	public static void main(String[] argv) {
		try {

			Toplevel arbre = AsmlReader.read(argv[0]);     

			assert (arbre != null);

			System.out.println("----------- ASML AST ------------");
			arbre.accept(new AsmlPrintVisitor());
			System.out.println();
			System.out.println("------------ REGISTER AND MEMORY ALLOCATION ------------");
            System.out.println("------------           Linear scan          ------------");
            LiveIntervalsVisitor liveIntervalsVisitor = new LiveIntervalsVisitor();
			/*
            arbre.accept(liveIntervalsVisitor);
            System.out.println();
            System.out.println(liveIntervalsVisitor.getLivesIntervals());
            arbre.accept(new LinearScanAllocator(liveIntervalsVisitor.getLivesIntervals()));
			System.out.println();
			System.out.println();
			arbre.accept(new AsmlPrintVisitor());
			System.out.println();
            */
            System.out.println("------------             Basic              ------------");
            BasicRegisterAllocation regAlloc = new BasicRegisterAllocation();
			arbre.accept(regAlloc);
            System.out.println(regAlloc.getVariableMap().toString());
            System.out.println();
            System.out.println();
            arbre.accept(new AsmlPrintVisitor());
			System.out.println();

            System.out.println("--------------- ARM ---------------");
			//TODO
			System.out.println(arbre.accept(new SimpleGenerator()));
			//arbre.accept(new AsmlPrintVisitor());
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
