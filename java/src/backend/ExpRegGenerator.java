package backend;

import asml.tree.Formal_arg;
import asml.tree.asmt.FinAsmt;
import asml.tree.asmt.LetId;
import asml.tree.exp.*;
import asml.tree.fundef.Fun;
import asml.tree.fundef.Main;
import asml.tree.ident_or_imm.Ident;
import asml.tree.ident_or_imm.Ident_or_Arm;
import asml.tree.ident_or_imm.Register;
import asml.tree.ident_or_imm.StackOffset;
import asml.visitor.ObjArgVisitor;
import tools.CoupeEntier;
import tools.Pair;

/**
 * 
 * Visiteur qui permet de générer du code ARM
 * (contrairement à SimpleGenerator, le registre destination est ici passé en paramètre sous forme d'un string)
 *
 */
public class ExpRegGenerator implements ObjArgVisitor<String, String> {

	SimpleGenerator sg;
	
	public ExpRegGenerator(SimpleGenerator sg) {
		this.sg=sg;
	}
	
	@Override
	public String visit(Int e, String arg) {
		String res="";//"\tmov "+arg+", "+e.accept(sg)+"\n";
		int v = e.i;
		int i = -1;
		if(v<256) {
			res += "\tmov "+arg+", #"+v+"\n";	
		}
		else {
			res += "\tmov "+arg+", #0\n";	
			while(i!=0) {
				Pair<Integer,Integer> p = CoupeEntier.Coupe(v, 255);
				int nv = p.getKey();
				i = p.getValue();
				if(i!=0) {
					res += "\tmov r3, #"+nv+"\n";
					res += "\tadd "+arg+", r3, LSL #"+i+"\n";
				}
				else {
					res += "\tadd "+arg+", "+arg+", #"+nv+"\n";
				}
				v=v-(nv<<i);
			}
		}
		return res;
	}

	@Override
	public String visit(Ident e, String arg) {
		if(e.id.isSelf()) {
			return "\tldr "+arg+", [fp,#8]\n";
		}
		if(e.id.getId().charAt(0)=='_') {
			return "\tldr "+arg+", ="+e.id.getId().substring(1)+"\n";
		}
		return "[TODO: Ident de ExpRegGenerator]"+e.id;
	}

	@Override
	public String visit(Label e, String arg) {
		// TODO Auto-generated method stub
		return "\tldr "+arg+", ="+e.accept(sg)+"\n";
	}

	@Override
	public String visit(Neg e, String arg) {
		String res=e.ident.accept(this,"r2");
		res+="\trsb "+arg+", r2, #0\n";
		return res;
	}

	@Override
	public String visit(Add e, String arg) {
		String res="",ra,rb;
		if(e.left instanceof StackOffset){
			res += "\tldr r2, "+e.left.accept(sg)+"\n";
			ra = "r2";
		}
		else{
			ra = e.left.accept(sg);
		}
		
		if(e.right instanceof StackOffset){
			res += "\tldr r3, "+e.right.accept(sg)+"\n";
			rb = "r3";
		}
		else{
			rb = e.right.accept(sg);
		}
		res += "\tadd "+arg+", "+ra+", "+rb+"\n";
		return res;
	}

	@Override
	public String visit(Sub e, String arg) {
		String res="",ra,rb;
		if(e.left instanceof StackOffset){
			res += "\tldr r2, "+e.left.accept(sg)+"\n";
			ra = "r2";
		}
		else{
			ra = e.left.accept(sg);
		}
		
		if(e.right instanceof StackOffset){
			res += "\tldr r3, "+e.right.accept(sg)+"\n";
			rb = "r3";
		}
		else{
			rb = e.right.accept(sg);
		}
		res += "\tsub "+arg+", "+ra+", "+rb+"\n";
		return res;
	}

	@Override
	public String visit(MemLoad e, String arg) {
		String res = "";
		res += e.address.accept(this, "r0");
		if(e.offset instanceof Int) {
			res += ((Int)e.offset).accept(this,"r1");
		}
		else if(e.offset instanceof Ident_or_Arm){
			res += ((Ident_or_Arm)e.offset).accept(this,"r1");
		}
		else {
			assert false : "bad tree";
		}
		res += "\tbl min_caml_array_get\n\tmov "+arg+", r0\n";
		return res;
	}

	@Override
	public String visit(MemStore e, String arg) {
		// TODO Auto-generated method stub
		return e.accept(sg);
	}

	@Override
	public String visit(If e, String arg) {
		String res="";
		int Lthen = sg.ifThenElseCounter;
		int Lfin = sg.ifThenElseCounter+1;
		//int nvl_then = e.nextThen.accept(new LocalVariablesCounterVisitor());
		//int nvl_else = e.nextElse.accept(new LocalVariablesCounterVisitor());;
		sg.ifThenElseCounter+=2;
		if(e.condLeft instanceof Register)
			res = "\tcmp "+e.condLeft+", "+e.condRight.accept(sg)+"\n";
		else if(e.condLeft instanceof StackOffset) {
			if(e.condRight instanceof StackOffset) {
				res += ((StackOffset)e.condRight).accept(this,"r3");
				res += e.condLeft.accept(this,"r2")+"\tcmp r2, r3\n";
			} 
			else {
				res += e.condLeft.accept(this,"r2")+"\tcmp r2, "+e.condRight.accept(sg)+"\n";
			}

		}
		else {
			assert false:"Error: bad tree";
		}
		//res += "@then:"+nvl_then+"   else:"+nvl_else+"\n";
		switch(e.condition) {
		case EQ:
			res += "\tbeq .L"+Lthen+"\n";
			break;
		case FEQ:
			res += "\t[UNDEFINED: FEQ]\n";
			break;
		case FLE:
			res += "\t[UNDEFINED: FLE]\n";
			break;
		case GE:
			res += "\tbge .L"+Lthen+"\n";
			break;
		case LE:
			res += "\tble .L"+Lthen+"\n";
			break;
		}
		//else
		//res += "\tsub sp, sp, #"+(nvl_else*4)+"\n";
		res += e.nextElse.accept(this,arg)+"\tb .L"+Lfin+"\n";
		//res += "\tadd sp, sp, #"+(nvl_else*4)+"\n";
		//then
		res += ".L"+Lthen+":\n";
		//res += "\tsub sp, sp, #"+(nvl_then*4)+"\n";
		res += e.nextThen.accept(this,arg);
		//res += "\tadd sp, sp, #"+(nvl_then*4)+"\n";
		res += ".L"+Lfin+":\n";
		return res;
	}

	@Override
	public String visit(Call e, String arg) {
		String res="";
		res+=e.args.accept(sg);
		res += "\tsub sp, sp, #4\n"; //place pour que les offsets soit ok (normalement cette place est utilisé pour la closure)
		res+="\tbl "+e.l.accept(sg)+"\n";
		if(e.args.args.size()>SimpleGenerator.NB_REG_FCT)
			res+="\tadd sp, sp, #"+(e.args.args.size()-SimpleGenerator.NB_REG_FCT)*4+"\n";
		res += "\tadd sp, sp, #4\n";//place closure
		res += "\tmov "+arg+", r0\n";
		return res;
	}

	@Override
	public String visit(CallClo e, String arg) {
		String res = "";
		//res+="\tstmfd sp!, {r4}\t@_____callclo_____\n";
		res += e.ident.accept(this,"r3");
		res += "\tmov r0, r3\n\tmov r1, #0\n\tbl min_caml_array_get\n";
		res += "\tmov r12, r0\n";//r4<-fonction a appeler

		for(int i=e.args.args.size()-1;i>=SimpleGenerator.NB_REG_FCT;i--){
			res+=e.args.args.get(i).accept(this,"r2");
			res+="\tsub sp, sp, #4\n";
			res+="\tstr r2, [sp]\n";
		}
		res += "\tsub sp, sp, #4\n\tstr r3, [sp]\n";//push de la closure sur la pile
		
		for(int i=0;i<SimpleGenerator.NB_REG_FCT && i<e.args.args.size();i++) {
			res+=e.args.args.get(i).accept(this,"r"+i);
		}
		res += "\tmov lr, pc\n";
		res += "\tmov pc, r12\n";
		res += "\tmov "+arg+", r0\n";
		res += "\tadd sp, sp, #4\n";
		if(e.args.args.size()>SimpleGenerator.NB_REG_FCT)
			res+="\tadd sp, sp, #"+(e.args.args.size()-SimpleGenerator.NB_REG_FCT)*4+"\n";
		//res+="\tldmfd sp!, {r4}\n";
		return res;
	}

	@Override
	public String visit(New e, String arg) {
		//adresse et offset en bytes !
		/*String res = "\tldr r2, ptr_hp\n\tldr r3,[r2]\n";
		if(e.ident instanceof Int) {
			res += ((Int)e.ident).accept(this,"r1");
		}
		else {
			res += ((Ident_or_Arm)e.ident).accept(this,"r1");
		}
		res += "\tadd r1, r3, r1\n";
		res+="\tstr r1, [r2]\n";
		res += "\tmov "+arg+", r3\n";*/
		String res = "";
		if(e.ident instanceof Int) {
			res += ((Int)e.ident).accept(this,"r0");
		}
		else {
			res += ((Ident_or_Arm)e.ident).accept(this,"r0");
		}
		res += "\tmov r1, #0\n";
		res += "\tbl min_caml_create_array\n";
		res += "\tmov "+arg+", r0\n";
		return res;
	}

	@Override
	public String visit(Nop e, String arg) {
		return "";
	}

	@Override
	public String visit(Formal_arg e, String arg) {
		// TODO Auto-generated method stub
		return "[TODO: Formal_arg de ExpRegGenerator]";
	}

	@Override
	public String visit(LetId e, String arg) {
		String res="";
		String resfin="";
		if(e.id instanceof StackOffset){
			res+=e.exp.accept(this,"r12");
			if(((StackOffset)e.id).aPush){
				res+="\tsub sp, sp, #4\n";
				resfin="\tadd sp, sp, #4\n";//TODO EN 1 OPERATION POUR TOUTES LES VARIABLES LOCALES
			}
			res+="\tstr r12, "+e.id.toString()+"\n";
		}
		else if(e.id instanceof Register){
			res+=e.exp.accept(this,e.id.toString());
		}
		else{
			res+= e.exp.accept(sg);
			//assert false:"Error: bad tree";
		}
		return res+e.next.accept(this,arg)+resfin;
	}

	@Override
	public String visit(FinAsmt e, String arg) {
		return e.exp.accept(this,arg);
	}

	@Override
	public String visit(Main e, String arg) {
		// TODO Auto-generated method stub
		return "[TODO: Main de ExpRegGenerator]";
	}

	@Override
	public String visit(Fun e, String arg) {
		// TODO Auto-generated method stub
		return "[TODO: Fun de ExpRegGenerator]";
	}

	@Override
	public String visit(StackOffset e, String arg) {
		return "\tldr "+arg+", "+e.toString()+"\n";
	}

	@Override
	public String visit(Register e, String arg) {
		return "\tmov "+arg+", "+e.toString()+"\n";
	}

}
