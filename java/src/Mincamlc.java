import asml.visitor.AsmlPrintVisitor;
import backend.LinearScanAllocator;
import backend.LiveIntervalsVisitor;
import mincaml.visitor.PrintVisitor;
import org.apache.commons.cli.*;
import asml.parser.AsmlReader;
import asml.tree.Toplevel;
import asml.visitor.AsmlWriteVisitor;
import backend.BasicRegisterAllocation;
import backend.SimpleGenerator;
import frontend.ASMLGen;
import frontend.Alpha_RedVisitor;
import frontend.ClosureConverted;
import frontend.ClosureVisitor;
import frontend.KNormVisitor;
import frontend.Let_RedVisitor;
import mincaml.parser.MincamlReader;
import mincaml.tree.Exp;
import mincaml.typechecking.GenEquations;

import java.io.PrintWriter;

public class Mincamlc{
	private static boolean debug = false;

	enum Mode {
		ALL,			// compilation total mincaml -> arm + typechecking
		TYPECHECKING,	// uniquement typechecking
		FRONTEND,		// uniquement frontend (entree mincaml/ sortie arbre asml)
		BACKEND,		// uniquement backend (entree: asml)
		PARSE			// parse uniquement l'entree (sortie arbre asml)
	}

	enum OptimisationLevel{
	    SPILL_EVERYTHING,	// Toutes les variables sur la pile
        LINEAR_ALLOCATOR;	// scan linéaire

		public static String toString(OptimisationLevel o){
	    	switch (o){
				case SPILL_EVERYTHING:
					return "Spill everything";
				case LINEAR_ALLOCATOR:
					return "Linear scan";
				default:
					return "UNDEFINED";
			}
		}
    }

	/**
	 * Traduction minCaml -> asml
	 * @param mincamlArbre arbre syntaxique mincaml a traduire
	 * @return l'arbre syntaxique asml correspondant
	 */
	public static Toplevel applyFrontend(Exp mincamlArbre){
		Exp knorm = mincamlArbre.accept(new KNormVisitor());
		Exp alpha_red = knorm.accept(new Alpha_RedVisitor());
		Exp let_red = alpha_red.accept(new Let_RedVisitor());
		ClosureVisitor cv = new ClosureVisitor(let_red);
		ClosureConverted closureConv = cv.getClosureConverted();
		ASMLGen aGen = new ASMLGen(closureConv);
		return aGen.getTop();
	}

	/**
	 * Traduction asml -> arm
	 * @param optLvl Niveau d'optimisation d'allocation de registres
	 * @param asmlArbre arbre syntaxique asml a traduire
	 * @return Une String contenant le code arm correspondant
	 */
	public static String applyBackend(OptimisationLevel optLvl,Toplevel asmlArbre) {
		assert (asmlArbre != null);
		switch (optLvl) {
			case SPILL_EVERYTHING:
				asmlArbre.accept(new BasicRegisterAllocation());
				break;
			case LINEAR_ALLOCATOR:
				LiveIntervalsVisitor liveIntervalsVisitor = new LiveIntervalsVisitor();
				asmlArbre.accept(liveIntervalsVisitor);
				asmlArbre.accept(new LinearScanAllocator(liveIntervalsVisitor.getLivesIntervals()));
				break;
			default:
				break;
		}
		printAsml(asmlArbre,optLvl,false);
		return asmlArbre.accept(new SimpleGenerator());
	}

	/**
	 * affiche un arbre syntaxique mincaml
	 * @param mincamlArbre arbre syntaxique mincaml
	 */
	public static void printMinCaml(Exp mincamlArbre){
		if(debug){
			System.out.println("--------------------------= MINCAML =--------------------------");
			mincamlArbre.accept(new PrintVisitor());
			System.out.println();
			System.out.println();
		}
	}


	/**
	 * affiche un arbre syntaxique asml
	 * @param asmlArbre arbre syntaxique asml
	 */
	public static void printAsml(Toplevel asmlArbre, OptimisationLevel optlvl, boolean mode){
		if(debug){
			if(mode){
				System.out.println("----------------------------= ASML =----------------------------");
			} else {
				System.out.println("---------------------= REGISTER ALLOCATION =--------------------");
				System.out.println("OPTIMISATION LVL = "+ OptimisationLevel.toString(optlvl));
			}
			System.out.println();
			asmlArbre.accept(new AsmlPrintVisitor());
			System.out.println();
			System.out.println();
		}
	}

	/**
	 * affiche un code arm
	 * @param armCode code arm
	 */
	public static void printArm(String armCode){
		if(debug){
			System.out.println("-----------------------------= ARM =----------------------------");
			System.out.println(armCode);
			System.out.println();
		}
	}

	/**
	 * Gere les erreurs, termine le programme et renvois 1
	 * @param e	erreur
	 * @param debug	booleen pour savoir si on affiche un message d'erreur
	 */
	public static void handleError(Exception e,boolean debug){
		if(debug) {
			e.printStackTrace();
		}
		System.exit(1);
	}

	public static void main(String argv[]){

		//Etat
		Mode mode = Mode.ALL;
		OptimisationLevel optLvl = OptimisationLevel.SPILL_EVERYTHING;
		CommandLine commandLine;

		// Définition des options
		final Option versionOption = Option.builder("v")
				.longOpt("version")
				.desc("affiche la version")
				.required(false)
				.build();

		final Option helpOption = Option.builder("h")
				.longOpt("help")
				.desc("affiche l'aide")
				.required(false)
				.build();

		final Option outputOption = Option.builder("o")
				.longOpt("output")
				.desc("fichier de sortie")
				.hasArg(true)
				.required(false)
				.build();

		final Option readAsmlOption = Option.builder("backend")
				.desc("uniquement backend (entree: asml)")
				.required(false)
				.build();
		
		final Option frontendOption = Option.builder("frontend")
				.desc("uniquement frontend (entree mincaml/ sortie arbre asml)")
				.required(false)
				.build();
		
		final Option typeCheckingOption = Option.builder("t")
				.longOpt("typechecking")
				.desc("uniquement typechecking")
				.required(false)
				.build();

        final Option optimisationOption = Option.builder("O")
                .longOpt("optimisation")
                .desc("optimisation: 0=Spill everything, 1=Linear Scan")
                .hasArg(true)
                .required(false)
                .build();

		final Option parseOnlyOption = Option.builder("p")
				.longOpt("parse-only")
				.desc("parse uniquement l'entree (sortie arbre asml)")
				.hasArg(false)
				.required(false)
				.build();
		final Option debugOption = Option.builder("d")
				.longOpt("debug")
				.desc("Affiche une trace si une erreur de compilation survient")
				.hasArg(false)
				.required(false)
				.build();

		final Options options = new Options();
		options.addOption(versionOption);
		options.addOption(helpOption);
		options.addOption(outputOption);
		options.addOption(readAsmlOption);
		options.addOption(optimisationOption);
		options.addOption(frontendOption);
		options.addOption(typeCheckingOption);
		options.addOption(parseOnlyOption);
		options.addOption(debugOption);

		//Déclaration des fichiers IO
		String input_file=null;
		String output_file=null;

		// Déclaration des arbres minCaml, asml et du Arm résultat
		Toplevel asmlArbre = null;
		Exp mincamlArbre = null;
		String result;

		/****************************************
		 * 			OPTIONS PARSING				*
		 ****************************************/

		try{
			commandLine = new DefaultParser().parse(options,argv);
			if(commandLine.hasOption("v")){
				System.out.println("mincamlc - version 0.1");
				return;
			}
			else if(commandLine.hasOption("h")){
				HelpFormatter f = new HelpFormatter();
				f.printHelp("mincamlc <fichier_entree>",options,true);
				return;
			}
			else if(commandLine.hasOption("backend")){
				mode=Mode.BACKEND;
			}
			else if(commandLine.hasOption("frontend")){
				mode=Mode.FRONTEND;
			}
			else if(commandLine.hasOption("t")){
				mode=Mode.TYPECHECKING;
			} else if(commandLine.hasOption("p")) {
				mode=Mode.PARSE;
			}
			if(commandLine.hasOption("d")){
				debug = true;
			}
			if(commandLine.hasOption("o")){
				output_file = commandLine.getOptionValue("o");
			}
			if(commandLine.getArgs().length<1){
				System.err.println("Aucun fichier d'entree specifie");
				System.exit(1);
			}
			//Optimisation
			if(commandLine.hasOption("O")){
				switch (commandLine.getOptionValue("O")){
				case "1":
					optLvl = OptimisationLevel.LINEAR_ALLOCATOR;
					break;
				case "0":
				default:
					optLvl = OptimisationLevel.SPILL_EVERYTHING;
					break;
				}
			}
			input_file = commandLine.getArgs()[0];
		}
		catch(Exception e){
			handleError(e,true);
		}

		/****************************************
		 * 				COMPILATION				*
		 ****************************************/

		if(mode==Mode.BACKEND){ //Lecture de l'arbre ASML
			try {
				asmlArbre = AsmlReader.read(input_file);
			} catch (Exception e) {
				handleError(e,debug);
			}
			assert (asmlArbre != null);
		} else { //Lecture de l'arbre minCaml
			try {
				mincamlArbre = MincamlReader.read(input_file);
			} catch (Exception e) {
				handleError(e,debug);
			}
			assert (mincamlArbre != null);
			if(mode==Mode.PARSE){
				return;
			}
		}

		switch (mode){
			case TYPECHECKING:
				try {
					GenEquations.start(mincamlArbre);
				} catch (Exception e) {
					handleError(e,debug);
				}
				break;
			case FRONTEND:
				try {
					asmlArbre = applyFrontend(mincamlArbre);
					printAsml(asmlArbre,optLvl,true);

					if(output_file!=null){
						new AsmlWriteVisitor(asmlArbre, output_file);
					}
				} catch (Exception e) {
					handleError(e,debug);
				}
				break;
			case BACKEND:
				try {
					printAsml(asmlArbre,optLvl,false);
					result = applyBackend(optLvl,asmlArbre);
					printArm(result);

					if(output_file!=null){
						PrintWriter pw = new PrintWriter(output_file);
						pw.print(result);
						pw.close();
					}
				} catch (Exception e) {
					handleError(e,debug);
				}
				break;
			case ALL:
				try {
					printMinCaml(mincamlArbre);
					GenEquations.start(mincamlArbre);
					asmlArbre = applyFrontend(mincamlArbre);
					printAsml(asmlArbre,optLvl,true);
					result = applyBackend(optLvl,asmlArbre);
					printArm(result);

					if(output_file!=null){
						PrintWriter pw = new PrintWriter(output_file);
						pw.print(result);
						pw.close();
					}

				} catch (Exception e) {
					handleError(e,debug);
				}
				break;
			default:
				System.exit(1);
		}
	}
}
