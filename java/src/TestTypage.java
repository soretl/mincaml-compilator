
import mincaml.parser.*;
import mincaml.tree.*;
import mincaml.typechecking.*;
import mincaml.visitor.PrintVisitor;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author rotivala
 */
public class TestTypage {

    static public void main(String argv[]) {
        try {
            Exp expression = MincamlReader.read(argv[0]);
            assert (expression != null);
            GenEquations te = new GenEquations();

            System.out.println("------ AST ------");
            expression.accept(new PrintVisitor());
            System.out.println();

            System.out.println();

            System.out.println("STOP - TYPETIME");

            te.start(expression);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
