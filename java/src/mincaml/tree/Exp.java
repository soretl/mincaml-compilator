package mincaml.tree;
import mincaml.visitor.ObjVisitor;
import mincaml.visitor.Visitor;

public abstract class Exp {
    public abstract void accept(Visitor v);

    public abstract <E> E accept(ObjVisitor<E> v);
}
