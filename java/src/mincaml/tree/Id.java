package mincaml.tree;

public class Id {
    public String id;
    public Id(String id) {
        this.id = id;
    }
    @Override
    public String toString() {
        return id;
    }
    static int x = -1;
    public static Id gen() {
        x++;
        return new Id("v" + x);
    }
    
    static int unused = -1;
    public static Id genUnused() {
        unused++;
        return new Id("u" + unused);
    }

    public String getId(){
    	return id;
    }
}
