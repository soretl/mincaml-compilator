package mincaml.tree;

import java.util.List;

import mincaml.visitor.ObjVisitor;
import mincaml.visitor.Visitor;

public class Apply_closure extends Exp {
	public final Id id;
	public final List<Exp> es;

    public Apply_closure(Id id, List<Exp> es) {
        this.id = id;
        this.es = es;
    }

    public <E> E accept(ObjVisitor<E> v) {
        return v.visit(this);
    }
    public void accept(Visitor v) {
        v.visit(this);
    }
}