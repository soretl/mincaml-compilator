package mincaml.tree;

import java.util.List;

import mincaml.type.Type;

public class FunDef {
	public Id id;
    public final Type type;
    public final List<Id> args;
    public  Exp e;

    public FunDef(Id id, Type t, List<Id> args, Exp e) {
        this.id = id;
        this.type = t;
        this.args = args;
        this.e = e;
    }
 
}