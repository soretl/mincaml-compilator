package mincaml.tree;

import java.util.List;

import mincaml.visitor.ObjVisitor;
import mincaml.visitor.Visitor;

public class Make_closure extends Exp {
	public final Id id;
	public final List<Var> fv;

    public Make_closure(Id id, List<Var> fv) {
        this.id = id;
        this.fv = fv;
    }

    public <E> E accept(ObjVisitor<E> v) {
        return v.visit(this);
    }
    public void accept(Visitor v) {
        v.visit(this);
    }
}