package mincaml.tree;

import mincaml.visitor.ObjVisitor;
import mincaml.visitor.Visitor;

public class Var extends Exp {
	public Id id;

    public Var(Id id) {
        this.id = id;
    }

    public <E> E accept(ObjVisitor<E> v) {
        return v.visit(this);
    }
    public void accept(Visitor v) {
        v.visit(this);
    }
    
    /*
    Makes it much easier to manage the variable environment
    */
    @Override
    public String toString() {
        return id.toString();
    }
}