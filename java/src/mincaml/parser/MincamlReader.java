package mincaml.parser;

import java.io.FileReader;

import mincaml.tree.Exp;


public class MincamlReader {
	public static Exp read(String filename) throws Exception{
		Parser p;
		Lexer l = new Lexer(new FileReader(filename));
		p = new Parser(l);

		Exp arbre = (Exp) p.parse().value;      
		return arbre;
	}
}
