/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mincaml.type;

import mincaml.typechecking.EquationVisitor;
import java.util.ArrayList;

/**
 *
 * @author rotivala
 */
public class TFun extends Type { 
    public ArrayList<Type> from;
    public Type to;
    public TFun(ArrayList<Type> from, Type to) {
        this.from = from;
        this.to = to;
    }
    
    @Override
    public String toString(){
        String ret = new String();
        /*
        Functions are easy to read that way, which isn't really useful per se but
        did help while debuggint
        */
        for(int i = 0; i < from.size(); i++)
        {
            if(from.get(i).getClass().getSimpleName().equals("TFun"))
            {
                ret = ret.concat("(");
            }
            
            
            ret = ret.concat(from.get(i).toString());
            
            
            if(from.get(i).getClass().getSimpleName().equals("TFun"))
            {
                ret = ret.concat(")");
            }
            
            
            if(i < from.size())
            {
                ret = ret.concat("=>");
            }
        }
        
        if(to.getClass().getSimpleName().equals("TFun"))
            {
                ret = ret.concat("(");
            }
        
        ret = ret.concat(to.toString());
        
        if(to.getClass().getSimpleName().equals("TFun"))
            {
                ret = ret.concat(")");
            }
        return ret;
    }
    
    @Override
    public boolean accept(EquationVisitor v) {
        return v.visit(this);
    }
}