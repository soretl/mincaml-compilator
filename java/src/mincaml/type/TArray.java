/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mincaml.type;


import mincaml.typechecking.EquationVisitor;

/**
 *
 * @author rotivala
 */
public class TArray extends Type { 
    public Type t;  //Since all of an array has to be the same type, it mainly acts as any other type does
    
    public TArray(Type t) {
        this.t = t;
        
    }

    @Override
    public String toString(){        
        return "[" + t.toString() + "]";
    }
    
        @Override
    public boolean accept(EquationVisitor v) {
        return v.visit(this);
    }
}