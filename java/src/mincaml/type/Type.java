package mincaml.type;

import mincaml.typechecking.EquationVisitor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;

public abstract class Type {
    private static int x = 0;
    static HashMap<String,Type> ht = new HashMap<>();
    public static Type gen() {
        Type t = new TVar("?" + x++);
        ht.put(t.toString(), t);
        return t;
    }
    
    public abstract boolean accept(EquationVisitor v);
    
}













