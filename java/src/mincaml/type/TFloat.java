/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mincaml.type;

import mincaml.typechecking.EquationVisitor;

/**
 *
 * @author rotivala
 */
public class TFloat extends Type { 
    public TFloat() {

    }
    
    @Override
    public String toString(){
        return "float";
    }
    
    @Override
    public boolean accept(EquationVisitor v) {
        return v.visit(this);
    }
}