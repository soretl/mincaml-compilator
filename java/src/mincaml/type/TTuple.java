/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mincaml.type;

import mincaml.typechecking.EquationVisitor;
import java.util.ArrayList;

/**
 *
 * @author rotivala
 */
public class TTuple extends Type { 
    public ArrayList<Type> tlist;
    public TTuple(ArrayList<Type> tlist) {
        this.tlist = tlist;
    }

    @Override
    public String toString(){
        String ret = new String();
        ret = ret.concat("(");
        for(int i = 0; i < tlist.size(); i++)
        {
            ret = ret.concat(tlist.get(i).toString());
            if(i != tlist.size() - 1)
            {
                ret = ret.concat(", ");
            }
        }
        ret = ret.concat(")");
        return ret;
    }
    
    @Override
    public boolean accept(EquationVisitor v) {
        return v.visit(this);
    }
    
}

