/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mincaml.type;

import mincaml.typechecking.EquationVisitor;

/**
 *
 * @author rotivala
 */
public class TUnit extends Type {

    public TUnit() {
    }
    
    @Override
    public String toString()
    {
        return "unit";
    }

    @Override
    public boolean accept(EquationVisitor v) {
        return v.visit(this);
    }
    
}