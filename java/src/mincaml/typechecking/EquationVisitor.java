package mincaml.typechecking;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import mincaml.type.TArray;
import mincaml.type.TBool;
import mincaml.type.TFloat;
import mincaml.type.TFun;
import mincaml.type.TInt;
import mincaml.type.TTuple;
import mincaml.type.TUnit;
import mincaml.type.TVar;
import mincaml.type.Type;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * EquationVisitor. does exactly as its name says : it visits the equations in
 * the list given - as well as the variable environment - to replace occurences
 * of a variable type with another type. Aside from that, it acts as an ordinary
 * Visitor.
 *
 * @author rotivala
 */
public class EquationVisitor {

    Type t1;
    Type t2;
    HashMap<String, Type> env;

    /**
     *
     * @param t1 : the type to be replaced
     * @param t2 : the type with which to replace it
     * @param env : the variable environment
     */
    public EquationVisitor(Type t1, Type t2, HashMap<String, Type> env) {
        this.t1 = t1;
        this.t2 = t2;
        this.env = env;
    }

    /* Note : in all the functions of the visitor there is a return, which is
    often a fixed false, since it can only be true when the types match.
     */
    public boolean visit(ArrayList<Equations> le) {
        if (t1.toString().equals(t2.toString())) {
            return false;
        }
        for (Equations e : le) {
            this.visit(e);
        }
        replaceInEnv();   //Acts differently, since it's not technically part of the list of Equations
        return false;
    }

    /**
     * The root of the "equation"
     *
     * @param e : the equation to visit
     * @return : always false
     */
    public boolean visit(Equations e) {
        if (e.t1.accept(this)) {
            e.t1 = t2;
        }

        if (e.t2.accept(this)) {
            e.t2 = t2;
        }
        return false;
    }

    public boolean visit(TUnit e) {
        return false;
    }

    public boolean visit(TBool e) {
        return false;
    }

    public boolean visit(TInt e) {
        return false;
    }

    public boolean visit(TFloat e) {
        return false;
    }

    public boolean visit(TVar e) {

        if (e.toString().equals(t1.toString())) {
            return true;
        }

        return false;
    }

    public boolean visit(TFun e) {
        for (int i = 0; i < e.from.size(); i++) {
            e.from.get(i).accept(this);

            if (e.from.get(i).accept(this)) {
                if (!e.from.getClass().getSimpleName().equals("TFun")) {

                    Type tfrom = t2;
                    e.from.add(tfrom);
                    tfrom = e.from.remove(i);
                }
                e.from.set(i, t2);
            }

            if (e.to.accept(this)) {

                e.to = t2;
                Type tfrom = e.from.get(i);

                if (e.to.getClass().getSimpleName().equals("TFun")) {

                    e.from.set(i, ((TFun) e.to).from.get(i));
                    e.from.add(tfrom);
                    e.to = ((TFun) e.to).to;
                }

            }
        }
        return false;

    }

    public boolean visit(TTuple e) {
        for (Type t : e.tlist) {
            
            if(t.accept(this))
            {
                t = t2;
            }
        }
        return false;
    }

    public boolean visit(TArray e) {
        if (e.t.accept(this)) {
            e.t = t2;
        }
        return false;
    }

    
    
    
    /*
    Here we enter the part of the code which acts differently (which is to say
    it doesn't act as a visitor (it didn't make sense in this context).
    So the Environment is visited in a more traditionnal manner.
    */

    public boolean replaceInEnv() {
        Set envSet = env.entrySet();
        Iterator<Entry> it = envSet.iterator();
        while (it.hasNext()) {

            Entry e = it.next();
            if (e.getValue().toString().equals(t1.toString())) {
                String key = (String) e.getKey();
                env.put(key, t2);

            }

            switch(e.getValue().getClass().getSimpleName())
            {
                case "TFun":
                    replaceInFun((TFun) e.getValue());
                    break;
                    
                case "TTuple" :
                    replaceInTuple((TTuple) e.getValue());
                    break;
                    
                case "TArray" :
                    replaceInArray((TArray) e.getValue());
                    break;
            }
        }
        return false;
    }
    
    

    public boolean replaceInFun(TFun f) {
        
        switch(f.to.getClass().getSimpleName())
        {
            case "TFun" :
                replaceInFun((TFun) f.to);
                break;
                
            case "TTuple" :
                replaceInTuple((TTuple) f.to);
                break;
                
            case "TArray" :
                replaceInArray((TArray) f.to);
                break;
        }

        if (f.to.toString().equals(t1.toString())) {
            f.to = t2;
        }

        for (int i = 0; i < f.from.size(); i++) {

            switch (f.from.get(i).getClass().getSimpleName()) {
                case "TFun":
                    replaceInFun((TFun)f.from.get(i));
                    break;

                case "TTuple":
                    replaceInTuple((TTuple)f.from.get(i));
                    break;

                case "TArray":
                    replaceInArray((TArray)f.from.get(i));
                    break;
            }
            if (f.from.get(i).toString().equals(t1.toString())) {
                f.from.set(i, t2);
            }
        }

        return false;

    }

    public boolean replaceInTuple(TTuple tup) {
        
        

        for (int i = 0; i < tup.tlist.size(); i++) {
            
            switch(tup.tlist.get(i).getClass().getSimpleName())
            {
                case "TFun" :
                    replaceInFun((TFun) tup.tlist.get(i));
                    break;
                    
                case "TTuple" :
                    replaceInTuple((TTuple) tup.tlist.get(i));
                    break;
                    
                case "TArray" :
                    replaceInArray((TArray) tup.tlist.get(i));
                    break;
                    
            }
            
            if (tup.tlist.get(i).toString().equals(t1.toString())) {
                tup.tlist.set(i, t2);
            }

        }

        return false;

    }

    public boolean replaceInArray(TArray tar) {
        if (tar.t.toString().equals(t1.toString())) {
            tar.t = t2;
        }

        switch (tar.t.getClass().getSimpleName()) {
            case "TFun":
                replaceInFun((TFun) tar.t);
                break;

            case "TTuple":
                replaceInTuple((TTuple) tar.t);
                break;

            case "TArray":
                replaceInArray((TArray) tar.t);
                break;
        }

        return false;
    }

}
