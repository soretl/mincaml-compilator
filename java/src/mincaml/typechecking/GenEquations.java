package mincaml.typechecking;

import java.util.ArrayList;
import java.util.HashMap;
import mincaml.tree.*;
import mincaml.type.TArray;
import mincaml.type.TBool;
import mincaml.type.TFloat;
import mincaml.type.TFun;
import mincaml.type.TInt;
import mincaml.type.TTuple;
import mincaml.type.TUnit;
import mincaml.type.TVar;
import mincaml.type.Type;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author rotivala
 */
public class GenEquations {

    private static ArrayList listeEq = new ArrayList<Equations>();  // The list of Equations
    private static ArrayList<String> varSeen = new ArrayList<>();   // Useful for redeclarations

    /**
     * Having a static "start" function makes it far easier for the final
     * implementation.
     *
     * @param e : the "root" of the tree, the point where we start our analysis.
     */
    public static void start(Exp e) {
        TypeVisitor tv = new TypeVisitor();
        e.accept(tv);

        //Pretty self-explanatory
        GenEquations(tv.env, e, new TUnit());
        SolveEquations(listeEq, tv.env);



    }

    /*
    This is it, the hardest part of Typechecking : the actual generation of type equations.
    Though it is far from perfect, it actually works well !
     */
    private static ArrayList<Equations> GenEquations(HashMap<String, Type> env, Exp e, Type t) {

        TypeVisitor tv = new TypeVisitor(env);
        Type t1;                                //Only used to stock types for posterior use
        HashMap envFun;                         //Alternative environment used to allow redeclaration
        String newkey;
        
        boolean newDec = false;

        switch (e.getClass().getSimpleName()) {
            case "Unit":
                listeEq.add(new Equations(new TUnit(), t));
                break;

            case "Int":
                listeEq.add(new Equations(new TInt(), t));
                break;

            case "Float":
                listeEq.add(new Equations(new TFloat(), t));
                break;

            case "Bool":
                listeEq.add(new Equations(new TBool(), t));
                break;

            case "Not":
                GenEquations(env, ((Not) e).e, new TBool());
                listeEq.add(new Equations(new TBool(), t));
                break;

            case "Neg":
                GenEquations(env, ((Neg) e).e, new TInt());
                listeEq.add(new Equations(new TInt(), t));
                break;

            case "Add":

                GenEquations(env, ((Add) e).e1, new TInt());
                GenEquations(env, ((Add) e).e2, new TInt());
                listeEq.add(new Equations(new TInt(), t));
                break;

            case "Sub":

                GenEquations(env, ((Sub) e).e1, new TInt());
                GenEquations(env, ((Sub) e).e2, new TInt());
                listeEq.add(new Equations(new TInt(), t));
                break;

            case "Let":

                Let l = (Let) e;
                String id = l.id.toString();
                envFun = new HashMap<String, Type>();
                envFun.putAll(env);


                if (!varSeen.contains(l.id.toString()) && !env.get(l.id.toString()).getClass().getSimpleName().equals("TFun")) {
                    varSeen.add(l.id.toString());

                    t1 = env.get(l.id.toString());
                } else {

                    newDec= true;
                    t1 = Type.gen();
                    envFun.put((id), t1);
                }

                GenEquations(env, l.e1, t1);

                Type prov = env.get(l.id.toString());
                env.put(l.id.toString(), t1);

                GenEquations(envFun, l.e2, t);

                env.put(l.id.toString(), prov);
                
                if (newDec) {
                    newkey = id;
                    while(env.containsKey(newkey))
                    {
                        newkey = newkey.concat("'");
                    }
                    env.put(newkey, (Type) envFun.get(id));
                }

                break;

            case "Var":
                t1 = e.accept(tv);
                if (!t1.getClass().getSimpleName().equals("TFun")) {
                    listeEq.add(new Equations(t1, t));
                }

                break;

            case "If":
                If iff = (If) e;
                GenEquations(env, iff.e1, new TBool());
                GenEquations(env, iff.e2, t);
                GenEquations(env, iff.e3, t);
                break;

            case "Eq":
                GenEquations(env, ((Eq) e).e1, new TInt());
                GenEquations(env, ((Eq) e).e2, new TInt());
                break;

            case "LE":
                GenEquations(env, ((LE) e).e1, new TInt());
                GenEquations(env, ((LE) e).e2, new TInt());
                break;

            case "LetRec":
                LetRec lr = (LetRec) e;
                String idlr = lr.fd.id.toString();

                //We create an identical env in which to check types for the body of the function
                envFun = new HashMap<String, Type>();
                envFun.putAll(env);
                

                //We check if we've already seen this before
                if (!varSeen.contains(idlr)) {
                    varSeen.add(idlr);
                } else {

                    envFun.put((idlr), lr.accept(tv));
                    newDec = true;

                }
                
                GenEquations(env, lr.e, t);

                GenEquations(envFun, lr.fd.e, ((TFun) env.get(idlr)).to);

                //This is more for debugging purpose, but useful enought that I chose to leave it here
                if (newDec) {
                    newkey = idlr;
                    while(env.containsKey(newkey))
                    {
                        newkey = newkey.concat("'");
                    }
                    env.put(newkey, (Type) envFun.get(idlr));
                }


                break;

            case "FAdd":
                GenEquations(env, ((FAdd) e).e1, new TFloat());
                GenEquations(env, ((FAdd) e).e2, new TFloat());
                listeEq.add(new Equations(new TFloat(), t));
                break;

            case "FSub":
                GenEquations(env, ((FSub) e).e1, new TFloat());
                GenEquations(env, ((FSub) e).e2, new TFloat());
                listeEq.add(new Equations(new TFloat(), t));

                break;

            case "FMul":
                GenEquations(env, ((FMul) e).e1, new TFloat());
                GenEquations(env, ((FMul) e).e2, new TFloat());
                listeEq.add(new Equations(new TFloat(), t));

                break;

            case "FDiv":
                GenEquations(env, ((FDiv) e).e1, new TFloat());
                GenEquations(env, ((FDiv) e).e2, new TFloat());
                listeEq.add(new Equations(new TFloat(), t));
                break;

            case "FNeg":
                GenEquations(env, ((FNeg) e).e, new TFloat());
                listeEq.add(new Equations(new TFloat(), t));
                break;

            case "Tuple":
                ArrayList tt = new ArrayList<Type>();
                for (Exp expression : ((Tuple) e).es) {
                    tv = new TypeVisitor(env);
                    Type tprime = expression.accept(tv);
                    GenEquations(env, expression, tprime);
                    tt.add(tprime);
                }
                listeEq.add(new Equations(new TTuple(tt), t));
                break;

            case "LetTuple":
                LetTuple lt = (LetTuple) e;
                
                Type tupleType;
                ArrayList<Type> tupleElt = new ArrayList();
                
                for(int i = 0; i < lt.ids.size(); i++)
                {
                    tupleElt.add(env.get(lt.ids.get(i).toString()));
                }
                
                tupleType = new TTuple(tupleElt);
                TypeVisitor tv2 = new TypeVisitor();
                listeEq.add(new Equations(tupleType, lt.e1.accept(tv2)));

                GenEquations(tv2.env, lt.e1, tupleType);
                GenEquations(env, lt.e2, t);

                for (int i = 0; i < lt.ids.size(); i++) {
                    newkey = lt.ids.get(i).toString();
                    if (tv2.env.containsKey(lt.ids.get(i).toString())) {
                        while (env.containsKey(newkey)) {
                            newkey = newkey.concat("'");
                        }
                        env.put(newkey, tv2.env.get(lt.ids.get(i).toString()));
                    }

                }

                break;

            /*
                This is the most complex case, or at least the one that needs the most code,
                simply because there are SO MANY ways to call a function.
             */
            case "App":
                try {
                    App app = (App) e;

                    ArrayList<Exp> listeArg = new ArrayList<>();
                    listeArg.addAll(app.es);

                    ArrayList<Type> supposedArg = new ArrayList<>();

                    Type to = app.e.accept(tv);

                    //This part allows us to get the theoretical list of arguments for a given function
                    if (app.e.getClass().getSimpleName().equals("Var")) {
                        
                        
                        supposedArg.addAll(((TFun) env.get(((Var) app.e).id.toString())).from);

                        if (to.getClass().getSimpleName().equals(((TFun) env.get(((Var) app.e).id.toString())).to)) {
                            listeEq.add(new Equations(to, ((TFun) env.get(((Var) app.e).id.toString())).to));
                        }
                        
                        
                    } else if (app.e.getClass().getSimpleName().equals("Get")) {
                        
                        
                        supposedArg.addAll(((TFun) ((TArray) env.get(((Var) ((Get) app.e).e1).id.toString())).t).from);

                        if (to.getClass().getSimpleName().equals(((TFun) ((TArray) env.get(((Var) ((Get) app.e).e1).id.toString())).t).to)) {
                            listeEq.add(new Equations(to, ((TFun) ((TArray) env.get(((Var) ((Get) app.e).e1).id.toString())).t).to));
                        }

                    } else {
                        supposedArg.addAll(((TFun) env.get(((App) app.e).toString())).from);

                        if (to.getClass().getSimpleName().equals(((TFun) env.get(((App) app.e).toString())).to)) {
                            listeEq.add(new Equations(to, ((TFun) env.get(((App) app.e).toString())).to));
                        }

                        TFun orfun = ((TFun) env.get(((Var) ((App) app.e).e).id.toString()));

                        for (int i = 0; i < listeArg.size(); i++) {

                            listeEq.add(new Equations(orfun.from.get(i), listeArg.get(i).accept(tv)));
                        }
                    }

                    int x = 0;

                    //Here we match every given argument to its theoretical counterpart
                    for (Exp expression : listeArg) {

                        tv = new TypeVisitor(env);
                        t1 = expression.accept(tv);

                        if (t1.getClass().getSimpleName().equals(supposedArg.get(x).getClass().getSimpleName())) {
                            listeEq.add(new Equations(t1, supposedArg.get(x)));
                        } else if (supposedArg.get(x).getClass().getSimpleName().equals("TVar")) {
                            listeEq.add(new Equations(t1, supposedArg.get(x)));
                        }

                        GenEquations(env, expression, supposedArg.get(x));

                        x++;

                    }

                    //And here we match the theoretical and given return types
                    if (app.e.getClass().getSimpleName().equals("Var")) {

                        listeEq.add(new Equations(((TFun) env.get(((Var) app.e).id.toString())).to, t));

                    } else if (app.e.getClass().getSimpleName().equals("Get")) {

                        listeEq.add(new Equations(((TFun) ((TArray) env.get(((Var) ((Get) app.e).e1).id.toString())).t).to, t));
                    } else {

                        listeEq.add(new Equations(((TFun) env.get(((App) app.e).toString())).to, t));
                    }

                } catch (IndexOutOfBoundsException indexException) {
                    /* This exception is only thrown if there are too many arguments
                    to a function. For example, if we try to call print_int with three
                    arguments, there has to be a problem.
                     */
                    throw new TypageIncorrectException("ERREUR - Typage incorrect (Nombre d'Arguments)");
                }
                break;

            case "Array":
                Array a = (Array) e;
                GenEquations(env, a.e1, new TInt());
                TArray AType = (TArray)a.accept(tv);
                GenEquations(env, a.e2, AType.t);
                listeEq.add(new Equations(t, AType));
                break;

            case "Get":
                GenEquations(env, ((Get) e).e2, new TInt());
                if (((Get) e).e1.getClass().getSimpleName().equals("Var")) {
                    listeEq.add(new Equations(t, ((TArray) env.get(((Var) ((Get) e).e1).id.toString())).t));
                }

                break;

            case "Put":
                Put p = (Put) e;
                GenEquations(env, p.e2, new TInt());
                GenEquations(env, p.e3, ((TArray) env.get(p.e1.toString())).t);
                listeEq.add(new Equations(t, new TUnit()));
                break;

            default:
                // This should NEVER EVER happen, because it means that the parser
                // let something through that hasn't been implemented.
                throw new TypageIncorrectException("ERREUR - Operation " + e.getClass().getSimpleName().toString() + " non supportée !");

        }

        return null;
    }

    /**
     * We do not use this function in the code for now. It proves very useful,
     * however, for debugging.
     *
     * @param ListeEq: the list of equations
     */
    private static void PrintEquations(ArrayList<Equations> ListeEq) {
        if (ListeEq.isEmpty()) {
            return;
        }
        System.out.println("");
        System.out.println("");
        System.out.println("Système d'équations du programme :");
        System.out.println("");
        int i = 0;
        for (Equations e : ListeEq) {
            System.out.println(i++ + " - " + e.t1.toString() + " -> " + e.t2.toString());
        }
    }

    /*
    I chose not to make the solver as a visitor, because it didn't seem to make
    much sense. I stuck to a simple "check every equation for a possible modification
    and if you do a complete run without changing anything then you're done".
     */
    /**
     * The solver itself
     *
     * @param ListeEq: The list of equations
     * @param env: The variable environment
     */
    private static void SolveEquations(ArrayList<Equations> ListeEq, HashMap<String, Type> env) {
        ArrayList<Equations> toRemove = new ArrayList<>();  //Since you can't remove from a list while you use it. The other solution would have been to use an iterator.
        ArrayList<Equations> toAdd = new ArrayList<>();
        boolean isSolved;                                   //To check if something has been changed this iteration

        do {
            isSolved = true;
            
            /*
            This is a preliminary check, intended to remove redundant equations
            (such as int => int or ?5 => ?5) and simplify
             */
            for (Equations e : ListeEq) {
                // Logically, if both sides of the equation are arrays, then it
                // means their elements are equivalent, so we remove the array
                // part to keep only the base type.
                if (e.t1.getClass().getSimpleName().equals("TArray") && e.t2.getClass().getSimpleName().equals("TArray")) {
                    e.t1 = ((TArray) e.t1).t;
                    e.t2 = ((TArray) e.t2).t;
                }

                //If similar, we remove the equation
                if (e.t1.toString().equals(e.t2.toString())) {
                    toRemove.add(e);
                }
                
                //Tuples are broken into their components, who are in turn added as equations
                if (e.t1.getClass().getSimpleName().equals("TTuple") && e.t2.getClass().getSimpleName().equals("TTuple") && ((TTuple) e.t1).tlist.size() == ((TTuple) e.t2).tlist.size()) {

                    for (int i = 0; i < ((TTuple) e.t1).tlist.size(); i++) {
                        if (!((TTuple) e.t1).tlist.get(i).toString().equals(((TTuple) e.t2).tlist.get(i).toString())) {
                            toAdd.add(new Equations(((TTuple) e.t1).tlist.get(i), ((TTuple) e.t2).tlist.get(i)));
                        }

                    }
                    toRemove.add(e);

                    isSolved = false;
                }

            }
            //Finally, we add all new equations and remove obsolete/redudants ones
            ListeEq.addAll(toAdd);
            ListeEq.removeAll(toRemove);

            for (Equations e : ListeEq) {
                /*
                From now on, we just list the ways there could be an equivalence
                hidden in an equation. Pretty simple in theory, but takes a lot
                of lines of code ...
                 */

                //If the right part is an unknown var, we switch them
                if (e.t2.toString().startsWith("?") && e.t2.getClass().getSimpleName().equals("TVar")) {

                    Type tprov = e.t2;
                    e.t2 = e.t1;
                    e.t1 = tprov;
                    EquationVisitor ev = new EquationVisitor(tprov, e.t1, env);
                    ev.visit(ListeEq);
                    isSolved = false;

                }

                //If the left part is, we replace it with the right part everywhere
                if (e.t1.toString().startsWith("?") && e.t1.getClass().getSimpleName().equals("TVar")) {

                    EquationVisitor ev = new EquationVisitor(e.t1, e.t2, env);
                    ev.visit(ListeEq);
                    isSolved = false;

                }

                

                /*
                Functions are a bit of a pain to manage, and so have earned their
                own 30 lines of code !
                 */
                if (e.t1.getClass().getSimpleName().equals("TFun") && e.t2.getClass().getSimpleName().equals("TFun")) {

                    //For every type in the "from" part
                    for (int i = 0; i < ((TFun) e.t1).from.size(); i++) {

                        Type tprime = ((TFun) e.t1).from.get(i);
                        Type tsec = ((TFun) e.t2).from.get(i);

                        if (tprime.getClass().getSimpleName().equals("TVar")) {
                            EquationVisitor ev = new EquationVisitor(tprime, tsec, env);
                            ev.visit(ListeEq);
                            isSolved = false;
                        }

                        if (tsec.getClass().getSimpleName().equals("TVar")) {
                            EquationVisitor ev = new EquationVisitor(tsec, tprime, env);
                            ev.visit(ListeEq);
                            isSolved = false;
                        }
                    }

                    if (((TFun) e.t1).to.getClass().getSimpleName().equals("TVar")) {
                        EquationVisitor ev = new EquationVisitor(((TFun) e.t1).to, ((TFun) e.t2).to, env);
                        ev.visit(ListeEq);
                        isSolved = false;
                    }

                    if (((TFun) e.t2).to.getClass().getSimpleName().equals("TVar")) {
                        EquationVisitor ev = new EquationVisitor(((TFun) e.t2).to, ((TFun) e.t1).to, env);
                        ev.visit(ListeEq);
                        isSolved = false;
                    }

                }

            }

            


        } while (!isSolved);
        
        /*
        IF and ONLY IF there is something left in the list of equations after we
        have solved everything that can be solved, it necessarily means that it's
        unsolvable, and thus that there is a conflict, a typing error.
         */

        if (!ListeEq.isEmpty()) {
            throw new TypageIncorrectException("ERREUR - Typage incorrect  (" + ((Equations) listeEq.get(0)).t1.toString() + " différent de " + ((Equations) listeEq.get(0)).t2.toString() + ") \n(" + (listeEq.size()-1) + " other type errors)\n\n");

        }
    }

}
