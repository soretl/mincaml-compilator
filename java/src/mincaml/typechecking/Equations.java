/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mincaml.typechecking;

import mincaml.type.Type;


/**
 * 
 * @author rotivala
 * Pretty simple class : every equation is a equivalence between two types.
 */
public class Equations {

    Type t1;
    Type t2;

    public Equations(Type t1, Type t2) {
        this.t1 = t1;
        this.t2 = t2;
    }

}
