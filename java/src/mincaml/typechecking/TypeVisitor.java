package mincaml.typechecking;

import java.util.ArrayList;
import java.util.HashMap;
import mincaml.tree.*;
import mincaml.type.*;
import mincaml.visitor.ObjVisitor;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * TypeVisitor 
 * @author rotivala
 * Pretty simple visitor. I technically COULD have done without,
 * but it allows me to pre-type most variables, which ends up being really
 * useful (functions, for instance, are dealt with more easily that way)
 */
public class TypeVisitor implements ObjVisitor<Type> {

    public HashMap<String, Type> env = new HashMap<>(); //The variable environment

    public TypeVisitor(HashMap<String, Type> env) {
        this.env = env;

        /*
         * Here I "declare" some built-in functions. I don't have to, since the
         * solver is pretty good at deducing their signature anyway, but it's
         * safer that way.
         */
        ArrayList prov = new ArrayList();
        prov.add(new TInt());
        env.put("print_int", new TFun(prov, new TUnit()));

        prov = new ArrayList();
        prov.add(new TUnit());
        env.put("exit", new TFun(prov, new TUnit()));

        prov = new ArrayList();
        prov.add(new TUnit());
        env.put("print_newline", new TFun(prov, new TUnit()));
        
        prov = new ArrayList();
        prov.add(new TFloat());
        env.put("truncate", new TFun(prov, new TInt()));
    }

    public TypeVisitor() {
        this.env = new HashMap<>();

        //same as above
        ArrayList prov = new ArrayList();
        prov.add(new TInt());
        env.put("print_int", new TFun(prov, new TUnit()));

        prov = new ArrayList();
        prov.add(new TUnit());
        env.put("exit", new TFun(prov, new TUnit()));

        prov = new ArrayList();
        prov.add(new TUnit());
        env.put("print_newline", new TFun(prov, new TUnit()));
        
        prov = new ArrayList();
        prov.add(new TFloat());
        env.put("truncate", new TFun(prov, new TInt()));
    }

    @Override
    public Type visit(Unit e) {
        return new TUnit();
    }

    @Override
    public Type visit(Bool e) {
        return new TBool();
    }

    @Override
    public Type visit(Int e) {
        return new TInt();
    }

    @Override
    public Type visit(mincaml.tree.Float e) {
        return new TFloat();
    }

    @Override
    public Type visit(Not e) {

        return e.e.accept(this);
    }

    @Override
    public Type visit(Neg e) {

        return e.e.accept(this);
    }

    @Override
    public Type visit(Add e) {
        Type t1 = e.e1.accept(this);
        e.e2.accept(this);
        return t1;
    }

    @Override
    public Type visit(Sub e) {
        Type t1 = e.e1.accept(this);
        e.e2.accept(this);
        return t1;
    }

    @Override
    public Type visit(FNeg e) {
        return e.e.accept(this);
    }

    @Override
    public Type visit(FAdd e) {
        Type t1 = e.e1.accept(this);
        e.e2.accept(this);
        return t1;
    }

    @Override
    public Type visit(FSub e) {
        Type t1 = e.e1.accept(this);
        e.e2.accept(this);
        return t1;
    }

    @Override
    public Type visit(FMul e) {
        Type t1 = e.e1.accept(this);
        e.e2.accept(this);
        return t1;
    }

    @Override
    public Type visit(FDiv e) {
        Type t1 = e.e1.accept(this);
        e.e2.accept(this);
        return t1;
    }

    @Override
    public Type visit(Eq e) {
        return new TBool();
    }

    @Override
    public Type visit(LE e) {
        return new TBool();
    }

    @Override
    public Type visit(If e) {
        e.e1.accept(this);
        e.e2.accept(this);
        Type t3 = e.e3.accept(this);

        return t3;

    }

    @Override
    public Type visit(Let e) {

        if (!env.containsKey(e.id.toString())) {        //We only generate a new type if the variable hasn't been encountered before
            Type t = Type.gen();
            env.put(e.id.toString(), t);
        }
        e.e1.accept(this);                              //Even though we don't use the return type, visiting every part helps build the env.

        return e.e2.accept(this);
    }

    @Override
    public Type visit(Var e) {
        Type t;

        if (!env.containsKey(e.id.toString())) {

            t = Type.gen();
            env.put(e.id.toString(), t);

        } else {

            t = env.get(e.id.id);
        }

        return t;
    }

    @Override
    public Type visit(LetRec e) {

        Type to = e.fd.e.accept(this);

        while (e.fd.e.getClass().getSimpleName().equals("TFun")) {
            to = ((TFun) e.fd.e.accept(this)).to;
        }

        ArrayList<Type> typesFrom = new ArrayList<>();
        for (Id i : e.fd.args) {
            if (!env.containsKey(i.id)) {

                Type t = Type.gen();
                env.put(i.id, t);
            }

            typesFrom.add(env.get(i.id));

        }

        Type t = new TFun(typesFrom, to);
        env.put(e.fd.id.id, t);
        return e.e.accept(this);
    }

    @Override
    public Type visit(App e) {

        int x = 0;
        Type t = e.e.accept(this);
        ArrayList<Type> tt = new ArrayList<>();

        while (t.getClass().getSimpleName().equals("TFun")) {
            t = ((TFun) t).to;
        }

        for (Exp expression : e.es) {
            Type t1 = expression.accept(this);
        }

        /*
        Since we have to cast, we also have to check the type before casting.
         */
        switch (e.e.getClass().getSimpleName()) {
            case "Var":
                for (Exp expression : e.es) {
                    Type t1 = expression.accept(this);
                    if (t1.getClass().getSimpleName().equals("TFun")) {
                        t1 = ((TFun) t1).to;
                    }

                    tt.add(t1);

                }

                if (!env.containsKey(((Var) e.e).id.toString()) || env.get(((Var) e.e).id.toString()).getClass().getSimpleName().equals("TVar")) {

                    t = Type.gen();
                    TFun tfun = new TFun(tt, t);
                    env.put(((Var) e.e).id.toString(), tfun);
                }

                break;

            case "App":
                for (Exp expression : e.es) {
                    Type t1 = expression.accept(this);
                    if (t1.getClass().getSimpleName().equals("TFun")) {
                        t1 = ((TFun) t1).to;
                    }
                    tt.add(t1);
                }

                if (!env.containsKey(((App) e.e).toString()) || env.get(((App) e.e).toString()).getClass().getSimpleName().equals("TVar")) {

                    t = Type.gen();
                    TFun tfun = new TFun(tt, t);
                    env.put(((App) e.e).toString(), tfun);

                }
                break;

            case "Get":
                for (Exp expression : e.es) {
                    Type t1 = expression.accept(this);
                    if (t1.getClass().getSimpleName().equals("TFun")) {
                        t1 = ((TFun) t1).to;
                    }
                    tt.add(t1);
                }

                if (!env.containsKey(((Var) ((Get) e.e).e1).id.toString()) || env.get(((Var) ((Get) e.e).e1).id.toString()).getClass().getSimpleName().equals("TArray")) {
                    t = Type.gen();
                    TFun tfun = new TFun(tt, t);
                    TArray ta = new TArray(tfun);
                    env.put(((Var) ((Get) e.e).e1).id.toString(), ta);

                }
                break;

        }

        return t;
    }

    @Override
    public Type visit(Tuple e) {
        ArrayList tt = new ArrayList<Type>();
        for (Exp expression : e.es) {
            tt.add(expression.accept(this));

        }

        return new TTuple(tt);
    }

    @Override
    public Type visit(LetTuple e) {
        for (Id i : e.ids) {
            if (!env.containsKey(i.toString())) {
                Type t = Type.gen();
                env.put(i.toString(), t);
            }
        }
        e.e1.accept(this);
        return e.e2.accept(this);
    }

    @Override
    public Type visit(Array e) {

        Type t = e.e2.accept(this);
        return new TArray(t);
    }

    @Override
    public Type visit(Get e) {
        Type t = e.e1.accept(this);

        if (e.e1.getClass().getSimpleName().equals("Var")) {

            t = env.get(((Var) e.e1).id.toString());
            if (!t.getClass().getSimpleName().equals("TArray")) {
                env.put(((Var) e.e1).id.toString(), new TArray(Type.gen()));
            } else {
                t = ((TArray) t).t;
            }
        } else if (e.e1.getClass().getSimpleName().equals("Array")) {
            t = ((TArray) t).t;
        }

        return t;
    }

    @Override
    public Type visit(Put e) {
        if (e.e1.getClass().getSimpleName().equals("Var")) {
            if (!env.containsKey(((Var) e.e1).id.toString())) {
                env.put(((Var) e.e1).id.toString(), new TArray(Type.gen()));
            }
            if (!env.get(((Var) e.e1).id.toString()).equals("TArray")) {
                env.put(((Var) e.e1).id.toString(), new TArray(Type.gen()));
            }

        } else {
            env.put(e.e1.toString(), new TArray(Type.gen()));
        }

        return new TUnit();
    }

    /*
    These just have to be implemented, but they won't ever be used.
     */
    @Override
    public Type visit(Apply_direct e) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Type visit(Apply_closure e) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Type visit(Make_closure e) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
