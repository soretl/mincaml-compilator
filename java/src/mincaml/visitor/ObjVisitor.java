package mincaml.visitor;

import mincaml.tree.Add;
import mincaml.tree.App;
import mincaml.tree.Apply_closure;
import mincaml.tree.Apply_direct;
import mincaml.tree.Array;
import mincaml.tree.Bool;
import mincaml.tree.Eq;
import mincaml.tree.FAdd;
import mincaml.tree.FDiv;
import mincaml.tree.FMul;
import mincaml.tree.FNeg;
import mincaml.tree.FSub;
import mincaml.tree.Float;
import mincaml.tree.Get;
import mincaml.tree.If;
import mincaml.tree.Int;
import mincaml.tree.LE;
import mincaml.tree.Let;
import mincaml.tree.LetRec;
import mincaml.tree.LetTuple;
import mincaml.tree.Make_closure;
import mincaml.tree.Neg;
import mincaml.tree.Not;
import mincaml.tree.Put;
import mincaml.tree.Sub;
import mincaml.tree.Tuple;
import mincaml.tree.Unit;
import mincaml.tree.Var;

public interface ObjVisitor<E> {
	public E visit(Unit e);
	public E visit(Bool e);
	public E visit(Int e);
	public E visit(Float e);
	public E visit(Not e);
	public E visit(Neg e);
	public E visit(Add e);
	public E visit(Sub e);
	public E visit(FNeg e);
	public  E visit(FAdd e);
	public E visit(FSub e);
	public E visit(FMul e);
	public  E visit(FDiv e);
	public  E visit(Eq e);
	public  E visit(LE e);
	public E visit(If e);
	public  E visit(Let e);
	public  E visit(Var e);
	public  E visit(LetRec e);
	public  E visit(App e);
	public  E visit(Tuple e);
	public  E visit(LetTuple e);
	public  E visit(Array e);
	public   E visit(Get e);
	public   E visit(Put e);
        public E visit(Apply_direct e);
        public E visit(Apply_closure e);
        public E visit(Make_closure e);
}


