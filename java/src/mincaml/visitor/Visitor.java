package mincaml.visitor;

import java.util.*;

import mincaml.tree.Add;
import mincaml.tree.App;
import mincaml.tree.Apply_closure;
import mincaml.tree.Apply_direct;
import mincaml.tree.Array;
import mincaml.tree.Bool;
import mincaml.tree.Eq;
import mincaml.tree.FAdd;
import mincaml.tree.FDiv;
import mincaml.tree.FMul;
import mincaml.tree.FNeg;
import mincaml.tree.FSub;
import mincaml.tree.Float;
import mincaml.tree.Get;
import mincaml.tree.If;
import mincaml.tree.Int;
import mincaml.tree.LE;
import mincaml.tree.Let;
import mincaml.tree.LetRec;
import mincaml.tree.LetTuple;
import mincaml.tree.Make_closure;
import mincaml.tree.Neg;
import mincaml.tree.Not;
import mincaml.tree.Put;
import mincaml.tree.Sub;
import mincaml.tree.Tuple;
import mincaml.tree.Unit;
import mincaml.tree.Var;

public interface Visitor {

	public void visit(Unit e);
	public void visit(Bool e);
	public void visit(Int e);
	public void visit(Float e);
	public void visit(Not e);
	public void visit(Neg e);
	public void visit(Add e);
	public void visit(Sub e);
	public void visit(FNeg e);
	public void visit(FAdd e);
	public void visit(FSub e);
	public void visit(FMul e);
	public void visit(FDiv e);
	public void visit(Eq e);
	public void visit(LE e);
	public void visit(If e);
	public void visit(Let e);
	public void visit(Var e);
	public void visit(LetRec e);
	public void visit(App e);
	public void visit(Tuple e);
	public void visit(LetTuple e);
	public void visit(Array e);
	public void visit(Get e);
	public void visit(Put e);
        public void visit(Apply_direct e);
        public void visit(Apply_closure e);
        public void visit(Make_closure e);
}


