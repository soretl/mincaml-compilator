package mincaml.visitor;

import java.util.*;

import mincaml.tree.Add;
import mincaml.tree.App;
import mincaml.tree.Apply_closure;
import mincaml.tree.Apply_direct;
import mincaml.tree.Array;
import mincaml.tree.Bool;
import mincaml.tree.Eq;
import mincaml.tree.Exp;
import mincaml.tree.FAdd;
import mincaml.tree.FDiv;
import mincaml.tree.FMul;
import mincaml.tree.FNeg;
import mincaml.tree.FSub;
import mincaml.tree.Float;
import mincaml.tree.Get;
import mincaml.tree.Id;
import mincaml.tree.If;
import mincaml.tree.Int;
import mincaml.tree.LE;
import mincaml.tree.Let;
import mincaml.tree.LetRec;
import mincaml.tree.LetTuple;
import mincaml.tree.Make_closure;
import mincaml.tree.Neg;
import mincaml.tree.Not;
import mincaml.tree.Put;
import mincaml.tree.Sub;
import mincaml.tree.Tuple;
import mincaml.tree.Unit;
import mincaml.tree.Var;

public class VarVisitor implements ObjVisitor<Set<String>> {

    public Set<String> visit(Unit e) {
        return new HashSet<String>();
    }

    public Set<String> visit(Bool e) {
        return new HashSet<String>();
    }

    public Set<String> visit(Int e) {
        return new HashSet<String>();
    }

    public Set<String> visit(Float e) { 
        return new HashSet<String>();
    }

    public Set<String> visit(Not e) {
        Set<String> fv = e.e.accept(this);
        return e.e.accept(this);
    }

    public Set<String> visit(Neg e) {
        Set<String> fv = e.e.accept(this);
        return fv;
    }

    public Set<String> visit(Add e) {
        Set<String> fv1 = e.e1.accept(this);
        Set<String> fv2 = e.e2.accept(this);
        fv1.addAll(fv2);
        return fv1;
    }

    public Set<String> visit(Sub e) {
        Set<String> fv1 = e.e1.accept(this);
        Set<String> fv2 = e.e2.accept(this);
        fv1.addAll(fv2);
        return fv1;
    }

    public Set<String> visit(FNeg e){
        Set<String> fv = e.e.accept(this);
        return fv;
    }

    public Set<String> visit(FAdd e){
        Set<String> fv1 = e.e1.accept(this);
        Set<String> fv2 = e.e2.accept(this);
        fv1.addAll(fv2);
        return fv1;
    }

    public Set<String> visit(FSub e){
        Set<String> fv1 = e.e1.accept(this);
        Set<String> fv2 = e.e2.accept(this);
        fv1.addAll(fv2);
        return fv1;
    }

    public Set<String> visit(FMul e) {
        Set<String> fv1 = e.e1.accept(this);
        Set<String> fv2 = e.e2.accept(this);
        fv1.addAll(fv2);
        return fv1;
    }

    public Set<String> visit(FDiv e){
        Set<String> fv1 = e.e1.accept(this);
        Set<String> fv2 = e.e2.accept(this);
        fv1.addAll(fv2);
        return fv1;
    }

    public Set<String> visit(Eq e){
        Set<String> fv1 = e.e1.accept(this);
        Set<String> fv2 = e.e2.accept(this);
        fv1.addAll(fv2);
        return fv1;
    }

    public Set<String> visit(LE e){
        Set<String> fv1 = e.e1.accept(this);
        Set<String> fv2 = e.e2.accept(this);
        fv1.addAll(fv2);
        return fv1;
    }

    public Set<String> visit(If e){
        Set<String> fv1 = e.e1.accept(this);
        Set<String> fv2 = e.e2.accept(this);
        Set<String> fv3 = e.e3.accept(this);
        fv1.addAll(fv2);
        fv1.addAll(fv3);
        return fv1;
    }

    public Set<String> visit(Let e) {
        Set<String> res = new HashSet<String>();
        Set<String> fv1 = e.e1.accept(this);
        Set<String> fv2 = e.e2.accept(this);
        fv2.remove(e.id.toString());
        res.addAll(fv1);
        res.addAll(fv2);

        return res;
    }

    public Set<String> visit(Var e){
        Set<String> res = new HashSet<String>();
        res.add(e.id.toString());
        return res;
    }

    public Set<String> visit(LetRec e){
        Set<String> res = new HashSet<String>();
        Set<String> fv = e.e.accept(this);
        Set<String> fv_fun = e.fd.e.accept(this);
        
        
        
        
        
        for (Id id : e.fd.args) {
            fv_fun.remove(id.toString());
        }
        
        
        fv.remove(e.fd.id.toString());
        fv_fun.remove(e.fd.id.toString());
        res.addAll(fv);
        res.addAll(fv_fun);

        
        
        return res;
    }

    public Set<String> visit(App e){
        Set<String> res = new HashSet<String>();
        res.addAll(e.e.accept(this));
        for (Exp exp : e.es) {
            res.addAll(exp.accept(this));
        }
        return res;
    }

    public Set<String> visit(Tuple e){
        Set<String> res = new HashSet<String>();
        for (Exp exp : e.es) {
            res.addAll(exp.accept(this));
        }
        return res;
    }

    public Set<String> visit(LetTuple e){
        Set<String> res = new HashSet<String>();
        Set<String> fv1 = e.e1.accept(this);
        Set<String> fv2 = e.e2.accept(this);
        for (Id id : e.ids) {
            fv2.remove(id.toString());
        }
        res.addAll(fv1);
        res.addAll(fv2);
        return res;
    }

    public Set<String> visit(Array e){
        Set<String> fv1 = e.e1.accept(this);
        Set<String> fv2 = e.e2.accept(this);
        fv1.addAll(fv2);
        return fv1;
    }

    public Set<String> visit(Get e){
        Set<String> fv1 = e.e1.accept(this);
        Set<String> fv2 = e.e2.accept(this);
        fv1.addAll(fv2);
        return fv1;
    }

    public Set<String> visit(Put e){
        Set<String> fv1 = e.e1.accept(this);
        Set<String> fv2 = e.e2.accept(this);
        Set<String> fv3 = e.e3.accept(this);
        fv1.addAll(fv2);
        fv1.addAll(fv3);
        return fv1;
    }
    
    public Set<String> visit(Apply_direct e){
        Set<String> res = new HashSet<String>();
        for (Exp exp : e.es) {
            res.addAll(exp.accept(this));
        }
        return res;
    }
    
    public Set<String> visit(Apply_closure e){
        Set<String> res = new HashSet<String>();
        for (Exp exp : e.es) {
            res.addAll(exp.accept(this));
        }
        return res;
    }
    
    public Set<String> visit(Make_closure e){
        Set<String> res = new HashSet<String>();
        for (Exp exp : e.fv) {
            res.addAll(exp.accept(this));
        }
        return res;
    }
}


