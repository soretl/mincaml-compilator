package asml.tree;

import java.util.LinkedList;
import java.util.List;

import asml.tree.ident_or_imm.Ident_or_Arm;
import asml.visitor.AsmlObjVisitor;
import asml.visitor.AsmlVisitor;

// Arguments d'une fonction
public class Formal_arg implements Noeud{
	public List<Ident_or_Arm> args;

	public Formal_arg() {
		this.args = new LinkedList<Ident_or_Arm> ();
	}

	public void add_front(Ident_or_Arm id){
		args.add(0,id);
	}
	
	public void add_end(Ident_or_Arm id){
		args.add(id);
	}

	public void accept(AsmlVisitor v) {
		v.visit(this);
	}

	public <E> E accept(AsmlObjVisitor<E> v) {
		return v.visit(this);
	}
}
