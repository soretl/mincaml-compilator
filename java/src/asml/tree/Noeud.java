package asml.tree;

import asml.visitor.AsmlObjVisitor;
import asml.visitor.AsmlVisitor;

public interface Noeud {
    public void accept(AsmlVisitor v);
    public <E> E accept(AsmlObjVisitor<E> v);
}
