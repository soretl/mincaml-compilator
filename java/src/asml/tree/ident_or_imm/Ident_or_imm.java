package asml.tree.ident_or_imm;

import asml.visitor.AsmlObjVisitor;
import asml.visitor.AsmlVisitor;

// Nom de variable ou valeur immediate
public interface Ident_or_imm {
	public void accept(AsmlVisitor v);
	public <E> E accept(AsmlObjVisitor<E> v);
	public String toString();
}