package asml.tree.ident_or_imm;

import asml.tree.Noeud;
import asml.tree.exp.Exp;
import asml.visitor.AsmlObjVisitor;
import asml.visitor.AsmlVisitor;
import asml.visitor.ObjArgVisitor;

public class StackOffset extends Exp implements Ident_or_Arm,Ident_or_imm,Noeud{

	public int offset;
	public boolean aPush; //besoin d'incrementer le sp
	
	public StackOffset(int offset,boolean aPush) {
		this.offset=offset;
		this.aPush = aPush;
	}
	
	public void accept(AsmlVisitor v) {
		v.visit(this);
	}

	public <E> E accept(AsmlObjVisitor<E> v) {
		return v.visit(this);
	}

	@Override
	public String toString(){
		return "[fp,#"+offset+"]";
	}
	
	@Override
	public <E, A> E accept(ObjArgVisitor<E, A> v, A arg) {
		return v.visit(this, arg);
	}

	@Override
	public boolean equals(Object obj){
		return (obj instanceof StackOffset) && this.offset == ((StackOffset) obj).offset;
	}

	@Override
	public int hashCode(){
		return this.offset;
	}

	@Override
	public Ident_or_Arm clone() {
		return new StackOffset(offset,aPush);
	}

}
