package asml.tree.ident_or_imm;

import asml.visitor.AsmlObjVisitor;
import asml.visitor.AsmlVisitor;
import asml.visitor.ObjArgVisitor;

public interface Ident_or_Arm {

	public void accept(AsmlVisitor v);

	public <E> E accept(AsmlObjVisitor<E> v);
	public <E, A> E accept(ObjArgVisitor<E, A> v, A arg);
	public boolean equals(Object obj);
	public String toString();
	public int hashCode();
	public Ident_or_Arm clone();
}
