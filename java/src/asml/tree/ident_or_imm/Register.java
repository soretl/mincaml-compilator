package asml.tree.ident_or_imm;

import asml.tree.Noeud;
import asml.tree.exp.Exp;
import asml.visitor.AsmlObjVisitor;
import asml.visitor.AsmlVisitor;
import asml.visitor.ObjArgVisitor;

public class Register extends Exp implements Ident_or_Arm,Ident_or_imm,Noeud {

	private int numero;
	
	public Register(int r) {
		this.numero = r;
	}

	public int getNumero(){
		return numero;
	}

	public void accept(AsmlVisitor v) {
		v.visit(this);
	}

	public <E> E accept(AsmlObjVisitor<E> v) {
		return v.visit(this);
	}

	@Override
	public <E, A> E accept(ObjArgVisitor<E, A> v, A arg) {
		return v.visit(this, arg);
	}

	@Override
	public String toString(){
		return "r"+numero;
	}

	@Override
	public boolean equals(Object obj){
		return (obj instanceof Register) && this.numero == ((Register) obj).numero;
	}

	@Override
	public int hashCode(){
		return this.numero;
	}

	@Override
	public Ident_or_Arm clone() {
		return new Register(numero);
	}

}
