package asml.tree.ident_or_imm;

import asml.tree.Id;
import asml.tree.Noeud;
import asml.tree.exp.Exp;
import asml.visitor.AsmlObjVisitor;
import asml.visitor.AsmlVisitor;
import asml.visitor.ObjArgVisitor;

// Nom de variable
public class Ident extends Exp implements Ident_or_imm, Noeud, Ident_or_Arm {
	public Id id;

	public Ident(Id id) {
		this.id = id;
	}
	
	public void accept(AsmlVisitor v) {
		v.visit(this);
	}

	public <E> E accept(AsmlObjVisitor<E> v) {
		return v.visit(this);
	}

	@Override
	public String toString(){
		return id.toString();
	}
	
	@Override
	public <E, A> E accept(ObjArgVisitor<E, A> v, A arg) {
		return v.visit(this, arg);
	}

	@Override
	public boolean equals(Object obj){
		return (obj instanceof Ident) && this.id.equals(((Ident) obj).id);
	}

	@Override
	public int hashCode(){
		return this.id.hashCode();
	}

	@Override
	public Ident_or_Arm clone() {
		return new Ident(id);
	}
}