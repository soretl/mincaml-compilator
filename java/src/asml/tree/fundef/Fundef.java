package asml.tree.fundef;

import asml.tree.Noeud;
import asml.visitor.AsmlObjVisitor;
import asml.visitor.AsmlVisitor;

/*
fundefs:
	| LET UNDERSC EQUAL asmt
	| LET LABEL EQUAL FLOAT fundefs
	| LET LABEL formal_args EQUAL asmt fundefs
*/
public abstract class Fundef implements Noeud {
	public abstract void accept(AsmlVisitor v);

	public abstract <E> E accept(AsmlObjVisitor<E> v);
}
