package asml.tree.fundef;

import asml.tree.Noeud;
import asml.tree.asmt.Asmt;
import asml.visitor.AsmlObjVisitor;
import asml.visitor.AsmlVisitor;

// LET UNDERSC EQUAL asmt
public class Main extends Fundef implements Noeud {
	public Asmt asmt;

	public Main(Asmt a) {
		this.asmt = a;
	}

	public void accept(AsmlVisitor v) { v.visit(this); }

	public <E> E accept(AsmlObjVisitor<E> v) {	return v.visit(this); }
}