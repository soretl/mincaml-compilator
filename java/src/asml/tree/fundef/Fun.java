package asml.tree.fundef;

import asml.tree.Formal_arg;
import asml.tree.Noeud;
import asml.tree.asmt.Asmt;
import asml.tree.exp.Label;
import asml.visitor.AsmlObjVisitor;
import asml.visitor.AsmlVisitor;

// LET LABEL formal_args EQUAL asmt fundefs
public class Fun extends Fundef implements Noeud {

	public Label label;
	public Formal_arg args;
	public Asmt functionCore;
	public Fundef next;

	public Fun(	Label label, Formal_arg args, Asmt functionCore, Fundef next){
		this.label=label;
		this.args=args;
		this.functionCore=functionCore;
		this.next=next;
	}

	public void accept(AsmlVisitor v) { v.visit(this); }

	public <E> E accept(AsmlObjVisitor<E> v) {	return v.visit(this); }
}