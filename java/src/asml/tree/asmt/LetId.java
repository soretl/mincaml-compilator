package asml.tree.asmt;

import asml.tree.Noeud;
import asml.tree.exp.Exp;
import asml.tree.ident_or_imm.Ident_or_Arm;
import asml.visitor.AsmlObjVisitor;
import asml.visitor.AsmlVisitor;
import asml.visitor.ObjArgVisitor;

// Let Ident = exp in asmt
public class LetId extends Asmt implements Noeud {
	public Ident_or_Arm id;
	public Exp exp;
	public Asmt next;
	
	public LetId(Ident_or_Arm i, Exp e, Asmt n) {
		this.id = i;
		this.exp = e;
		this.next = n;
	}

	public void setNext(Asmt next) {
		this.next = next;
	}

	public void accept(AsmlVisitor v) {
		v.visit(this);
	}

	public <E> E accept(AsmlObjVisitor<E> v) {
		return v.visit(this);
	}
	
	@Override
	public <E, A> E accept(ObjArgVisitor<E, A> v, A arg) {
		return v.visit(this, arg);
	}
}