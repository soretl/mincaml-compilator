package asml.tree.asmt;

import asml.tree.Noeud;
import asml.tree.exp.Exp;
import asml.visitor.AsmlObjVisitor;
import asml.visitor.AsmlVisitor;
import asml.visitor.ObjArgVisitor;

// exp
public class FinAsmt extends Asmt implements Noeud {
	public Exp exp;
	
	public FinAsmt(Exp e) {
		this.exp = e;
	}
	
	public void accept(AsmlVisitor v) {
		v.visit(this);
	}

	public <E> E accept(AsmlObjVisitor<E> v) {
		return v.visit(this);
	}

	@Override
	public <E, A> E accept(ObjArgVisitor<E, A> v, A arg) {
		return v.visit(this, arg);
	}
}