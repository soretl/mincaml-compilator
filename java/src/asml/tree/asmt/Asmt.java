package asml.tree.asmt;

import asml.tree.Noeud;
import asml.visitor.AsmlObjVisitor;
import asml.visitor.AsmlVisitor;
import asml.visitor.ObjArgVisitor;

/**
 * asmt:
 * | LET IDENT EQUAL exp IN asmt
 * | exp
 */
public abstract class Asmt implements Noeud {
    public abstract void accept(AsmlVisitor v);
    public abstract <E> E accept(AsmlObjVisitor<E> v);
    public abstract <E,A> E accept(ObjArgVisitor<E, A> v, A arg);
}