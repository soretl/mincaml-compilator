package asml.tree;

import mincaml.tree.Bool;

public class Id {
    String id;
    Boolean isUnused;
    Boolean isSelf;
    static int unused = -1;

    public Id(String id) {
        this.id = id;
        this.isUnused = false;
        this.isSelf = false;
    }

    public Id(String id, Boolean u, Boolean s){
        this.id = id;
        this.isUnused = u;
        this.isSelf = s;
    }


    public String getId(){
        return id;
    }

    public Boolean isUnused(){
        return this.isUnused;
    }

    public Boolean isSelf() {return this.isSelf;}

    @Override
    public String toString() {
        return id;
    }

    @Override
    public boolean equals(Object obj){
        return (obj instanceof Id) && (this.id.equals(((Id) obj).id));
    }

    @Override
    public int hashCode(){
        return this.id.hashCode();
    }

    static int standard = -1;

    public static Id genStandard() {
        standard++;
        return new Id("w" + standard);
    }

    public static Id genStandard(String s) {
        return new Id(s);
    }

    public static Id genLabel(String s) {
        return new Id(s);
    }

    public static Id genUnused() {
        unused++;
        return new Id("u"+unused,true,false);
    }

    public static Id genUnused(String s) {
        return new Id(s,true,false);
    }

    public static  Id genSelf() {
        return  new Id("%self",false,true);

    }

}
