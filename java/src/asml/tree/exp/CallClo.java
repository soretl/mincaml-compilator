package asml.tree.exp;

import asml.tree.Formal_arg;
import asml.tree.Noeud;
import asml.tree.ident_or_imm.Ident_or_Arm;
import asml.visitor.AsmlObjVisitor;
import asml.visitor.AsmlVisitor;
import asml.visitor.ObjArgVisitor;

// CALLCLO LABEL formal_args
public class CallClo extends Exp implements Noeud {
	public Ident_or_Arm ident;
	public Formal_arg args;

	public CallClo(Ident_or_Arm ident, Formal_arg args) {
		this.ident = ident;
		this.args = args;
	}

	public void accept(AsmlVisitor v) {
		v.visit(this);
	}

	public <E> E accept(AsmlObjVisitor<E> v) {
		return v.visit(this);
	}
	
	@Override
	public <E, A> E accept(ObjArgVisitor<E, A> v, A arg) {
		return v.visit(this, arg);
	}
}