package asml.tree.exp;

import asml.tree.Noeud;
import asml.tree.ident_or_imm.Ident_or_Arm;
import asml.tree.ident_or_imm.Ident_or_imm;
import asml.visitor.AsmlObjVisitor;
import asml.visitor.AsmlVisitor;
import asml.visitor.ObjArgVisitor;

// SUB INDENT INDENT
public class Sub extends Exp implements Noeud {
	public Ident_or_Arm left;
	public Ident_or_imm right;

	public Sub (Ident_or_Arm left, Ident_or_imm right){
		this.left = left;
		this.right = right;
	}

	@Override
	public void accept(AsmlVisitor v) {v.visit(this);}

	@Override
	public <E> E accept(AsmlObjVisitor<E> v) { return v.visit(this); }
	
	@Override
	public <E, A> E accept(ObjArgVisitor<E, A> v, A arg) {
		return v.visit(this, arg);
	}
}