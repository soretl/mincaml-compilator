package asml.tree.exp;

import asml.tree.Noeud;
import asml.tree.ident_or_imm.Ident_or_Arm;
import asml.visitor.AsmlObjVisitor;
import asml.visitor.AsmlVisitor;
import asml.visitor.ObjArgVisitor;

// NEG INDENT
public class Neg extends Exp implements Noeud {
	public Ident_or_Arm ident;

	public Neg(Ident_or_Arm ident){
		this.ident=ident;
	}

	@Override
	public void accept(AsmlVisitor v) {v.visit(this);}

	@Override
	public <E> E accept(AsmlObjVisitor<E> v) { return v.visit(this); }
	
	@Override
	public <E, A> E accept(ObjArgVisitor<E, A> v, A arg) {
		return v.visit(this, arg);
	}
}