package asml.tree.exp;

public enum Condition {
	EQ,
	LE,
	GE,
	FEQ,
	FLE;

	@Override
	public String toString() {
		switch (this){
			case EQ:
				return "=";
			case LE:
				return "<=";
			case GE:
				return ">=";
			case FEQ:
				return "=.";
			case FLE:
				return "<=.";
			default:
				System.err.println("[Erreur interne] Condition inconnue.");
				return "UNDEFINED";
		}
	}
}