package asml.tree.exp;

import asml.tree.Noeud;
import asml.tree.asmt.Asmt;
import asml.tree.ident_or_imm.Ident_or_Arm;
import asml.tree.ident_or_imm.Ident_or_imm;
import asml.visitor.AsmlObjVisitor;
import asml.visitor.AsmlVisitor;
import asml.visitor.ObjArgVisitor;

/*
 IF IDENT EQUAL ident_or_imm THEN asmt ELSE asmt
| IF IDENT LE ident_or_imm THEN asmt ELSE asmt
| IF IDENT GE ident_or_imm THEN asmt ELSE asmt
| IF IDENT FEQUAL IDENT THEN asmt ELSE asmt
| IF IDENT FLE IDENT THEN asmt ELSE asmt
 */
public class If extends Exp implements Noeud {
	public Condition condition;
	public Ident_or_Arm condLeft;
	public Ident_or_imm condRight;
	public Asmt nextThen;
	public Asmt nextElse;

	public If(Condition condition, Ident_or_Arm condLeft, Ident_or_imm condRight, Asmt nextThen, Asmt nextElse){
		this.condition=condition;
		this.condLeft=condLeft;
		this.condRight=condRight;
		this.nextThen=nextThen;
		this.nextElse=nextElse;
	}

	@Override
	public void accept(AsmlVisitor v) {
		v.visit(this);
	}

	@Override
	public <E> E accept(AsmlObjVisitor<E> v) {
		return v.visit(this);
	}
	
	@Override
	public <E, A> E accept(ObjArgVisitor<E, A> v, A arg) {
		return v.visit(this, arg);
	}

}