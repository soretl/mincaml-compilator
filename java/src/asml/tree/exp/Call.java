package asml.tree.exp;

import asml.tree.Formal_arg;
import asml.tree.Noeud;
import asml.visitor.AsmlObjVisitor;
import asml.visitor.AsmlVisitor;
import asml.visitor.ObjArgVisitor;

// CALL LABEL formal_args
public class Call extends Exp implements Noeud {
	public Label l;
	public Formal_arg args;

	public Call(Label l, Formal_arg args) {
		this.l = l;
		this.args = args;
	}
	
	public void accept(AsmlVisitor v) {
		v.visit(this);
	}
	

	public <E> E accept(AsmlObjVisitor<E> v) {
		return v.visit(this);
	}
	
	@Override
	public <E, A> E accept(ObjArgVisitor<E, A> v, A arg) {
		return v.visit(this, arg);
	}
}