package asml.tree.exp;
import asml.tree.Noeud;
import asml.tree.ident_or_imm.Ident_or_imm;
import asml.visitor.AsmlObjVisitor;
import asml.visitor.AsmlVisitor;
import asml.visitor.ObjArgVisitor;

// Entier naturel
public class Int extends Exp implements Ident_or_imm, Noeud {
	public final int i;

	public Int(int i){
		this.i = i;
	}

	@Override
	public void accept(AsmlVisitor v) {
		v.visit(this);
	}

	@Override
	public <E> E accept(AsmlObjVisitor<E> v) {
		return v.visit(this);
	}

	@Override
	public String toString(){
		return Integer.toString(i);
	}

	@Override
	public <E, A> E accept(ObjArgVisitor<E, A> v, A arg) {
		return v.visit(this, arg);
	}

}