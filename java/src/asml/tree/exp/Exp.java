package asml.tree.exp;

import asml.tree.Noeud;
import asml.visitor.AsmlObjVisitor;
import asml.visitor.AsmlVisitor;
import asml.visitor.ObjArgVisitor;

/*
exp: 
| INT
| IDENT
| LABEL
| NEG IDENT
| ADD IDENT ident_or_imm
| SUB IDENT ident_or_imm
| MEM LPAREN IDENT PLUS ident_or_imm RPAREN
| MEM LPAREN IDENT PLUS ident_or_imm RPAREN ASSIGN IDENT
| IF IDENT EQUAL ident_or_imm THEN asmt ELSE asmt
| IF IDENT LE ident_or_imm THEN asmt ELSE asmt
| IF IDENT GE ident_or_imm THEN asmt ELSE asmt
| IF IDENT FEQUAL IDENT THEN asmt ELSE asmt
| IF IDENT FLE IDENT THEN asmt ELSE asmt
| CALL LABEL formal_args 
 */
public abstract class Exp implements Noeud {
    public abstract void accept(AsmlVisitor v);
    public abstract <E> E accept(AsmlObjVisitor<E> v);
    public abstract <E,A> E accept(ObjArgVisitor<E, A> v, A arg);
}