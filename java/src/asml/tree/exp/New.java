package asml.tree.exp;

import asml.tree.Noeud;
import asml.tree.ident_or_imm.Ident_or_imm;
import asml.visitor.AsmlObjVisitor;
import asml.visitor.AsmlVisitor;
import asml.visitor.ObjArgVisitor;

public class New extends Exp implements Noeud {
    public Ident_or_imm ident;

    public New(Ident_or_imm id){
        this.ident = id;
    }

    @Override
    public void accept(AsmlVisitor v) {
        v.visit(this);
    }

    @Override
    public <E> E accept(AsmlObjVisitor<E> v) {
        return v.visit(this);
    }

    @Override
    public <E, A> E accept(ObjArgVisitor<E, A> v, A arg) {
        return v.visit(this,arg);
    }
}
