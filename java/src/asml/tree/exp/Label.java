package asml.tree.exp;

import asml.tree.Id;
import asml.tree.Noeud;
import asml.visitor.AsmlObjVisitor;
import asml.visitor.AsmlVisitor;
import asml.visitor.ObjArgVisitor;

// Nom de fonction
public class Label extends Exp implements Noeud {
	public Id id;

	public Label(Id id) {
		this.id = id;
	}
	
	@Override
	public void accept(AsmlVisitor v) {
		v.visit(this);
	}

	@Override
	public <E> E accept(AsmlObjVisitor<E> v) {
		return v.visit(this);
	}
	
	@Override
	public <E, A> E accept(ObjArgVisitor<E, A> v, A arg) {
		return v.visit(this, arg);
	}
}