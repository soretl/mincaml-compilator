package asml.tree.exp;

import asml.tree.Noeud;
import asml.tree.ident_or_imm.Ident;
import asml.tree.ident_or_imm.Ident_or_Arm;
import asml.tree.ident_or_imm.Ident_or_imm;
import asml.visitor.AsmlObjVisitor;
import asml.visitor.AsmlVisitor;
import asml.visitor.ObjArgVisitor;

// mem(IDENT + ident_or_imm) <- IDENT
public class MemStore extends Exp implements Noeud {
	public Ident_or_Arm address;
	public Ident_or_imm offset;
	public Ident_or_Arm source;

	public MemStore(Ident_or_Arm address, Ident_or_imm offset, Ident_or_Arm source) {
		this.address = address;
		this.offset = offset;
		this.source = source;
	}

	@Override
	public void accept(AsmlVisitor v) {
		v.visit(this);
	}

	@Override
	public <E> E accept(AsmlObjVisitor<E> v) {
		return v.visit(this);
	}
	
	@Override
	public <E, A> E accept(ObjArgVisitor<E, A> v, A arg) {
		return v.visit(this, arg);
	}
}