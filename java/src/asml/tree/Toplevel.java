package asml.tree;

import asml.tree.fundef.Fundef;
import asml.visitor.AsmlObjVisitor;
import asml.visitor.AsmlVisitor;

public class Toplevel implements Noeud{
	public Fundef f;
	public Toplevel(Fundef f){
		this.f = f;
	}
	public void accept(AsmlVisitor v) { v.visit(this); }
	public <E> E accept(AsmlObjVisitor<E> v) {	return v.visit(this); }
}
