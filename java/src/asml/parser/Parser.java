package asml.parser;


import asml.tree.*;
import asml.tree.asmt.*;
import asml.tree.exp.*;
import asml.tree.fundef.*;
import asml.tree.ident_or_imm.*;
//----------------------------------------------------
// The following code was generated by CUP v0.11b 20140808 (SVN rev 54)
//----------------------------------------------------

import java_cup.runtime.*;
import java.util.*;
import java_cup.runtime.XMLElement;

/** CUP v0.11b 20140808 (SVN rev 54) generated parser.
  */
@SuppressWarnings({"rawtypes"})
public class Parser extends java_cup.runtime.lr_parser {

 public final Class getSymbolContainer() {
    return sym.class;
}

  /** Default constructor. */
  public Parser() {super();}

  /** Constructor which sets the default scanner. */
  public Parser(java_cup.runtime.Scanner s) {super(s);}

  /** Constructor which sets the default scanner. */
  public Parser(java_cup.runtime.Scanner s, java_cup.runtime.SymbolFactory sf) {super(s,sf);}

  /** Production table. */
  protected static final short _production_table[][] = 
    unpackFromStrings(new String[] {
    "\000\034\000\002\002\004\000\002\002\004\000\002\002" +
    "\003\000\002\002\003\000\002\006\003\000\002\006\003" +
    "\000\002\004\005\000\002\004\003\000\002\004\005\000" +
    "\002\004\005\000\002\004\003\000\002\004\003\000\002" +
    "\004\005\000\002\004\005\000\002\004\004\000\002\004" +
    "\012\000\002\004\012\000\002\004\012\000\002\004\010" +
    "\000\002\004\012\000\002\004\004\000\002\004\003\000" +
    "\002\005\005\000\002\005\010\000\002\005\003\000\002" +
    "\003\006\000\002\003\010\000\002\007\003" });

  /** Access to production table. */
  public short[][] production_table() {return _production_table;}

  /** Parse-action table. */
  protected static final short[][] _action_table = 
    unpackFromStrings(new String[] {
    "\000\120\000\004\025\004\001\002\000\006\010\011\046" +
    "\010\001\002\000\004\002\007\001\002\000\004\002\uffe6" +
    "\001\002\000\004\002\001\001\002\000\004\015\121\001" +
    "\002\000\006\007\013\011\014\001\002\000\004\015\016" +
    "\001\002\000\022\002\uffff\007\013\011\014\013\uffff\015" +
    "\uffff\024\uffff\025\uffff\026\uffff\001\002\000\016\002\ufffe" +
    "\013\ufffe\015\ufffe\024\ufffe\025\ufffe\026\ufffe\001\002\000" +
    "\016\002\000\013\000\015\000\024\000\025\000\026\000" +
    "\001\002\000\036\005\017\007\020\010\024\012\035\022" +
    "\026\025\033\030\032\032\023\040\025\041\027\042\034" +
    "\043\021\044\036\045\030\001\002\000\014\002\ufffa\013" +
    "\ufffa\024\ufffa\025\ufffa\026\ufffa\001\002\000\014\002\ufff7" +
    "\013\ufff7\024\ufff7\025\ufff7\026\ufff7\001\002\000\006\005" +
    "\063\007\061\001\002\000\014\002\uffe9\013\uffe9\024\uffe9" +
    "\025\uffe9\026\uffe9\001\002\000\004\012\111\001\002\000" +
    "\014\002\ufff6\013\ufff6\024\ufff6\025\ufff6\026\ufff6\001\002" +
    "\000\004\007\107\001\002\000\004\007\064\001\002\000" +
    "\004\007\060\001\002\000\004\007\056\001\002\000\004" +
    "\025\004\001\002\000\004\007\054\001\002\000\004\007" +
    "\045\001\002\000\004\010\043\001\002\000\036\005\017" +
    "\007\020\010\024\012\035\022\026\025\033\030\032\032" +
    "\023\040\025\041\027\042\034\043\021\044\036\045\030" +
    "\001\002\000\014\002\uffec\013\uffec\024\uffec\025\uffec\026" +
    "\uffec\001\002\000\004\013\042\001\002\000\004\013\041" +
    "\001\002\000\014\002\uffeb\013\uffeb\024\uffeb\025\uffeb\026" +
    "\uffeb\001\002\000\014\002\ufffb\013\ufffb\024\ufffb\025\ufffb" +
    "\026\ufffb\001\002\000\006\007\013\011\014\001\002\000" +
    "\014\002\ufff9\013\ufff9\024\ufff9\025\ufff9\026\ufff9\001\002" +
    "\000\004\015\046\001\002\000\034\005\017\007\020\010" +
    "\024\012\050\022\026\030\032\032\023\040\025\041\027" +
    "\042\034\043\021\044\036\045\030\001\002\000\004\026" +
    "\052\001\002\000\034\005\017\007\020\010\024\012\050" +
    "\022\026\030\032\032\023\040\025\041\027\042\034\043" +
    "\021\044\036\045\030\001\002\000\004\013\042\001\002" +
    "\000\036\005\017\007\020\010\024\012\035\022\026\025" +
    "\033\030\032\032\023\040\025\041\027\042\034\043\021" +
    "\044\036\045\030\001\002\000\014\002\uffea\013\uffea\024" +
    "\uffea\025\uffea\026\uffea\001\002\000\014\002\ufff3\013\ufff3" +
    "\024\ufff3\025\ufff3\026\ufff3\001\002\000\004\002\uffe7\001" +
    "\002\000\006\007\013\011\014\001\002\000\014\002\ufff8" +
    "\013\ufff8\024\ufff8\025\ufff8\026\ufff8\001\002\000\006\005" +
    "\063\007\061\001\002\000\016\002\ufffc\013\ufffc\023\ufffc" +
    "\024\ufffc\025\ufffc\026\ufffc\001\002\000\014\002\ufff4\013" +
    "\ufff4\024\ufff4\025\ufff4\026\ufff4\001\002\000\016\002\ufffd" +
    "\013\ufffd\023\ufffd\024\ufffd\025\ufffd\026\ufffd\001\002\000" +
    "\010\015\066\017\065\021\067\001\002\000\006\005\063" +
    "\007\061\001\002\000\006\005\063\007\061\001\002\000" +
    "\006\005\063\007\061\001\002\000\004\023\071\001\002" +
    "\000\036\005\017\007\020\010\024\012\035\022\026\025" +
    "\033\030\032\032\023\040\025\041\027\042\034\043\021" +
    "\044\036\045\030\001\002\000\004\024\073\001\002\000" +
    "\036\005\017\007\020\010\024\012\035\022\026\025\033" +
    "\030\032\032\023\040\025\041\027\042\034\043\021\044" +
    "\036\045\030\001\002\000\014\002\ufff0\013\ufff0\024\ufff0" +
    "\025\ufff0\026\ufff0\001\002\000\004\023\076\001\002\000" +
    "\036\005\017\007\020\010\024\012\035\022\026\025\033" +
    "\030\032\032\023\040\025\041\027\042\034\043\021\044" +
    "\036\045\030\001\002\000\004\024\100\001\002\000\036" +
    "\005\017\007\020\010\024\012\035\022\026\025\033\030" +
    "\032\032\023\040\025\041\027\042\034\043\021\044\036" +
    "\045\030\001\002\000\014\002\ufff2\013\ufff2\024\ufff2\025" +
    "\ufff2\026\ufff2\001\002\000\004\023\103\001\002\000\036" +
    "\005\017\007\020\010\024\012\035\022\026\025\033\030" +
    "\032\032\023\040\025\041\027\042\034\043\021\044\036" +
    "\045\030\001\002\000\004\024\105\001\002\000\036\005" +
    "\017\007\020\010\024\012\035\022\026\025\033\030\032" +
    "\032\023\040\025\041\027\042\034\043\021\044\036\045" +
    "\030\001\002\000\014\002\ufff1\013\ufff1\024\ufff1\025\ufff1" +
    "\026\ufff1\001\002\000\006\005\063\007\061\001\002\000" +
    "\014\002\ufff5\013\ufff5\024\ufff5\025\ufff5\026\ufff5\001\002" +
    "\000\004\007\112\001\002\000\004\014\113\001\002\000" +
    "\006\005\063\007\061\001\002\000\004\013\115\001\002" +
    "\000\016\002\uffef\013\uffef\024\uffef\025\uffef\026\uffef\037" +
    "\116\001\002\000\004\007\117\001\002\000\014\002\uffee" +
    "\013\uffee\024\uffee\025\uffee\026\uffee\001\002\000\014\002" +
    "\uffed\013\uffed\024\uffed\025\uffed\026\uffed\001\002\000\036" +
    "\005\017\007\020\010\024\012\035\022\026\025\033\030" +
    "\032\032\023\040\025\041\027\042\034\043\021\044\036" +
    "\045\030\001\002\000\004\002\uffe8\001\002" });

  /** Access to parse-action table. */
  public short[][] action_table() {return _action_table;}

  /** <code>reduce_goto</code> table. */
  protected static final short[][] _reduce_table = 
    unpackFromStrings(new String[] {
    "\000\120\000\006\003\005\007\004\001\001\000\002\001" +
    "\001\000\002\001\001\000\002\001\001\000\002\001\001" +
    "\000\002\001\001\000\004\002\011\001\001\000\002\001" +
    "\001\000\004\002\014\001\001\000\002\001\001\000\002" +
    "\001\001\000\006\004\021\005\030\001\001\000\002\001" +
    "\001\000\002\001\001\000\004\006\117\001\001\000\002" +
    "\001\001\000\002\001\001\000\002\001\001\000\002\001" +
    "\001\000\002\001\001\000\002\001\001\000\002\001\001" +
    "\000\004\003\054\001\001\000\002\001\001\000\002\001" +
    "\001\000\002\001\001\000\006\004\036\005\037\001\001" +
    "\000\002\001\001\000\002\001\001\000\002\001\001\000" +
    "\002\001\001\000\002\001\001\000\004\002\043\001\001" +
    "\000\002\001\001\000\002\001\001\000\004\004\046\001" +
    "\001\000\002\001\001\000\004\004\050\001\001\000\002" +
    "\001\001\000\006\004\021\005\052\001\001\000\002\001" +
    "\001\000\002\001\001\000\002\001\001\000\004\002\056" +
    "\001\001\000\002\001\001\000\004\006\061\001\001\000" +
    "\002\001\001\000\002\001\001\000\002\001\001\000\002" +
    "\001\001\000\004\006\101\001\001\000\004\006\074\001" +
    "\001\000\004\006\067\001\001\000\002\001\001\000\006" +
    "\004\021\005\071\001\001\000\002\001\001\000\006\004" +
    "\021\005\073\001\001\000\002\001\001\000\002\001\001" +
    "\000\006\004\021\005\076\001\001\000\002\001\001\000" +
    "\006\004\021\005\100\001\001\000\002\001\001\000\002" +
    "\001\001\000\006\004\021\005\103\001\001\000\002\001" +
    "\001\000\006\004\021\005\105\001\001\000\002\001\001" +
    "\000\004\006\107\001\001\000\002\001\001\000\002\001" +
    "\001\000\002\001\001\000\004\006\113\001\001\000\002" +
    "\001\001\000\002\001\001\000\002\001\001\000\002\001" +
    "\001\000\002\001\001\000\006\004\021\005\121\001\001" +
    "\000\002\001\001" });

  /** Access to <code>reduce_goto</code> table. */
  public short[][] reduce_table() {return _reduce_table;}

  /** Instance of action encapsulation class. */
  protected CUP$Parser$actions action_obj;

  /** Action encapsulation object initializer. */
  protected void init_actions()
    {
      action_obj = new CUP$Parser$actions(this);
    }

  /** Invoke a user supplied parse action. */
  public java_cup.runtime.Symbol do_action(
    int                        act_num,
    java_cup.runtime.lr_parser parser,
    java.util.Stack            stack,
    int                        top)
    throws java.lang.Exception
  {
    /* call code in generated class */
    return action_obj.CUP$Parser$do_action(act_num, parser, stack, top);
  }

  /** Indicates start state. */
  public int start_state() {return 0;}
  /** Indicates start production. */
  public int start_production() {return 0;}

  /** <code>EOF</code> Symbol index. */
  public int EOF_sym() {return 0;}

  /** <code>error</code> Symbol index. */
  public int error_sym() {return 1;}


  /** User initialization code. */
  public void user_init() throws java.lang.Exception
    {
  
    }

  /** Scan to get the next Symbol. */
  public java_cup.runtime.Symbol scan()
    throws java.lang.Exception
    {
 return l.next_token(); 
    }


    Lexer l;
    Parser(Lexer l){
	   this.l = l;
    }

    class Pair<A,B> {
        A left;
        B right;

        Pair(A left, B right) {
            this.left = left;
            this.right = right;
        }
    }

    

/** Cup generated class to encapsulate user supplied action code.*/
@SuppressWarnings({"rawtypes", "unchecked", "unused"})
class CUP$Parser$actions {
  private final Parser parser;

  /** Constructor */
  CUP$Parser$actions(Parser parser) {
    this.parser = parser;
  }

  /** Method 0 with the actual generated action code for actions 0 to 300. */
  public final java_cup.runtime.Symbol CUP$Parser$do_action_part00000000(
    int                        CUP$Parser$act_num,
    java_cup.runtime.lr_parser CUP$Parser$parser,
    java.util.Stack            CUP$Parser$stack,
    int                        CUP$Parser$top)
    throws java.lang.Exception
    {
      /* Symbol object for return from actions */
      java_cup.runtime.Symbol CUP$Parser$result;

      /* select the action based on the action number */
      switch (CUP$Parser$act_num)
        {
          /*. . . . . . . . . . . . . . . . . . . .*/
          case 0: // $START ::= toplevel EOF 
            {
              Object RESULT =null;
		int start_valleft = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)).left;
		int start_valright = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)).right;
		Toplevel start_val = (Toplevel)((java_cup.runtime.Symbol) CUP$Parser$stack.elementAt(CUP$Parser$top-1)).value;
		RESULT = start_val;
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("$START",0, ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          /* ACCEPT */
          CUP$Parser$parser.done_parsing();
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 1: // formal_args ::= IDENT formal_args 
            {
              Formal_arg RESULT =null;
		int idleft = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)).left;
		int idright = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)).right;
		Ident id = (Ident)((java_cup.runtime.Symbol) CUP$Parser$stack.elementAt(CUP$Parser$top-1)).value;
		int argsleft = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).left;
		int argsright = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).right;
		Formal_arg args = (Formal_arg)((java_cup.runtime.Symbol) CUP$Parser$stack.peek()).value;
		 args.add_front(id); RESULT = args; 
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("formal_args",0, ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 2: // formal_args ::= IDENT 
            {
              Formal_arg RESULT =null;
		int idleft = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).left;
		int idright = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).right;
		Ident id = (Ident)((java_cup.runtime.Symbol) CUP$Parser$stack.peek()).value;
		 Formal_arg l = new Formal_arg(); 
      l.add_front(id); 
      RESULT = l; 
   
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("formal_args",0, ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 3: // formal_args ::= NIL 
            {
              Formal_arg RESULT =null;
		 RESULT = new Formal_arg(); 
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("formal_args",0, ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 4: // ident_or_imm ::= INT 
            {
              Ident_or_imm RESULT =null;
		int ileft = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).left;
		int iright = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).right;
		int i = (int)((java_cup.runtime.Symbol) CUP$Parser$stack.peek()).value;
		 RESULT = new Int(i); 
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("ident_or_imm",4, ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 5: // ident_or_imm ::= IDENT 
            {
              Ident_or_imm RESULT =null;
		int idleft = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).left;
		int idright = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).right;
		Ident id = (Ident)((java_cup.runtime.Symbol) CUP$Parser$stack.peek()).value;
		 RESULT = id; 
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("ident_or_imm",4, ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 6: // exp ::= LPAREN exp RPAREN 
            {
              Exp RESULT =null;
		int eleft = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)).left;
		int eright = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)).right;
		Exp e = (Exp)((java_cup.runtime.Symbol) CUP$Parser$stack.elementAt(CUP$Parser$top-1)).value;
		 RESULT = e; 
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("exp",2, ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-2)), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 7: // exp ::= INT 
            {
              Exp RESULT =null;
		int ileft = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).left;
		int iright = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).right;
		int i = (int)((java_cup.runtime.Symbol) CUP$Parser$stack.peek()).value;
		 RESULT = new Int(i); 
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("exp",2, ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 8: // exp ::= CALL LABEL formal_args 
            {
              Exp RESULT =null;
		int idleft = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)).left;
		int idright = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)).right;
		Label id = (Label)((java_cup.runtime.Symbol) CUP$Parser$stack.elementAt(CUP$Parser$top-1)).value;
		int fleft = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).left;
		int fright = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).right;
		Formal_arg f = (Formal_arg)((java_cup.runtime.Symbol) CUP$Parser$stack.peek()).value;
		 RESULT = new Call(id, f); 
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("exp",2, ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-2)), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 9: // exp ::= APPCLO IDENT formal_args 
            {
              Exp RESULT =null;
		int idleft = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)).left;
		int idright = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)).right;
		Ident id = (Ident)((java_cup.runtime.Symbol) CUP$Parser$stack.elementAt(CUP$Parser$top-1)).value;
		int fleft = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).left;
		int fright = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).right;
		Formal_arg f = (Formal_arg)((java_cup.runtime.Symbol) CUP$Parser$stack.peek()).value;
		 RESULT = new CallClo(id,f); 
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("exp",2, ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-2)), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 10: // exp ::= IDENT 
            {
              Exp RESULT =null;
		int idleft = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).left;
		int idright = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).right;
		Ident id = (Ident)((java_cup.runtime.Symbol) CUP$Parser$stack.peek()).value;
		 RESULT = id; 
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("exp",2, ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 11: // exp ::= LABEL 
            {
              Exp RESULT =null;
		int lleft = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).left;
		int lright = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).right;
		Label l = (Label)((java_cup.runtime.Symbol) CUP$Parser$stack.peek()).value;
		 RESULT = l; 
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("exp",2, ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 12: // exp ::= ADD IDENT ident_or_imm 
            {
              Exp RESULT =null;
		int idleft = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)).left;
		int idright = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)).right;
		Ident id = (Ident)((java_cup.runtime.Symbol) CUP$Parser$stack.elementAt(CUP$Parser$top-1)).value;
		int iileft = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).left;
		int iiright = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).right;
		Ident_or_imm ii = (Ident_or_imm)((java_cup.runtime.Symbol) CUP$Parser$stack.peek()).value;
		 RESULT = new Add(id,ii); 
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("exp",2, ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-2)), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 13: // exp ::= SUB IDENT ident_or_imm 
            {
              Exp RESULT =null;
		int idleft = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)).left;
		int idright = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)).right;
		Ident id = (Ident)((java_cup.runtime.Symbol) CUP$Parser$stack.elementAt(CUP$Parser$top-1)).value;
		int iileft = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).left;
		int iiright = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).right;
		Ident_or_imm ii = (Ident_or_imm)((java_cup.runtime.Symbol) CUP$Parser$stack.peek()).value;
		 RESULT = new Sub(id,ii); 
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("exp",2, ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-2)), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 14: // exp ::= NEG IDENT 
            {
              Exp RESULT =null;
		int idleft = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).left;
		int idright = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).right;
		Ident id = (Ident)((java_cup.runtime.Symbol) CUP$Parser$stack.peek()).value;
		 RESULT = new Neg(id); 
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("exp",2, ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 15: // exp ::= IF IDENT EQUAL ident_or_imm THEN asmt ELSE asmt 
            {
              Exp RESULT =null;
		int idleft = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-6)).left;
		int idright = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-6)).right;
		Ident id = (Ident)((java_cup.runtime.Symbol) CUP$Parser$stack.elementAt(CUP$Parser$top-6)).value;
		int iileft = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-4)).left;
		int iiright = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-4)).right;
		Ident_or_imm ii = (Ident_or_imm)((java_cup.runtime.Symbol) CUP$Parser$stack.elementAt(CUP$Parser$top-4)).value;
		int a1left = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-2)).left;
		int a1right = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-2)).right;
		Asmt a1 = (Asmt)((java_cup.runtime.Symbol) CUP$Parser$stack.elementAt(CUP$Parser$top-2)).value;
		int a2left = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).left;
		int a2right = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).right;
		Asmt a2 = (Asmt)((java_cup.runtime.Symbol) CUP$Parser$stack.peek()).value;
		 RESULT = new If(Condition.EQ,id,ii,a1,a2); 
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("exp",2, ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-7)), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 16: // exp ::= IF IDENT LE ident_or_imm THEN asmt ELSE asmt 
            {
              Exp RESULT =null;
		int idleft = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-6)).left;
		int idright = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-6)).right;
		Ident id = (Ident)((java_cup.runtime.Symbol) CUP$Parser$stack.elementAt(CUP$Parser$top-6)).value;
		int iileft = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-4)).left;
		int iiright = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-4)).right;
		Ident_or_imm ii = (Ident_or_imm)((java_cup.runtime.Symbol) CUP$Parser$stack.elementAt(CUP$Parser$top-4)).value;
		int a1left = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-2)).left;
		int a1right = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-2)).right;
		Asmt a1 = (Asmt)((java_cup.runtime.Symbol) CUP$Parser$stack.elementAt(CUP$Parser$top-2)).value;
		int a2left = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).left;
		int a2right = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).right;
		Asmt a2 = (Asmt)((java_cup.runtime.Symbol) CUP$Parser$stack.peek()).value;
		 RESULT = new If(Condition.LE,id,ii,a1,a2); 
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("exp",2, ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-7)), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 17: // exp ::= IF IDENT GE ident_or_imm THEN asmt ELSE asmt 
            {
              Exp RESULT =null;
		int idleft = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-6)).left;
		int idright = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-6)).right;
		Ident id = (Ident)((java_cup.runtime.Symbol) CUP$Parser$stack.elementAt(CUP$Parser$top-6)).value;
		int iileft = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-4)).left;
		int iiright = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-4)).right;
		Ident_or_imm ii = (Ident_or_imm)((java_cup.runtime.Symbol) CUP$Parser$stack.elementAt(CUP$Parser$top-4)).value;
		int a1left = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-2)).left;
		int a1right = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-2)).right;
		Asmt a1 = (Asmt)((java_cup.runtime.Symbol) CUP$Parser$stack.elementAt(CUP$Parser$top-2)).value;
		int a2left = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).left;
		int a2right = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).right;
		Asmt a2 = (Asmt)((java_cup.runtime.Symbol) CUP$Parser$stack.peek()).value;
		 RESULT = new If(Condition.GE,id,ii,a1,a2); 
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("exp",2, ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-7)), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 18: // exp ::= MEM LPAREN IDENT PLUS ident_or_imm RPAREN 
            {
              Exp RESULT =null;
		int idleft = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-3)).left;
		int idright = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-3)).right;
		Ident id = (Ident)((java_cup.runtime.Symbol) CUP$Parser$stack.elementAt(CUP$Parser$top-3)).value;
		int iileft = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)).left;
		int iiright = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)).right;
		Ident_or_imm ii = (Ident_or_imm)((java_cup.runtime.Symbol) CUP$Parser$stack.elementAt(CUP$Parser$top-1)).value;
		 RESULT = new  MemLoad(id,ii); 
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("exp",2, ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-5)), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 19: // exp ::= MEM LPAREN IDENT PLUS ident_or_imm RPAREN ASSIGN IDENT 
            {
              Exp RESULT =null;
		int idleft = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-5)).left;
		int idright = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-5)).right;
		Ident id = (Ident)((java_cup.runtime.Symbol) CUP$Parser$stack.elementAt(CUP$Parser$top-5)).value;
		int iileft = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-3)).left;
		int iiright = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-3)).right;
		Ident_or_imm ii = (Ident_or_imm)((java_cup.runtime.Symbol) CUP$Parser$stack.elementAt(CUP$Parser$top-3)).value;
		int id2left = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).left;
		int id2right = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).right;
		Ident id2 = (Ident)((java_cup.runtime.Symbol) CUP$Parser$stack.peek()).value;
		 RESULT = new MemStore(id,ii,id2); 
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("exp",2, ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-7)), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 20: // exp ::= NEW ident_or_imm 
            {
              Exp RESULT =null;
		int iileft = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).left;
		int iiright = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).right;
		Ident_or_imm ii = (Ident_or_imm)((java_cup.runtime.Symbol) CUP$Parser$stack.peek()).value;
		 RESULT = new New(ii); 
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("exp",2, ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 21: // exp ::= NOP 
            {
              Exp RESULT =null;
		 RESULT = new Nop(); 
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("exp",2, ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 22: // asmt ::= LPAREN asmt RPAREN 
            {
              Asmt RESULT =null;
		int aleft = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)).left;
		int aright = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)).right;
		Asmt a = (Asmt)((java_cup.runtime.Symbol) CUP$Parser$stack.elementAt(CUP$Parser$top-1)).value;
		 RESULT = a; 
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("asmt",3, ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-2)), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 23: // asmt ::= LET IDENT EQUAL exp IN asmt 
            {
              Asmt RESULT =null;
		int idleft = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-4)).left;
		int idright = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-4)).right;
		Ident id = (Ident)((java_cup.runtime.Symbol) CUP$Parser$stack.elementAt(CUP$Parser$top-4)).value;
		int eleft = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-2)).left;
		int eright = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-2)).right;
		Exp e = (Exp)((java_cup.runtime.Symbol) CUP$Parser$stack.elementAt(CUP$Parser$top-2)).value;
		int aleft = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).left;
		int aright = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).right;
		Asmt a = (Asmt)((java_cup.runtime.Symbol) CUP$Parser$stack.peek()).value;
		 RESULT = new LetId(id,e,a); 
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("asmt",3, ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-5)), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 24: // asmt ::= exp 
            {
              Asmt RESULT =null;
		int eleft = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).left;
		int eright = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).right;
		Exp e = (Exp)((java_cup.runtime.Symbol) CUP$Parser$stack.peek()).value;
		 RESULT = new FinAsmt(e); 
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("asmt",3, ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 25: // fundefs ::= LET UNDERSC EQUAL asmt 
            {
              Fundef RESULT =null;
		int aleft = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).left;
		int aright = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).right;
		Asmt a = (Asmt)((java_cup.runtime.Symbol) CUP$Parser$stack.peek()).value;
		 RESULT = new Main(a); 
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("fundefs",1, ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-3)), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 26: // fundefs ::= LET LABEL formal_args EQUAL asmt fundefs 
            {
              Fundef RESULT =null;
		int lleft = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-4)).left;
		int lright = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-4)).right;
		Label l = (Label)((java_cup.runtime.Symbol) CUP$Parser$stack.elementAt(CUP$Parser$top-4)).value;
		int fleft = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-3)).left;
		int fright = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-3)).right;
		Formal_arg f = (Formal_arg)((java_cup.runtime.Symbol) CUP$Parser$stack.elementAt(CUP$Parser$top-3)).value;
		int aleft = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)).left;
		int aright = ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-1)).right;
		Asmt a = (Asmt)((java_cup.runtime.Symbol) CUP$Parser$stack.elementAt(CUP$Parser$top-1)).value;
		int nleft = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).left;
		int nright = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).right;
		Fundef n = (Fundef)((java_cup.runtime.Symbol) CUP$Parser$stack.peek()).value;
		 RESULT = new Fun(l,f,a,n); 
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("fundefs",1, ((java_cup.runtime.Symbol)CUP$Parser$stack.elementAt(CUP$Parser$top-5)), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 27: // toplevel ::= fundefs 
            {
              Toplevel RESULT =null;
		int fleft = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).left;
		int fright = ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()).right;
		Fundef f = (Fundef)((java_cup.runtime.Symbol) CUP$Parser$stack.peek()).value;
		 RESULT = new Toplevel(f); 
              CUP$Parser$result = parser.getSymbolFactory().newSymbol("toplevel",5, ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$Parser$stack.peek()), RESULT);
            }
          return CUP$Parser$result;

          /* . . . . . .*/
          default:
            throw new Exception(
               "Invalid action number "+CUP$Parser$act_num+"found in internal parse table");

        }
    } /* end of method */

  /** Method splitting the generated action code into several parts. */
  public final java_cup.runtime.Symbol CUP$Parser$do_action(
    int                        CUP$Parser$act_num,
    java_cup.runtime.lr_parser CUP$Parser$parser,
    java.util.Stack            CUP$Parser$stack,
    int                        CUP$Parser$top)
    throws java.lang.Exception
    {
              return CUP$Parser$do_action_part00000000(
                               CUP$Parser$act_num,
                               CUP$Parser$parser,
                               CUP$Parser$stack,
                               CUP$Parser$top);
    }
}

}
