package asml.parser;


import asml.tree.*;
import asml.tree.asmt.*;
import asml.tree.exp.*;
import asml.tree.fundef.*;
import asml.tree.ident_or_imm.*;
//----------------------------------------------------
// The following code was generated by CUP v0.11b 20140808 (SVN rev 54)
//----------------------------------------------------

/** CUP generated interface containing symbol constants. */
public interface sym {
  /* terminals */
  public static final int APPCLO = 35;
  public static final int GE = 15;
  public static final int FDIV = 26;
  public static final int LPAREN = 8;
  public static final int INT = 3;
  public static final int FMUL = 25;
  public static final int RPAREN = 9;
  public static final int FADD = 28;
  public static final int NIL = 7;
  public static final int NOP = 34;
  public static final int IN = 20;
  public static final int BOOL = 2;
  public static final int FLE = 14;
  public static final int CALL = 32;
  public static final int PLUS = 10;
  public static final int ASSIGN = 29;
  public static final int FNEG = 23;
  public static final int IF = 16;
  public static final int DOT = 21;
  public static final int LE = 13;
  public static final int EOF = 0;
  public static final int EQUAL = 11;
  public static final int NEW = 33;
  public static final int error = 1;
  public static final int ADD = 30;
  public static final int IDENT = 5;
  public static final int FEQUAL = 12;
  public static final int FSUB = 27;
  public static final int NEG = 22;
  public static final int ELSE = 18;
  public static final int MEM = 24;
  public static final int LET = 19;
  public static final int FLOAT = 4;
  public static final int THEN = 17;
  public static final int UNDERSC = 36;
  public static final int LABEL = 6;
  public static final int SUB = 31;
  public static final String[] terminalNames = new String[] {
  "EOF",
  "error",
  "BOOL",
  "INT",
  "FLOAT",
  "IDENT",
  "LABEL",
  "NIL",
  "LPAREN",
  "RPAREN",
  "PLUS",
  "EQUAL",
  "FEQUAL",
  "LE",
  "FLE",
  "GE",
  "IF",
  "THEN",
  "ELSE",
  "LET",
  "IN",
  "DOT",
  "NEG",
  "FNEG",
  "MEM",
  "FMUL",
  "FDIV",
  "FSUB",
  "FADD",
  "ASSIGN",
  "ADD",
  "SUB",
  "CALL",
  "NEW",
  "NOP",
  "APPCLO",
  "UNDERSC"
  };

  /* non terminals */
  static final int toplevel = 5;
  static final int ident_or_imm = 4;
  static final int formal_args = 0;
  static final int fundefs = 1;
  static final int exp = 2;
  static final int asmt = 3;
}

