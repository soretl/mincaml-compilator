package asml.parser;

import java.io.FileReader;

import asml.tree.Toplevel;


public class AsmlReader {
	public static Toplevel read(String filename) throws Exception{
		Parser p;
		Lexer l = new Lexer(new FileReader(filename));
		p = new Parser(l);

		Toplevel arbre = (Toplevel) p.parse().value;      
		return arbre;
	}
}
