package asml.visitor;

import asml.tree.Formal_arg;
import asml.tree.Toplevel;
import asml.tree.asmt.FinAsmt;
import asml.tree.asmt.LetId;
import asml.tree.exp.*;
import asml.tree.fundef.Fun;
import asml.tree.fundef.Main;
import asml.tree.ident_or_imm.Ident;
import asml.tree.ident_or_imm.Register;
import asml.tree.ident_or_imm.StackOffset;

public interface AsmlObjVisitor<E> {
	//TODO
    //Exp
    public E visit(Int e);
    public E visit(Ident e);
    public E visit(Label e);
    public E visit(Neg e);
    public E visit(Add e);
    public E visit(Sub e);
    public E visit(MemLoad e);
    public E visit(MemStore e);
    public E visit(If e);
    public E visit(Call e);
    public E visit(CallClo e);
    public E visit(New e);
    public E visit(Nop e);

    //Formal_args
    public E visit(Formal_arg e);

    //Asmt
    public E visit(LetId e);
    public E visit(FinAsmt e);

    //FunDef
    public E visit(Main e);
    public E visit(Fun e);
	public E visit(Toplevel e);

	public E visit(StackOffset e);
	public E visit(Register e);

}
