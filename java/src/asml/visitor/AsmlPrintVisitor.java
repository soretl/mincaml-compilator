package asml.visitor;

import asml.tree.Formal_arg;
import asml.tree.Toplevel;
import asml.tree.asmt.FinAsmt;
import asml.tree.asmt.LetId;
import asml.tree.exp.*;
import asml.tree.fundef.Fun;
import asml.tree.fundef.Main;
import asml.tree.ident_or_imm.Ident;
import asml.tree.ident_or_imm.Ident_or_Arm;
import asml.tree.ident_or_imm.Register;
import asml.tree.ident_or_imm.StackOffset;

public class AsmlPrintVisitor implements AsmlVisitor{

	private static String tabulation = "";

	@Override
	public void visit(Int e) {
		System.out.print(e.i);
	}

	@Override
	public void visit(Ident e) {
		System.out.print(e.id.toString());
	}

	@Override
	public void visit(Label e) {
		System.out.print(e.id.toString());
	}

	@Override
	public void visit(Neg e) {
		System.out.print("neg ");
		e.ident.accept(this);
	}

	@Override
	public void visit(Add e) {
		System.out.print("add ");
		e.left.accept(this);
		System.out.print(" ");
		e.right.accept(this);
	}

	@Override
	public void visit(Sub e) {
		e.left.accept(this);
		System.out.print("-");
		e.right.accept(this);
	}

	@Override
	public void visit(Call e) {
		System.out.print(tabulation+"Call ");
		e.l.accept(this);
		if(e.args.args.isEmpty()){
                    System.out.print(" () ");
                }
                else{
                    e.args.accept(this);
                }
	}

	@Override
	public void visit(CallClo e) {
		System.out.print("CallClo ");
		e.ident.accept(this);
		System.out.print(" ");
		e.args.accept(this);
	}

	@Override
	public void visit(New e) {
		System.out.print("new ");
		e.ident.accept(this);
	}

	@Override
	public void visit(Nop e) {
		System.out.print("nop");
	}

	@Override
	public void visit(Formal_arg e) {
		for (Ident_or_Arm l : e.args) {
			System.out.print(" ");
			l.accept(this);
		}
	}

	@Override
	public void visit(LetId e) {
		System.out.print(tabulation+"Let ");
		e.id.accept(this);
		System.out.print("=");
		e.exp.accept(this);
		System.out.println(" in ");
		e.next.accept(this);
	}

	@Override
	public void visit(FinAsmt e) {
            e.exp.accept(this);
	}

	@Override
	public void visit(Main e) {
		tabulation = "";
		System.out.println("Let _ = ");
		tabulation += "\t";
		e.asmt.accept(this);
	}

	// LET LABEL formal_args EQUAL asmt fundefs
	@Override
	public void visit(Fun e) {
		tabulation = "";
		System.out.print("Let ");
		e.label.accept(this);
		System.out.print(" ");
		if(e.args.args.isEmpty()){
                    System.out.print(" () ");
                }
                else{
                    e.args.accept(this);
                }
		System.out.println(" = ");
		tabulation += "\t";
		e.functionCore.accept(this);
		System.out.println();
		System.out.println();
		tabulation = "";
		e.next.accept(this);
	}

	@Override
	public void visit(MemLoad e) {
		System.out.print("mem(");
		e.address.accept(this);
		System.out.print("+");
		e.offset.accept(this);
		System.out.print(")");
	}

	@Override
	public void visit(MemStore e) {
		System.out.print("mem(");
		e.address.accept(this);
		System.out.print("+");
		e.offset.accept(this);
		System.out.print(")<-");
		e.source.accept(this);
	}

	@Override
	public void visit(If e) {
		System.out.print("if ");
		e.condLeft.accept(this);
		System.out.print(" "+e.condition.toString()+" ");
		e.condRight.accept(this);
		System.out.println(" then ");
		e.nextThen.accept(this);
                System.out.println();
                System.out.println(" else ");
		e.nextElse.accept(this);
	}

	@Override
	public void visit(Toplevel e) {
		e.f.accept(this);
	}
	
	@Override
	public void visit(StackOffset e) {
		System.out.print(e.toString()+ (e.aPush?"(yes)":"(no)"));
	}
	
	@Override
	public void visit(Register e) {
		System.out.print(e.toString());
	}
}