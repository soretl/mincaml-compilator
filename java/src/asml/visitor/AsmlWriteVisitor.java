package asml.visitor;

import asml.tree.Formal_arg;
import asml.tree.Noeud;
import asml.tree.Toplevel;
import asml.tree.asmt.FinAsmt;
import asml.tree.asmt.LetId;
import asml.tree.exp.*;
import asml.tree.fundef.Fun;
import asml.tree.fundef.Main;
import asml.tree.ident_or_imm.Ident;
import asml.tree.ident_or_imm.Ident_or_Arm;
import asml.tree.ident_or_imm.Ident_or_imm;
import asml.tree.ident_or_imm.Register;
import asml.tree.ident_or_imm.StackOffset;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class AsmlWriteVisitor implements AsmlVisitor{
    String res;
    
    public AsmlWriteVisitor(Noeud e, String output_file){
        res = "";
        e.accept(this);
        try {
            PrintWriter pw = new PrintWriter(output_file);
            pw.print(res);
            pw.close();
        } catch(IOException ie) {
           ie.printStackTrace();
        }
    }

    @Override
    public void visit(Int e) {
        res = res + e.i;
    }

    @Override
    public void visit(Ident e) {
        res = res + e.id.toString();
    }

    @Override
    public void visit(Label e) {
        res = res + e.id.toString();
    }

    @Override
    public void visit(Neg e) {
        res = res + "neg ";
        e.ident.accept(this);
    }

    @Override
    public void visit(Add e) {
        res = res + "add ";
        e.left.accept(this);
        res = res + " ";
        e.right.accept(this);
    }

    @Override
    public void visit(Sub e) {
        res = res + "sub ";
        e.left.accept(this);
        res = res + " ";
        e.right.accept(this);
    }

    @Override
    public void visit(Call e) {
        res = res + " call ";
        e.l.accept(this);
        if(e.args.args.isEmpty()){
            res = res + " () ";
        }
        else{
            e.args.accept(this);
        }
        res = res + " ";
    }

    @Override
    public void visit(CallClo e) {
        res = res + " call_closure ";
        e.ident.accept(this);
        res = res + " ";
        e.args.accept(this);
    }

    @Override
    public void visit(New e) {
        res += " new ";
        e.ident.accept(this);
    }

    @Override
    public void visit(Nop e) {
        res += " nop ";
    }

    @Override
    public void visit(Formal_arg e) {
        for (Ident_or_Arm l : e.args) {
            res = res + " ";
            l.accept(this);
        }
    }

    @Override
    public void visit(LetId e) {
        res = res + "let ";
        e.id.accept(this);
        res = res + "=";
        e.exp.accept(this);
        res = res + " in ";
        e.next.accept(this);
        res = res + " ";
    }

    @Override
    public void visit(FinAsmt e) {
        e.exp.accept(this);
    }

    @Override
    public void visit(Main e) {
        res = res + "let _ = \n";
        e.asmt.accept(this);
        res = res + " ";
    }

    // LET LABEL formal_args EQUAL asmt fundefs
    @Override
    public void visit(Fun e) {
        res = res + " let ";
        e.label.accept(this);
        res = res + " ";
        if(e.args.args.isEmpty()){
            res = res + " () ";
        }
        else{
            e.args.accept(this);
        }
        res = res + " = ";
        e.functionCore.accept(this);
        res = res + "\n";
        e.next.accept(this);
        res = res + " ";
    }

    @Override
    public void visit(MemLoad e) {
        res = res + "mem(";
        e.address.accept(this);
        res = res + " + ";
        e.offset.accept(this);
        res = res + ") ";
    }

    @Override
    public void visit(MemStore e) {
        res = res + "mem(";
        e.address.accept(this);
        res = res + " + ";
        e.offset.accept(this);
        res = res + ") <- ";
        e.source.accept(this);
        res = res + " ";
    }

    @Override
    public void visit(If e) {
        res = res + " if ";
        e.condLeft.accept(this);
        res = res + e.condition.toString(); 
        e.condRight.accept(this);
        res = res + " then ";
        e.nextThen.accept(this);
        res = res + " else ";
        e.nextElse.accept(this);
    }

    @Override
    public void visit(Toplevel e) {
        e.f.accept(this);
    }

    @Override
    public void visit(StackOffset e) {

    }

    @Override
    public void visit(Register e) {

    }
}