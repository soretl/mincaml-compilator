package asml.visitor;

import asml.tree.Formal_arg;
import asml.tree.Toplevel;
import asml.tree.asmt.FinAsmt;
import asml.tree.asmt.LetId;
import asml.tree.exp.*;
import asml.tree.fundef.Fun;
import asml.tree.fundef.Main;
import asml.tree.ident_or_imm.Ident;
import asml.tree.ident_or_imm.Register;
import asml.tree.ident_or_imm.StackOffset;

public interface AsmlVisitor {
	//TODO
    //Exp
    void visit(Int e);
    void visit(Ident e);
    void visit(Label e);
    void visit(Neg e);
    void visit(Add e);
    void visit(Sub e);
    void visit(MemLoad e);
    void visit(MemStore e);
    void visit(If e);
    void visit(Call e);
    void visit(CallClo e);
    void visit(New e);
    void visit(Nop e);

    //Formal_arg
    void visit(Formal_arg e);

    //Asmt
    void visit(LetId e);
    void visit(FinAsmt e);

    //FunDef
    void visit(Main e);
    void visit(Fun e);
	void visit(Toplevel e);
	
	void visit(StackOffset e);
	void visit(Register e);

}


