package asml.visitor;

import asml.tree.Formal_arg;
import asml.tree.asmt.FinAsmt;
import asml.tree.asmt.LetId;
import asml.tree.exp.*;
import asml.tree.fundef.Fun;
import asml.tree.fundef.Main;
import asml.tree.ident_or_imm.Ident;
import asml.tree.ident_or_imm.Register;
import asml.tree.ident_or_imm.StackOffset;

public interface ObjArgVisitor<E,A> {
	//Exp
    E visit(Int e, A arg);
    E visit(Ident e, A arg);
    E visit(Label e, A arg);
    E visit(Neg e, A arg);
    E visit(Add e, A arg);
    E visit(Sub e, A arg);
    E visit(MemLoad e, A arg);
    E visit(MemStore e, A arg);
    E visit(If e, A arg);
    E visit(Call e, A arg);
    E visit(CallClo e, A arg);
    E visit(New e, A arg);
    E visit(Nop e, A arg);

    //Formal_args
    E visit(Formal_arg e, A arg);

    //Asmt
    E visit(LetId e, A arg);
    E visit(FinAsmt e, A arg);

    //FunDef
    E visit(Main e, A arg);
    E visit(Fun e, A arg);
    
    E visit(StackOffset e, A arg);
    E visit(Register e, A arg);

}
